#DROP DATABASE psh_db_main;
CREATE DATABASE psh_db_main;
USE psh_db_main;

CREATE TABLE administrators (
  admin_ID varchar(10) NOT NULL,
  display_name varchar(20) DEFAULT NULL,
  adkey varchar(255) DEFAULT NULL,
  authorized BOOLEAN NOT NULL,
  last_login varchar(15) NOT NULL,
  PRIMARY KEY (admin_ID),
  UNIQUE (display_name)
);

INSERT INTO administrators VALUES('admin', 'arianne gallardo', 'fcea920f7412b5da7be0cf42b8c93759', 1, '');
INSERT INTO administrators VALUES('cara', 'cara delevingne', '015f48d02c2031ea86e3bf4506f59cdd', 1, '');
INSERT INTO administrators VALUES('lolaflora', 'lola flora', '62aaec4a5afc04d19c2a3b9cd91f4163', 1, '');

CREATE TABLE availed (
  username varchar(20) NOT NULL,
  order_ID varchar(20) DEFAULT NULL,
  time_stamp varchar(15) DEFAULT NULL,
  variation varchar(20) DEFAULT NULL,
  size varchar(20) DEFAULT NULL,
  quantity int(11) DEFAULT NULL,
  product_code VARCHAR(20),
  PRIMARY KEY (username, order_ID, product_code, quantity, variation, size)
);

INSERT INTO availed VALUES('augallardo', '1234', '11/11/2011', 'red', 'small', 'code-prod-1234', 2);
INSERT INTO availed VALUES('cardodalisay', '9873', '11/11/2011', 'red', 'small', 'code-prod-1234', 2);
INSERT INTO availed VALUES('ariannegallardo', '5678', '11/11/2011', 'red', 'small', 'code-prod-1234', 2);

CREATE TABLE checkout (
  username varchar(20) NOT NULL,
  order_ID varchar(20) NOT NULL,
  recipient varchar(50) DEFAULT NULL,
  shipping_address varchar(255) DEFAULT NULL,
  landmark varchar(70) DEFAULT NULL,
  checkout_date varchar(20) DEFAULT NULL,
  shipping_fee double DEFAULT NULL,
  products_price double DEFAULT NULL,
  status varchar(10) DEFAULT NULL,
  courier varchar(20) DEFAULT NULL,
  receiver varchar(50) DEFAULT NULL,
  tracking_number varchar(20) DEFAULT NULL,
  cancelled_on varchar(20) DEFAULT NULL,
  ETA varchar(20) DEFAULT NULL,
  PRIMARY KEY (username, order_ID)
);

INSERT INTO checkout VALUES('augallardo', '1010', 'arianne', 'address1', 'landmark ', '11/11/2011', 40, 670, 'processed', NULL, 'arianne gallardo', NULL, NULL, NULL);
INSERT INTO checkout VALUES('augallardo', '1234', 'arianne', 'address1', 'landmark ', '11/11/2011', 40, 670, 'processing', NULL, 'arianne gallardo', NULL, NULL, NULL);
INSERT INTO checkout VALUES('augallardo', '4567', 'arianne', 'address1', 'landmark ', '11/11/2011', 40, 670, 'processing', NULL, 'arianne gallardo', NULL, NULL, NULL);
INSERT INTO checkout VALUES('augallardo', '46434', 'arianne', 'address1', 'landmark ', '11/11/2011', 40, 670, 'shipped', NULL, 'arianne gallardo', NULL, NULL, NULL);
INSERT INTO checkout VALUES('augallardo', '9083', 'arianne', 'address1', 'landmark ', '11/11/2011', 40, 670, 'complete', NULL, 'arianne gallardo', NULL, NULL, NULL);

CREATE TABLE faq (
  tag int(11) NOT NULL,
  question varchar(100) DEFAULT NULL,
  answer varchar(1000) DEFAULT NULL
);

INSERT INTO faq VALUES(2, 'What is a Retweet?', 'A Retweet is a re-posting of a Tweet. Twitter\'s Retweet feature helps you and others quickly share that Tweet with all of your followers. You can Retweet your own Tweets or Tweets from someone else.');
INSERT INTO faq VALUES(3, 'What does a Retweet look like?', 'Retweets look like normal Tweets with the author\'s name and username next to it, but are distinguished by the Retweet icon  and the name of the person who Retweeted the Tweet. If you see content from someone you do not follow in your timeline, look for Re');
INSERT INTO faq VALUES(4, 'What are \"top\" Tweets?', 'When you search on twitter.com and on the Twitter for iOS and Android apps, you can filter your results by clicking or tapping Top, Latest, Accounts/People, Photos, or Videos (located at the top of your search results). Selecting Top shows Tweets you are likely to care about most first. ');
INSERT INTO faq VALUES(6, 'Why don\'t I see the Tweets I like in top Tweets?', 'Top Tweets are the most relevant Tweets for your search. We determine relevance based on the popularity of a Tweet (e.g., when a lot of people are interacting with or sharing via Retweets and replies), the keywords it contains, and many other factors. If Tweets you love aren\'t showing up as top Tweets, it means they may not be the most relevant results for your search. You can tap Latest to see the most recently-posted Tweets matching your query.');
INSERT INTO faq VALUES(7, 'Why am I not seeing group messages?', 'You can start and participate in group conversations when using twitter.com, the Twitter for iOS or Android apps, and TweetDeck. Learn more about group conversation settings.');
INSERT INTO faq VALUES(8, 'What location information is displayed?', 'If you have chosen to attach location information to your Tweets, your selected location label is displayed underneath the text of the Tweet.\r\nOn twitter.com, you can select a location label such as the name of a neighborhood or city.');
INSERT INTO faq VALUES(10, 'Will admins be able to add a new entry in the FAQ page?', 'Of course, they will be able to.');

CREATE TABLE inquire (
  tag int(11) NOT NULL,
  username varchar(20) DEFAULT NULL,
  product_code varchar(100) DEFAULT NULL,
  sent varchar(20) DEFAULT NULL,
  body varchar(255) DEFAULT NULL,
  parent int(11) DEFAULT NULL,
  admin BOOLEAN DEFAULT NULL,
  replied BOOLEAN DEFAULT NULL,
  PRIMARY KEY (tag)
);

INSERT INTO inquire VALUES(1, 'augallardo', '12345', 'idk', 'this is the body', 0, 0, 1);
INSERT INTO inquire VALUES(2, 'hello', '12345', 'idk', 'lols', 0, 0, 1);
INSERT INTO inquire VALUES(3, 'hello', '4354', 'idk', 'lols', 0, 0, 0);
INSERT INTO inquire VALUES(4, 'arianne', '', '2018-12-02 11:51:11', 'replied', 1, 1, 0);
INSERT INTO inquire VALUES(5, 'arianne', '', '2018-12-02 12:43:03', 'njnvjnxznxcinjnvjnxznxcinjnvjnxznxcinjnvjnxznxcinjnvjnxznxcinjnvjnxznxci', 2, 1, 0);
INSERT INTO inquire VALUES(6, '', '12345', '2018-12-02 18:37:26', 'THIS IS AN INQUIRY', 0, 0, 0);
INSERT INTO inquire VALUES(7, 'augallardo', 'code-prod-1234', '2018-12-02 18:41:15', 'im tired', 0, 0, 1);
INSERT INTO inquire VALUES(8, 'augallardo', 'code-prod-1234', '2018-12-02 18:43:04', 'maam pa extend ng deadline', 0, 0, 1);
INSERT INTO inquire VALUES(9, 'augallardo', 'code-prod-1234', '2018-12-02 18:43:33', 'maam pa extend ng deadline', 0, 0, 1);
INSERT INTO inquire VALUES(10, 'admin', '', '2018-12-03 10:54:59', 'no chances.', 8, 1, 0);
INSERT INTO inquire VALUES(11, 'admin', '', '2018-12-03 10:57:07', 'you can do it :)', 7, 1, 0);
INSERT INTO inquire VALUES(12, 'admin', '', '2018-12-03 11:01:29', 'no chances :(', 9, 1, 0);

CREATE TABLE messages (
  tag int(11) NOT NULL,
  sent varchar(20) DEFAULT NULL,
  email varchar(20) DEFAULT NULL,
  name varchar(50) DEFAULT NULL,
  category varchar(20) DEFAULT NULL,
  title varchar(50) DEFAULT NULL,
  body varchar(1500) DEFAULT NULL,
  replied BOOLEAN DEFAULT NULL
);

INSERT INTO messages VALUES(1, '2018-12-02', 'email', 'arianne', 'cat', 'TITLE HERE', 'BODY HERE LOL', 1);
INSERT INTO messages VALUES(2, '2018-12-02', 'email', 'hello world', 'cat', 'title comone', 'second', 1);
INSERT INTO messages VALUES(5, '2018-12-03 08:23:59', 'tom@cruise.com', 'tom cruise', '', 'i need job asap', 'hello do you have any vacancy for a model', 1);
INSERT INTO messages VALUES(6, '2018-12-03 08:25:48', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 0);
INSERT INTO messages VALUES(7, '2018-12-03 08:44:00', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 0);
INSERT INTO messages VALUES(8, '2018-12-03 08:44:39', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 0);
INSERT INTO messages VALUES(9, '2018-12-03 08:44:47', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 0);
INSERT INTO messages VALUES(10, '2018-12-03 08:45:14', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 1);
INSERT INTO messages VALUES(11, '2018-12-03 08:46:00', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 0);
INSERT INTO messages VALUES(12, '2018-12-03 08:46:16', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 0);
INSERT INTO messages VALUES(13, '2018-12-03 08:46:33', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 0);
INSERT INTO messages VALUES(14, '2018-12-03 08:46:50', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 0);
INSERT INTO messages VALUES(15, '2018-12-03 08:47:09', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 0);
INSERT INTO messages VALUES(16, '2018-12-03 08:47:21', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 0);
INSERT INTO messages VALUES(17, '2018-12-03 08:47:45', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 1);
INSERT INTO messages VALUES(18, '2018-12-03 10:42:53', 'cardo@yahoo.com', 'cardo dalisay', '', 'Amazing website!', 'this is such an amazing website!', 1);

CREATE TABLE payments (
  username varchar(20) NOT NULL,
  order_ID varchar(20) NOT NULL,
  method varchar(20) DEFAULT NULL,
  reference varchar(50) NOT NULL,
  sender_name varchar(50) DEFAULT NULL,
  amount_sent double DEFAULT NULL,
  total double DEFAULT NULL,
  date_sent varchar(20) DEFAULT NULL,
  received BOOLEAN DEFAULT NULL,
  PRIMARY KEY (username, order_ID, reference)
);

INSERT INTO payments VALUES('augallardo', '1010', 'palawan', 'ref-1234', 'arianne gallardo', 710, 710, '11/11/2011', 1);
INSERT INTO payments VALUES('augallardo', '1234', 'palawan', 'ref-1234', 'arianne gallardo', 710, 710, '11/11/2011', 0);
INSERT INTO payments VALUES('augallardo', '4567', 'palawan', 'ref-1234', 'arianne gallardo', 710, 710, '11/11/2011', 0);

CREATE TABLE products (
  product_code varchar(100) NOT NULL,
  name varchar(255) NOT NULL,
  category varchar(20) NOT NULL,
  status varchar(15) NOT NULL,
  description varchar(255) DEFAULT NULL,
  price double NOT NULL,
  discount double DEFAULT NULL,
  rating int(2) DEFAULT '0',
  manufacturer varchar(50) DEFAULT NULL,
  sizerange varchar(50) DEFAULT NULL,
  colors varchar(50) DEFAULT NULL,
  material varchar(50) DEFAULT NULL,
  madein varchar(20) DEFAULT NULL,
  imgcnt int(11) DEFAULT '0',
  sales int(11) DEFAULT '0',
  PRIMARY KEY (product_code)
);

INSERT INTO products VALUES('12345', 'ROMPER SOMETHING', 'rompers', 'available', 'ABCDEFHUWDFHSUDHFUDSHFUHFU', 400, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO products VALUES('code-prod-1234', 'DRESS ITEM', 'dress', 'available', NULL, 340, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO products VALUES('FLRL-SLVLSS-CHFFN-SLT-BCKLSS-BCH-DRSS', 'FLORAL SLEEVELESS CHIFFON SLIT BACKLESS BEACH DRESS', 'dresses', 'available', 'FLORAL SLEEVELESS CHIFFON SLIT BACKLESS BEACH DRESS', 1230, 0, 0, '', '', '', '', '', 0, 0);
INSERT INTO products VALUES('HLDY-STYL-FLRL-SLVLSS-MX-DRSS', 'HOLIDAY STYLE FLORAL SLEEVELESS MAXI DRESS', 'dresses', 'available', 'this is the HOLIDAY STYLE FLORAL SLEEVELESS MAXI DRESS', 1500, 0, 0, 'forever21', 'freesize', 'red,blue,maroon', 'cotton', 'korea', 0, 0);
INSERT INTO products VALUES('PLS-SZ-FLWR-WD-LG-JMPST', 'PLUS SIZE FLOWER WIDE LEG JUMPSUIT', 'rompers', 'available', 'PLUS SIZE FLOWER WIDE LEG JUMPSUIT', 1180, 0, 0, '', '', '', '', '', 0, 0);
INSERT INTO products VALUES('PLS-SZ-PRNT--LN-DRSS', 'PLUS SIZE PRINT A LINE DRESS', 'summerdresses', 'available', 'PLUS SIZE PRINT A LINE DRESS', 1500, 0, 0, 'plussizehvn', 'XL', 'black', '', '', 0, 0);
INSERT INTO products VALUES('PLS-SZ-RFFL-CRP-TP-ND-STRPD-MX-SKRT', 'PLUS SIZE RUFFLE CROP TOP AND STRIPED MAXI SKIRT', 'summerdresses', 'available', 'PLUS SIZE RUFFLE CROP TOP AND STRIPED MAXI SKIRT\n', 1570, 0, 0, '', '', '', '', '', 0, 0);
INSERT INTO products VALUES('PLS-SZ-STRPD-HGH-SLT-JMPST', 'PLUS SIZE STRIPED HIGH SLIT JUMPSUIT', 'rompers', 'available', 'PLUS SIZE STRIPED HIGH SLIT JUMPSUIT', 1678, 0, 0, 'uniqlo', 'freesize', 'red, blue', 'cotton', 'japan', 0, 0);
INSERT INTO products VALUES('PLS-SZ-TRBL-CVR-P-KMN', 'PLUS SIZE TRIBAL COVER UP KIMONO', 'coverups', 'available', 'PLUS SIZE TRIBAL COVER UP KIMONO', 850, 0, 0, 'forever21', 'small, medium', 'green, blue', 'satin', 'vietnam', 0, 0);
INSERT INTO products VALUES('PLS-SZ-TW-TN-HGH-RS-BKN-ST', 'PLUS SIZE TWO TONE HIGH RISE BIKINI SET', 'swimwear', 'available', 'PLUS SIZE TWO TONE HIGH RISE BIKINI SET', 1200, 0, 0, 'topman', 'L, XL, XXL', '', '', 'thailand', 0, 0);
INSERT INTO products VALUES('PRCT-CRCHT-LC-P-PLS-SZ-CVR-P', 'APRICOT CROCHET LACE UP PLUS SIZE COVER UP', 'coverups', 'available', 'APRICOT CROCHET LACE UP PLUS SIZE COVER UP', 820, 0, 0, 'mango', 'small,large', 'white, cream', 'knitted', 'thailand', 0, 0);
INSERT INTO products VALUES('SMD-BKN-TP-RCHD-HGH-WST-BTTM-PLS-SWMWR', 'SEAMED BIKINI TOP RUCHED HIGH WAIST BOTTOM PLUS SWIMWEAR', 'swimwear', 'available', 'SEAMED BIKINI TOP RUCHED HIGH WAIST BOTTOM PLUS SWIMWEAR SALE ADD TO WISHLIST\nQUICK VIEW\nSEAMED BIKINI TOP RUCHED HIGH WAIST BOTTOM PLUS SWIMWEAR', 1250, 0, 0, '', '', '', '', '', 0, 0);
INSERT INTO products VALUES('V-NCK-FLWR-MX-DRSS-BCHWR', 'V NECK FLOWER MAXI DRESS BEACHWEAR', 'dresses', 'available', 'V NECK FLOWER MAXI DRESS BEACHWEAR', 1500, 0, 0, '', '', '', '', '', 0, 0);
INSERT INTO products VALUES('VNTG-SLD-BCKLSS-FLR-DRSS', 'VINTAGE SOLID BACKLESS FLOOR DRESS', 'dresses', 'available', 'VINTAGE SOLID BACKLESS FLOOR DRESS', 1800, 0, 0, 'estilo', 'freesize', 'maroon only', 'polyester', 'korea', 0, 0);

CREATE TABLE rate (
  username varchar(20) NOT NULL,
  product_code varchar(20) NOT NULL,
  time_stamp varchar(20) DEFAULT NULL,
  title varchar(20) DEFAULT NULL,
  body varchar(255) DEFAULT NULL,
  stars int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE registered_users (
  username varchar(20) NOT NULL,
  email varchar(20) DEFAULT NULL,
  display_name varchar(50) DEFAULT NULL,
  mobile varchar(15) NOT NULL,
  membership_date varchar(20) DEFAULT NULL,
  acctkey varchar(255) DEFAULT NULL,
  lastlogin varchar(15) NOT NULL,
  token_key_log varchar(255) DEFAULT NULL,
  PRIMARY KEY (username),
  UNIQUE (email)
);

INSERT INTO registered_users VALUES('admin', '', 'admin', '', '', 'fcea920f7412b5da7be0cf42b8c93759', '', NULL);
INSERT INTO registered_users VALUES('ariannegallardo', 'a.rianne13@yahoo.com', 'arianne gallardo', '09171169207', '2018-11-25 04:28:23', 'bd6a4bcc31759cf8cecafba925e2a677', '2018-11-25 04:2', NULL);
INSERT INTO registered_users VALUES('augallardo', 'hello@world.com', 'hello world', '09171169207', '2018-11-24 03:06:08', 'fcea920f7412b5da7be0cf42b8c93759', '2018-11-24 03:0', NULL);
INSERT INTO registered_users VALUES('cara', 'cara', '', '', '', '015f48d02c2031ea86e3bf4506f59cdd', '', '');
INSERT INTO registered_users VALUES('cardodalisay', 'cardo@yahoo.com', 'cardo dalisay', '09123456789', '2018-12-03 10:43:21', '74d3d1e8529116921ea1365bebf0fa96', '2018-12-03 10:4', '');
INSERT INTO registered_users VALUES('dvnbdhfb', 'dnisanf@nifdns.com', 'sdfjnajfb bfdsb', '09123456789', '2018-11-24 11:13:02', '015f48d02c2031ea86e3bf4506f59cdd', '2018-11-24 11:1', NULL);
INSERT INTO registered_users VALUES('lolaflora', 'lolaflora', '', '', '', '015f48d02c2031ea86e3bf4506f59cdd', '', '');
INSERT INTO registered_users VALUES('pickachuu', 'pika@chu.com', 'pika chi', '09123456789', '2018-11-24 03:12:24', '015f48d02c2031ea86e3bf4506f59cdd', '2018-11-24 03:1', NULL);
INSERT INTO registered_users VALUES('thisisit', 'a@yah.com', 'arianne gallardo', '09123456789', '2018-11-23 15:47:54', '67c69f03fc567808b0950ae02e0240b5', '', NULL);

CREATE TABLE shopping_carts (
  username varchar(20) NOT NULL,
  variation_name varchar(20) NOT NULL,
  size varchar(20) NOT NULL,
  product_code varchar(100) NOT NULL,
  quantity int(11) DEFAULT NULL,
   PRIMARY KEY (username, product_code, size, variation_name)
);

INSERT INTO shopping_carts VALUES('augallardo', '', '', '12345', 2);
INSERT INTO shopping_carts VALUES('augallardo', '', '', 'code-prod-1234', 5);
INSERT INTO shopping_carts VALUES('augallardo', '', '', 'FLRL-SLVLSS-CHFFN-SLT-BCKLSS-BCH-DRSS', 1);
INSERT INTO shopping_carts VALUES('cardodalisay', '', '', 'code-prod-1234', 1);

CREATE TABLE social_media (
  fb varchar(25) DEFAULT NULL,
  instagram varchar(25) DEFAULT NULL
);

INSERT INTO social_media VALUES('plussizeheaven', 'plussizeheaven');

CREATE TABLE terms_and_policies (
  about_us varchar(255) DEFAULT NULL,
  return_pol varchar(1000) DEFAULT NULL,
  payment_methods varchar(1000) DEFAULT NULL,
  shipping_options varchar(1000) DEFAULT NULL,
  tag int(11) NOT NULL
);

INSERT INTO terms_and_policies VALUES('If you are into women’s fashion, you’ll be happy to know about Estilo Philippines. As one of the online shopping website in the country, it offers an impressive collection of clothing apparel, swimwear, formal clothing and much more. The products on offer', 'Sorry but you can no longer return the item you already bought from here :)', 'Cash On Delivery\nCredit/ Debit Card , Philippine-Paypal accounts,Lazada E-WalletBDO Installment', 'JRS Express, LBC, LALAMOVE. Express deliveries available and with respective extra fees and charges.', 1);

CREATE TABLE user_addresses (
  username varchar(20) NOT NULL,
  a_index int(11) DEFAULT NULL,
  address varchar(255) NOT NULL,
  landmark varchar(255) DEFAULT NULL,
  PRIMARY KEY (a_index, username, address)
);

INSERT INTO user_addresses VALUES('augallardo', 1, '(Unit  )  314 Lakandula St. Tondo North Manila Metro Manila NCR', 'in front of EVER grocery and mall');
INSERT INTO user_addresses VALUES('augallardo', 2, '(Unit 5)  #3 DCS St. Diliman Quezon City NCR', 'near NIGS');

CREATE TABLE variations (
  product_code varchar(100) NOT NULL,
  variation_name varchar(20) NOT NULL,
 PRIMARY KEY (product_code, variation_name)
);

CREATE TABLE wishlist (
  username varchar(20) NOT NULL,
  product_code varchar(100) NOT NULL,
  PRIMARY KEY (username, product_code)
);

INSERT INTO wishlist VALUES('augallardo', '12345');
INSERT INTO wishlist VALUES('augallardo', 'code-prod-1234');
INSERT INTO wishlist VALUES('augallardo', 'FLRL-SLVLSS-CHFFN-SLT-BCKLSS-BCH-DRSS');