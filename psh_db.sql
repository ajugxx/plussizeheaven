-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2018 at 08:30 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `psh_db_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE `administrators` (
  `admin_ID` varchar(10) NOT NULL,
  `display_name` varchar(20) DEFAULT NULL,
  `adkey` varchar(255) DEFAULT NULL,
  `authorized` tinyint(1) NOT NULL,
  `last_login` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`admin_ID`, `display_name`, `adkey`, `authorized`, `last_login`) VALUES
('admin', 'arianne gallardo', 'fcea920f7412b5da7be0cf42b8c9375', 1, ''),
('cara', 'cara delevingne', '015f48d02c2031ea86e3bf4506f59cdd', 1, ''),
('secondadmi', 'second admin', '015f48d02c2031ea86e3bf4506f59cdd', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `availed`
--

CREATE TABLE `availed` (
  `username` varchar(20) NOT NULL,
  `order_ID` varchar(20) DEFAULT NULL,
  `time_stamp` varchar(15) DEFAULT NULL,
  `variation` varchar(20) DEFAULT NULL,
  `size` varchar(20) DEFAULT NULL,
  `code` varchar(20) NOT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `availed`
--

INSERT INTO `availed` (`username`, `order_ID`, `time_stamp`, `variation`, `size`, `code`, `quantity`) VALUES
('augallardo', '1234', '11/11/2011', 'red', 'small', 'code-prod-1234', 2);

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `username` varchar(20) NOT NULL,
  `order_ID` varchar(20) NOT NULL,
  `recipient` varchar(50) DEFAULT NULL,
  `shipping_address` varchar(255) DEFAULT NULL,
  `landmark` varchar(70) DEFAULT NULL,
  `checkout_date` varchar(20) DEFAULT NULL,
  `shipping_fee` double DEFAULT NULL,
  `products_price` double DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `courier` varchar(20) DEFAULT NULL,
  `receiver` varchar(50) DEFAULT NULL,
  `tracking_number` varchar(20) DEFAULT NULL,
  `cancelled_on` varchar(20) DEFAULT NULL,
  `ETA` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `checkout`
--

INSERT INTO `checkout` (`username`, `order_ID`, `recipient`, `shipping_address`, `landmark`, `checkout_date`, `shipping_fee`, `products_price`, `status`, `courier`, `receiver`, `tracking_number`, `cancelled_on`, `ETA`) VALUES
('augallardo', '1010', 'arianne', 'address1', 'landmark ', '11/11/2011', 40, 670, 'processed', NULL, 'arianne gallardo', NULL, NULL, NULL),
('augallardo', '1234', 'arianne', 'address1', 'landmark ', '11/11/2011', 40, 670, 'processing', NULL, 'arianne gallardo', NULL, NULL, NULL),
('augallardo', '4567', 'arianne', 'address1', 'landmark ', '11/11/2011', 40, 670, 'processing', NULL, 'arianne gallardo', NULL, NULL, NULL),
('augallardo', '9083', 'arianne', 'address1', 'landmark ', '11/11/2011', 40, 670, 'complete', NULL, 'arianne gallardo', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `tag` int(11) NOT NULL,
  `question` varchar(100) DEFAULT NULL,
  `answer` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`tag`, `question`, `answer`) VALUES
(2, 'What is a Retweet?', 'A Retweet is a re-posting of a Tweet. Twitter\'s Retweet feature helps you and others quickly share that Tweet with all of your followers. You can Retweet your own Tweets or Tweets from someone else.'),
(3, 'What does a Retweet look like?', 'Retweets look like normal Tweets with the author\'s name and username next to it, but are distinguished by the Retweet icon  and the name of the person who Retweeted the Tweet. If you see content from someone you do not follow in your timeline, look for Re'),
(4, 'What are \"top\" Tweets?', 'When you search on twitter.com and on the Twitter for iOS and Android apps, you can filter your results by clicking or tapping Top, Latest, Accounts/People, Photos, or Videos (located at the top of your search results). Selecting Top shows Tweets you are likely to care about most first. '),
(6, 'Why don\'t I see the Tweets I like in top Tweets?', 'Top Tweets are the most relevant Tweets for your search. We determine relevance based on the popularity of a Tweet (e.g., when a lot of people are interacting with or sharing via Retweets and replies), the keywords it contains, and many other factors. If Tweets you love aren\'t showing up as top Tweets, it means they may not be the most relevant results for your search. You can tap Latest to see the most recently-posted Tweets matching your query.'),
(7, 'Why am I not seeing group messages?', 'You can start and participate in group conversations when using twitter.com, the Twitter for iOS or Android apps, and TweetDeck. Learn more about group conversation settings.'),
(8, 'What location information is displayed?', 'If you have chosen to attach location information to your Tweets, your selected location label is displayed underneath the text of the Tweet.\r\nOn twitter.com, you can select a location label such as the name of a neighborhood or city.');

-- --------------------------------------------------------

--
-- Table structure for table `inquire`
--

CREATE TABLE `inquire` (
  `tag` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `product_code` varchar(20) DEFAULT NULL,
  `sent` varchar(20) DEFAULT NULL,
  `body` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `replied` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inquire`
--

INSERT INTO `inquire` (`tag`, `username`, `product_code`, `sent`, `body`, `parent`, `admin`, `replied`) VALUES
(1, 'augallardo', '12345', 'idk', 'this is the body', 0, 0, 1),
(2, 'hello', '12345', 'idk', 'lols', 0, 0, 1),
(3, 'hello', '4354', 'idk', 'lols', 0, 0, 0),
(4, 'arianne', '', '2018-12-02 11:51:11', 'replied', 1, 1, 0),
(5, 'arianne', '', '2018-12-02 12:43:03', 'njnvjnxznxcinjnvjnxznxcinjnvjnxznxcinjnvjnxznxcinjnvjnxznxcinjnvjnxznxci', 2, 1, 0),
(6, '', '12345', '2018-12-02 18:37:26', 'THIS IS AN INQUIRY', 0, 0, 0),
(7, 'augallardo', 'code-prod-1234', '2018-12-02 18:41:15', 'im tired', 0, 0, 0),
(8, 'augallardo', 'code-prod-1234', '2018-12-02 18:43:04', 'maam pa extend ng deadline', 0, 0, 0),
(9, 'augallardo', 'code-prod-1234', '2018-12-02 18:43:33', 'maam pa extend ng deadline', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `tag` int(11) NOT NULL,
  `sent` varchar(20) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `category` varchar(20) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `body` varchar(1500) DEFAULT NULL,
  `replied` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`tag`, `sent`, `email`, `name`, `category`, `title`, `body`, `replied`) VALUES
(1, '2018-12-02', 'email', 'arianne', 'cat', 'TITLE HERE', 'BODY HERE LOL', 1),
(2, '2018-12-02', 'email', 'hello world', 'cat', 'title comone', 'second', 1),
(5, '2018-12-03 08:23:59', 'tom@cruise.com', 'tom cruise', '', 'i need job asap', 'hello do you have any vacancy for a model', 0),
(6, '2018-12-03 08:25:48', 'felicia@friendster.c', 'Felicia Brenda', '', 'Is this form working?', 'Just testing this form!', 0);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `username` varchar(20) NOT NULL,
  `order_ID` varchar(20) NOT NULL,
  `method` varchar(20) DEFAULT NULL,
  `reference` varchar(50) NOT NULL,
  `sender_name` varchar(50) DEFAULT NULL,
  `amount_sent` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `date_sent` varchar(20) DEFAULT NULL,
  `received` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`username`, `order_ID`, `method`, `reference`, `sender_name`, `amount_sent`, `total`, `date_sent`, `received`) VALUES
('augallardo', '1010', 'palawan', 'ref-1234', 'arianne gallardo', 710, 710, '11/11/2011', 1),
('augallardo', '1234', 'palawan', 'ref-1234', 'arianne gallardo', 710, 710, '11/11/2011', 0),
('augallardo', '4567', 'palawan', 'ref-1234', 'arianne gallardo', 710, 710, '11/11/2011', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(20) NOT NULL,
  `status` varchar(15) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` double NOT NULL,
  `discount` double DEFAULT NULL,
  `rating` int(2) DEFAULT '0',
  `manufacturer` varchar(50) DEFAULT NULL,
  `sizerange` varchar(50) DEFAULT NULL,
  `colors` varchar(50) DEFAULT NULL,
  `material` varchar(50) DEFAULT NULL,
  `madein` varchar(20) DEFAULT NULL,
  `imgcnt` int(11) DEFAULT '0',
  `sales` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_code`, `name`, `category`, `status`, `description`, `price`, `discount`, `rating`, `manufacturer`, `sizerange`, `colors`, `material`, `madein`, `imgcnt`, `sales`) VALUES
('12345', 'ROMPER SOMETHING', 'rompers', 'available', 'ABCDEFHUWDFHSUDHFUDSHFUHFU', 400, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('code-prod-1234', 'DRESS ITEM', 'dress', 'available', NULL, 340, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('FLRL-SLVLSS-CHFFN-SLT-BCKLSS-BCH-DRSS', 'FLORAL SLEEVELESS CHIFFON SLIT BACKLESS BEACH DRESS', 'dresses', 'available', 'FLORAL SLEEVELESS CHIFFON SLIT BACKLESS BEACH DRESS', 1230, 0, 0, '', '', '', '', '', 0, 0),
('HLDY-STYL-FLRL-SLVLSS-MX-DRSS', 'HOLIDAY STYLE FLORAL SLEEVELESS MAXI DRESS', 'dresses', 'available', 'HOLIDAY STYLE FLORAL SLEEVELESS MAXI DRESS', 1100, 0, 0, 'h&m', 'small,medium,large', 'white,pink,black', '', '', 0, 0),
('PLS-SZ-FLWR-WD-LG-JMPST', 'PLUS SIZE FLOWER WIDE LEG JUMPSUIT', 'rompers', 'available', 'PLUS SIZE FLOWER WIDE LEG JUMPSUIT', 1180, 0, 0, '', '', '', '', '', 0, 0),
('PLS-SZ-PRNT--LN-DRSS', 'PLUS SIZE PRINT A LINE DRESS', 'summerdresses', 'available', 'PLUS SIZE PRINT A LINE DRESS', 1500, 0, 0, 'plussizehvn', 'XL', 'black', '', '', 0, 0),
('PLS-SZ-RFFL-CRP-TP-ND-STRPD-MX-SKRT', 'PLUS SIZE RUFFLE CROP TOP AND STRIPED MAXI SKIRT', 'summerdresses', 'available', 'PLUS SIZE RUFFLE CROP TOP AND STRIPED MAXI SKIRT\n', 1570, 0, 0, '', '', '', '', '', 0, 0),
('PLS-SZ-STRPD-HGH-SLT-JMPST', 'PLUS SIZE STRIPED HIGH SLIT JUMPSUIT', 'rompers', 'available', 'PLUS SIZE STRIPED HIGH SLIT JUMPSUIT', 1678, 0, 0, 'uniqlo', 'freesize', 'red, blue', 'cotton', 'japan', 0, 0),
('PLS-SZ-TRBL-CVR-P-KMN', 'PLUS SIZE TRIBAL COVER UP KIMONO', 'coverups', 'available', 'PLUS SIZE TRIBAL COVER UP KIMONO', 850, 0, 0, 'forever21', 'small, medium', 'green, blue', 'satin', 'vietnam', 0, 0),
('PLS-SZ-TW-TN-HGH-RS-BKN-ST', 'PLUS SIZE TWO TONE HIGH RISE BIKINI SET', 'swimwear', 'available', 'PLUS SIZE TWO TONE HIGH RISE BIKINI SET', 1200, 0, 0, 'topman', 'L, XL, XXL', '', '', 'thailand', 0, 0),
('PRCT-CRCHT-LC-P-PLS-SZ-CVR-P', 'APRICOT CROCHET LACE UP PLUS SIZE COVER UP', 'coverups', 'available', 'APRICOT CROCHET LACE UP PLUS SIZE COVER UP', 820, 0, 0, 'mango', 'small,large', 'white, cream', 'knitted', 'thailand', 0, 0),
('SMD-BKN-TP-RCHD-HGH-WST-BTTM-PLS-SWMWR', 'SEAMED BIKINI TOP RUCHED HIGH WAIST BOTTOM PLUS SWIMWEAR', 'swimwear', 'available', 'SEAMED BIKINI TOP RUCHED HIGH WAIST BOTTOM PLUS SWIMWEAR SALE ADD TO WISHLIST\nQUICK VIEW\nSEAMED BIKINI TOP RUCHED HIGH WAIST BOTTOM PLUS SWIMWEAR', 1250, 0, 0, '', '', '', '', '', 0, 0),
('V-NCK-FLWR-MX-DRSS-BCHWR', 'V NECK FLOWER MAXI DRESS BEACHWEAR', 'dresses', 'available', 'V NECK FLOWER MAXI DRESS BEACHWEAR', 1500, 0, 0, '', '', '', '', '', 0, 0),
('VNTG-SLD-BCKLSS-FLR-DRSS', 'VINTAGE SOLID BACKLESS FLOOR DRESS', 'dresses', 'available', 'VINTAGE SOLID BACKLESS FLOOR DRESS', 1800, 0, 0, 'estilo', 'freesize', 'maroon only', 'polyester', 'korea', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `username` varchar(20) NOT NULL,
  `product_code` varchar(20) NOT NULL,
  `time_stamp` varchar(20) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `body` varchar(255) DEFAULT NULL,
  `stars` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registered_users`
--

CREATE TABLE `registered_users` (
  `username` varchar(20) NOT NULL,
  `email` varchar(20) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `mobile` varchar(15) NOT NULL,
  `membership_date` varchar(20) DEFAULT NULL,
  `acctkey` varchar(255) DEFAULT NULL,
  `lastlogin` varchar(15) NOT NULL,
  `token_key_log` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registered_users`
--

INSERT INTO `registered_users` (`username`, `email`, `display_name`, `mobile`, `membership_date`, `acctkey`, `lastlogin`, `token_key_log`) VALUES
('admin', '', 'admin', '', '', 'fcea920f7412b5da7be0cf42b8c93759', '', NULL),
('ariannegallardo', 'a.rianne13@yahoo.com', 'arianne gallardo', '09171169207', '2018-11-25 04:28:23', 'bd6a4bcc31759cf8cecafba925e2a677', '2018-11-25 04:2', NULL),
('augallardo', 'hello@world.com', 'hello world', '09171169207', '2018-11-24 03:06:08', 'fcea920f7412b5da7be0cf42b8c93759', '2018-11-24 03:0', NULL),
('dvnbdhfb', 'dnisanf@nifdns.com', 'sdfjnajfb bfdsb', '09123456789', '2018-11-24 11:13:02', '015f48d02c2031ea86e3bf4506f59cdd', '2018-11-24 11:1', NULL),
('pickachuu', 'pika@chu.com', 'pika chi', '09123456789', '2018-11-24 03:12:24', '015f48d02c2031ea86e3bf4506f59cdd', '2018-11-24 03:1', NULL),
('thisisit', 'a@yah.com', 'arianne gallardo', '09123456789', '2018-11-23 15:47:54', '67c69f03fc567808b0950ae02e0240b5', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shopping_carts`
--

CREATE TABLE `shopping_carts` (
  `username` varchar(20) NOT NULL,
  `variation_name` varchar(20) NOT NULL,
  `size` varchar(20) NOT NULL,
  `product_code` varchar(20) NOT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopping_carts`
--

INSERT INTO `shopping_carts` (`username`, `variation_name`, `size`, `product_code`, `quantity`) VALUES
('augallardo', '', '', '12345', 2),
('augallardo', '', '', 'code-prod-1234', 5),
('augallardo', '', '', 'VNTG-SLD-BCKLSS-FLR-', 1);

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `fb` varchar(25) DEFAULT NULL,
  `instagram` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`fb`, `instagram`) VALUES
('', '');

-- --------------------------------------------------------

--
-- Table structure for table `terms_and_policies`
--

CREATE TABLE `terms_and_policies` (
  `about_us` varchar(255) DEFAULT NULL,
  `return_pol` varchar(1000) DEFAULT NULL,
  `payment_methods` varchar(1000) DEFAULT NULL,
  `shipping_options` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms_and_policies`
--

INSERT INTO `terms_and_policies` (`about_us`, `return_pol`, `payment_methods`, `shipping_options`) VALUES
('If you are into women’s fashion, you’ll be happy to know about Estilo Philippines. As one of the online shopping website in the country, it offers an impressive collection of clothing apparel, swimwear, formal clothing and much more. The products on offer', 'No return No exchange policy for all swimwear and Waist Trainers. Only defective products can be returned or exchanged. If a consumer had a change of mind on their purchase, they can neither return nor demand for a refund of their money. \nClients must also be aware that when speaking of the return and exchange policy, only products with factory defects or imperfections due to the seller’s negligence are covered. All items that are rendered defective due to a buyer’s mishandling or those that are exchanged for a different color, size or design are exempted.\nMake sure that it is the right size—Especially when buying clothes, it is important that you fit them to know if it is the right size for you.', 'Cash On Delivery\nCredit/ Debit Card , Philippine-Paypal accounts,Lazada E-WalletBDO Installment', 'JRS Express, LBC, LALAMOVE. Express deliveries available and with respective extra fees and charges.');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `username` varchar(20) NOT NULL,
  `a_index` int(11) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `landmark` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_addresses`
--

INSERT INTO `user_addresses` (`username`, `a_index`, `address`, `landmark`) VALUES
('augallardo', 1, '(Unit  )  314 Lakandula St. Tondo North Manila Metro Manila NCR', 'in front of EVER grocery and mall');

-- --------------------------------------------------------

--
-- Table structure for table `variations`
--

CREATE TABLE `variations` (
  `product_code` varchar(50) NOT NULL,
  `variation_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `username` varchar(20) NOT NULL,
  `product_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`username`, `product_code`) VALUES
('augallardo', '12345'),
('augallardo', 'code-prod-1234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`admin_ID`),
  ADD UNIQUE KEY `display_name` (`display_name`);

--
-- Indexes for table `availed`
--
ALTER TABLE `availed`
  ADD PRIMARY KEY (`username`,`code`);

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`username`,`order_ID`),
  ADD UNIQUE KEY `tracking_number` (`tracking_number`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`tag`);

--
-- Indexes for table `inquire`
--
ALTER TABLE `inquire`
  ADD PRIMARY KEY (`tag`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`tag`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`username`,`order_ID`,`reference`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_code`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`username`,`product_code`),
  ADD KEY `product_code` (`product_code`);

--
-- Indexes for table `registered_users`
--
ALTER TABLE `registered_users`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `shopping_carts`
--
ALTER TABLE `shopping_carts`
  ADD PRIMARY KEY (`username`,`product_code`,`size`,`variation_name`),
  ADD KEY `product_code` (`product_code`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`username`,`address`);

--
-- Indexes for table `variations`
--
ALTER TABLE `variations`
  ADD PRIMARY KEY (`product_code`,`variation_name`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`username`,`product_code`),
  ADD KEY `product_code` (`product_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `tag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `inquire`
--
ALTER TABLE `inquire`
  MODIFY `tag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `tag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `checkout`
--
ALTER TABLE `checkout`
  ADD CONSTRAINT `checkout_ibfk_1` FOREIGN KEY (`username`) REFERENCES `registered_users` (`username`);

--
-- Constraints for table `rate`
--
ALTER TABLE `rate`
  ADD CONSTRAINT `rate_ibfk_1` FOREIGN KEY (`username`) REFERENCES `registered_users` (`username`),
  ADD CONSTRAINT `rate_ibfk_2` FOREIGN KEY (`product_code`) REFERENCES `products` (`product_code`);

--
-- Constraints for table `shopping_carts`
--
ALTER TABLE `shopping_carts`
  ADD CONSTRAINT `shopping_carts_ibfk_1` FOREIGN KEY (`username`) REFERENCES `registered_users` (`username`);

--
-- Constraints for table `variations`
--
ALTER TABLE `variations`
  ADD CONSTRAINT `variations_ibfk_1` FOREIGN KEY (`product_code`) REFERENCES `products` (`product_code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
