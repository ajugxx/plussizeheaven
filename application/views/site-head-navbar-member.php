<div class="ui double stackable grid">
	<div class="ui five wide column" style="margin-right: -80px;">
		<a href="<?php echo base_url();?>">
			<img class="ui medium image" src="<?php echo base_url();?>assets/images/logo.jpg">
		</a>
	</div>

	<div class="one wide bottom aligned column" style = "margin-bottom: 15px;">
		<i class="large blue facebook icon"></i>
		<i class="large brown instagram icon"></i>
	</div>

	<div class="eleven wide column" style="padding-right: 5%">
		<div class="row navbar" style="height: 25px">
		</div>
		<div class="row navbar" style="text-align: right;font-family: Assistant-Light; letter-spacing: 3px;">
			<form action="#" method="post">
				<button type="submit" value="logout" name="logout" style="border:none;outline:none;background:none;cursor:pointer;"> LOGOUT <i class="large angle right icon"></i> </button><br> 
			</form>
			<a class="normal" href="<?php echo site_url();?>/profile"> MY PROFILE <i class=" user circle icon"></i> </a><br><br>
			<a class="normal" href="<?php echo site_url();?>/wishlist"> WISHLIST <i class=" heart outline icon"></i> </a><br>
			<a class="normal" href="<?php echo site_url();?>/my_cart"> MY CART <i class=" shopping cart icon"></i> </a><br>
		</div>
	</div>
</div>

<div class="ui sticky">
	<div class="navbar">
		<div class="ui large borderless inverted menu" id="menubar">
			<div class="navitemsLeft"></div>
			<a class="item" href="<?php echo site_url();?>/catalog?c=coverups" style="padding-right: 40px; font-family: Assistant-Light; letter-spacing: 3px;">
				COVER-UPS
			</a>
			<a class="item" href="<?php echo site_url();?>/catalog?c=dresses" style="padding-right: 40px; font-family: Assistant-Light; letter-spacing: 3px;">
				DRESSES
			</a>
			<a class="item" href="<?php echo site_url();?>/catalog?c=rompers" style="padding-right: 40px; font-family: Assistant-Light;letter-spacing: 3px;">
				ROMPERS
			</a>
			<a class="item" href="<?php echo site_url();?>/catalog?c=summerdresses" style="padding-right: 40px; font-family: Assistant-Light;letter-spacing: 3px;">
				SUMMER DRESSES
			</a>
			<a class="item" href="<?php echo site_url();?>/catalog?c=swimwear" style="padding-right: 40px; font-family: Assistant-Light; letter-spacing: 3px;">
				SWIMWEAR
			</a>
			<div class="right menu" >
				<div class="item">
				    <div class="ui inverted transparent icon input">
				   		<input placeholder="Search..." type="text" size="35" style="font-family: Assistant-SemiBold; text-transform:uppercase"><i class="search link icon"></i>
					</div>
				</div>
			</div>
			<div class="navitemsRight"></div>	
		</div>
	</div>
</div>

<script type="text/javascript">
	$('.ui.sticky')
  .sticky({
    context: '#body_content'
  })
;
</script>