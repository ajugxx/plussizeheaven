<div class="ui thirteen wide column" style="padding-right: 3%">
	<div class="ui fluid container" style="margin-top: -3%">
		<h1 style="float:right; font-size: 50px;font-family: Assistant-Bold; letter-spacing: 5px;padding-top: 2.5%;padding-bottom: 2.5%">
			ADMIN ACCOUNTS
		</h1>
	</div>

	<div class="ui container">
		<div class="ui items">
				<div class="item">
				    <!-- <div class="image" style="width: 282px;height: 282px">
				      <img src="<?php echo base_url();?>assets/images/1.jpg" class="visible content">
				    </div> -->
					<button class="ui basic button" style="width: 200px;height: 200px;font-size: 17px;color:black;letter-spacing: 2px;font-family: Assistant-Light;">
						UPLOAD DISPLAY PICTURE
					</button>
					<div class="content">
					 	<div class = "meta">
					 		<div class="ui form" style="margin-left: 5%;margin-right:  7%;">
								<div class="inline field">
									<label style="font-size: 80%;color:black;letter-spacing: 2px;font-family: Assistant-Light;">ADMIN NAME:</label>
									<input id="myname" type="text" size="40" value="<?php echo $self?>">
								</div>
								<div class="inline field">
									<label style="font-size: 80%;color:black;letter-spacing: 2px;font-family: Assistant-Light;">ADMIN USERNAME: @<?php echo $handle?></label>	    
								</div>

								<button class="save mini compact positive ui button" style="font-size: 10px; font-family: Assistant-Bold; letter-spacing: 3px;">
						    		SAVE CHANGES
						  		</button>
							</div>

					 	</div>
					</div>
				</div>

				<input type="text" id="message" value="<?php echo $message?>" hidden>

				<!-- x -->
			</div>
	</div>

	<div class="ui container" id="addform" style="margin-top: 4%;display: none;"> 
		<div class="ui fluid form">
			<div class="four fields">
				<div class="field">
					<label>DISPLAY NAME</label>
					<input type="text" id="name">
				</div>
				<div class="field">
					<label>USERNAME</label>
					<input type="text" id="handle">
				</div>
				<div class="field">
					<label>PASSWORD</label>
					<input type="password" id="pass">
				</div>
				<div class="field">
					<label> ... </label>
					<button class="add mini fluid positive ui button" style="font-size: 13px; font-family: Assistant-Bold; letter-spacing: 3px;">
						    		ADD
						  		</button>
				</div>
			</div>
		</div>
	</div>

	<div class="ui container" style="padding-top: 1%"> 
		<h2 style="float:left; font-size: 20px;font-family: Assistant-Bold; letter-spacing: 2px;color:black">ADMIN ACCOUNTS</h2>
		<input id='add' type="submit" style="border: none;background-color: Transparent;cursor:pointer;float:right;font-size: 20px;font-family: Assistant-Bold; letter-spacing: 2px;color:black" value="+ ADD A NEW ADMIN">
	</div>

	<div class="ui container" style="padding-top: 1%">
		<div class="ui items">


				<?php
					foreach ($row as $r) {
						echo "<div class='item' id='container$r->id'>
						    <div class='image' style='width: 110px;height: 110px'>
						    	<img src='<?php echo base_url();?>assets/images/1.jpg' class='visible content'>
						    </div>
						    <div class='content'>
						    	<div class='header' style='font-size: 20px;font-family: Assistant-Bold;'>$r->name</div>
						    	<div class='meta' style='font-size: 20px;font-family: Assistant-Light;margin-top:-.3%'>
						        	<span>@$r->id</span>
						    	</div>
						    	<div class='bottom aligned content'>
									<input class='remove' type='submit' style='border: none;background-color: Transparent;cursor:pointer;color: black;font-size: 15px; float: right; font-family: Assistant-Bold; letter-spacing: 3px;' id='$r->id' value='REMOVE ADMIN'>
								</div>
						    </div>
						</div>";
					}
				?>


				
			</div>		
	</div>
<br><br><br><br><br><br>
</div>
</div>


<script type="text/javascript">
	$(".remove").click(function(event) {
			var a = "true";
			var id = this.id;
			$("#container"+id).remove();
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/admin_accounts/',
				dataType: 'json',
				data: {remove: a, id: id},
				complete: function() {
					alert('Admin '+a+' removed.');
					 //window.location.href = window.location.href;
				}
			});
		});

	$('#add').click(function(e) {
			$('#addform').toggle();
		});

	$('.add').click(function(e) {
			var a = "true";
			var n = $("#name").val();
			var h = $("#handle").val();
			var p = md5($("#handle").val());
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/admin_accounts/',
				dataType: 'json',
				data: {add: a, n:n, h:h, p:p},
				complete: function() {
					alert('Admin '+h+' added.');
					window.location.href = window.location.href;
				}
			});
		});


</script>