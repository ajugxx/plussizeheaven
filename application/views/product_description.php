<style type="text/css">
	img.main {
		width:300px;
      	height:400px;
      	padding-bottom: 2px;
	}

	.text {
		line-height: 1.1;
	}

	.slick-prev {
    left: 3%; !important;
    z-index: 1;
  }
  .slick-next {
    right: 3%;
  }
	
	

</style>

<br><br>

 <script type="text/javascript">
  $(document).on('ready', function () {
    $(".regular").slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: false,
      dots: false,
      arrows:true,
      infinite: true
      });
    });

  $('.slider').glide();

</script>


<div class="ui container">
	<div class="ui double stackable grid">
		
		<div class="ui row">

			<div class="ui five wide column"> 
				<?php  
					/*	product images 
					*	assets/images/CATEGORY/PRODUCTNAME/n.jpg 			
					*/

					echo "<img class='ui fluid image' src='" . base_url() . "assets/images/PH/1.jpg'>";

					// get no of variations ($varations_count): SELECT COUNT(*) FROM variations WHERE [product_code = ]	
				?>

				<section class="regular slider">
					<?php 
						// ok --> $x
						for ($x = 1; $x <= 10 ; $x++) {
							$pic = 11 % $x;
							echo "<img src='" . base_url() . "assets/images/PH/$pic.jpg' style='width:50px;height:80px;padding:1px;padding-top:3px'>";
						}
					?> 
				</section>
			</div>

			<div class="ui eleven wide column" style=" position:relative">
					<div class="ui container">
						<div class="text" style="font-family: Assistant-ExtraBold;font-size: 32px;letter-spacing: 5px;"> 
							<?php echo $desc[0]->name?>
						</div>

						<i class="yellow star  icon"></i>
						<i class="yellow star  icon"></i>
						<i class="yellow star  icon"></i>
						<i class="star icon"></i>
						<i class="star icon"></i>

						<div class="text" style="font-family: Assistant-ExtraLight;font-size: 15px;letter-spacing: 5px;display: inline-block;"> 
							(<?php echo $rev_count[0]->c; ?>) 
						</div>


						<br><br>
						<div class="text" style="font-family: Assistant-SemiBold;font-size: 15px;letter-spacing: 5px;"> 
							PHP  <?php echo number_format($desc[0]->price,2); ?> / pc
							 <input type="hidden"  id="price" value="<?php echo $desc[0]->price;?>"> 
						</div>

					</div>
				<br>
				<div class="ui container" >
						<div class="ui mini transparent input">
						  <div class="ui basic button" id="decrease" onclick="dec()"> 
						    <center> <i class="minus icon"></i></center>
						  </div>
						  <input type="text" id="pieces" value="1" size="5%" style="text-align: center;" onchange="inputChange()">
						  <div class="ui basic button" id="increase" onclick="inc()">
						    <center> <i class="plus icon"></i></center>
						  </div>
						</div>					
						<div class="text" style="font-family: Assistant-Light;font-size: 15px;letter-spacing: 5px;display: inline-block;"> 
							PIECE/S 
						</div>

						<br>
						<!-- <br> -->

						<!-- <div class="text" style="font-family: Assistant-Light;font-size: 15px;letter-spacing: 5px;"> SELECT VARIANT: </div>		
						<div class="ui basic buttons">
						  <div class="ui button">RED</div>
						  <div class="ui button">BLUE</div>
						  <div class="ui button">PINK</div>
						</div>

						<br><br>
						<div class="text" style="font-family: Assistant-Light;font-size: 15px;letter-spacing: 5px;"> SELECT SIZE: </div>		
						<div class="ui basic buttons">
						  <div class="ui button">SMALL</div>
						  <div class="ui button">MEDIUM</div>
						  <div class="ui button">LARGE</div>
						</div>
 -->
						<br>
						<!-- <br> -->

						<div class="text" data-bind="value: total" style="font-family: Assistant-Light;font-size: 20px;letter-spacing: 5px;display: inline-block;"> 
							TOTAL: 
						</div>
						<span  style="font-family: Assistant-Bold;font-size: 20px;letter-spacing: 5px;"> PHP  <span id="total"></span></span>

				</div>			
						<div class="ui container" style="position:absolute;bottom:2.5%;right: 2.5%;text-align: right">
							<div  style="font-family: Assistant-Bold;font-size: 20px;letter-spacing: 5px; line-height: 1.7">
							<!-- <a href="#" class="normal">BUY NOW</a><BR> -->
							<a href="#" class="normal">ADD TO CART</a><BR>
							<a href="#" class="normal">ADD TO WISHLIST</a><BR>
						</div>
						</div>
			</div>

		</div>		

			<div class="ui accordion">
				<br>
				<div id="desctitle" class="active title" onclick="toggledesc()">  
					<i class="dropdown icon"></i> <div class="accordiontitle"> PRODUCT DETAILS </div> 
				</div>
				<div id="desccontent" class="active content" style="margin-left: 2%;font-family: Assistant-Light;font-size: 100%	;letter-spacing: 2px;"> 
					<?php echo $desc[0]->description?><br>
						<br>SIZE RANGE: <?php $desc[0]->sizerange?>
						<br>COLORS: <?php $desc[0]->colors?>
						<br>MATERIAL: <?php $desc[0]->material?>
						<br>MADE IN: <?php $desc[0]->madein?>
				</div>

				<div id="reviewtitle" class="title" onclick="togglerev()"> 
					<i class="dropdown icon"></i> <div class="accordiontitle">REVIEWS</div>
				</div>
				<div id="reviewcontent" class="content" style="margin-left: 2%">
					<div class="ui container">
						<div class="ui grid">

							<?php
								foreach ($rev as $r) {
								 	echo "<div class='ui row'>
										<div class='ui two wide column'> 
											<img class='ui fluid image' src='<?php echo base_url()?>assets/images/users/arianne.jpg'>
											<div style='font-family: Assistant-Light;font-size: 100%;letter-spacing: 2px'> 
												BOUGHT ON:<br>
												$r->time_stamp
											</div>
										</div>
										<div class='ui fourteen wide column'>
											<div class='ui row' style='font-family: Assistant-ExtraBold;font-size: 100%;letter-spacing: 2px'> 
												$r->username
											</div>
											<div class='ui row' style='font-family: Assistant-ExtraBold;font-size: 100%;letter-spacing: 2px'>
												$r->title
											</div>
											<div class='ui row' style='font-family: Assistant-Light;font-size: 100%;letter-spacing: 2px;'>
												$r->body
											</div>
										</div>
									</div><br>";
								 } 
							?>



						</div>
					</div>
				</div>

				<div id="inqtitle" class="title" onclick="toggleinq()"> 
					<i class="dropdown icon"></i><div class="accordiontitle">INQUIRIES</div>
				</div>
				<div id = "inqcontent" class="content" style="margin-left:2%"> 
					
					<?php
						foreach ($inq as $r) {
							echo "<div class='ui container'>
								<div class='row'> 
									<div  style='font-family: Assistant-ExtraBold;font-size: 100%;letter-spacing: 2px'>  $r->username </div>
									<div  style='font-family: Assistant-Light;font-size: 100%;letter-spacing: 2px;'>$r->sent</div>
									<div  style='font-family: Assistant-Light;font-size: 100%;letter-spacing: 2px; padding-top: .4%'>
										$r->body
									</div>
								</div><br>";
						}
					?>


				</div>

			</div>
	</div></div></div>
<br><br>

</div>

<script type="text/javascript">

	var initial =  document.getElementById("price").value;
	document.getElementById('total').innerHTML = initial;

	function inputChange() {
		var limit = 1000; 
		var current =  document.getElementById("pieces").value;
		if (current < 1)
			document.getElementById("pieces").value = 1;
		else if (current > limit)
			 document.getElementById("pieces").value = limit;

		var price = document.getElementById("price");
		var total = document.getElementById("total");

		total.innerHTML = (parseInt(document.getElementById("pieces").value) * (price.value)).toFixed(2);
	}

	function toggledesc() {
    	var element = document.getElementById("desccontent");
    	var wrap = document.getElementById("desctitle");
    	if (document.getElementById("desccontent").classList.contains("active")) {
    		element.classList.remove("active");
    		wrap.classList.remove("active");
    	}
    	else {
    		element.classList.add("active");
    		wrap.classList.add("active");
    	}
	}

	function togglerev() {
    	var element = document.getElementById("reviewcontent");
    	var wrap = document.getElementById("reviewtitle");
    	if (document.getElementById("reviewcontent").classList.contains("active")) {
    		element.classList.remove("active");
    		wrap.classList.remove("active");
    	}
    	else {
    		element.classList.add("active");
    		wrap.classList.add("active");
    	}
	}

	function toggleinq() {
    	var element = document.getElementById("inqcontent");
    	var wrap = document.getElementById("inqtitle");
    	if (document.getElementById("inqcontent").classList.contains("active")) {
    		element.classList.remove("active");
    		wrap.classList.remove("active");
    	}
    	else {
    		element.classList.add("active");
    		wrap.classList.add("active");
    	}
	}

	function dec() {
		var limit = 1000; //avaialable stocks
		var current = document.getElementById("pieces").value;
		if (current > limit) {document.getElementById("pieces").value = parseInt(limit);}
		else if (current > 1) {document.getElementById("pieces").value = parseInt(current) - 1;}
		else {}

		var price = document.getElementById("price");
		var total = document.getElementById("total");

		total.innerHTML = (parseInt(document.getElementById("pieces").value) * (price.value)).toFixed(2);
	}

	function inc() {
		var limit = 1000; //avaialable stocks
		var current = document.getElementById("pieces").value;
		if (current < limit) document.getElementById("pieces").value = parseInt(current) + 1;

		var price = document.getElementById("price");
		var total = document.getElementById("total");

		total.innerHTML = (parseInt(document.getElementById("pieces").value) * (price.value)).toFixed(2);
	}
	
</script>