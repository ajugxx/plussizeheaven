<style type="text/css">
    tr:hover {
          background-color: #FFEF00;
        }

    thead th {
        position: sticky;
        position: -webkit-sticky;
        top: 0;
        background: white;
        z-index: 10;
    }

    img {
        height: 100px;
        width: 100px;
    }
</style>

<div class="ui thirteen wide column" style="padding-right: 3%">
<div class="ui fluid container" style="margin-top: -3%">

    <div style="font-family: Assistant-ExtraBold; font-size: 300%; letter-spacing: 4%; text-align: right;padding-top: 2.5%">
        RECENT ACTIVITY
    </div>

    <div class="ui secondary pointing menu">
        <div class="right menu">
            <a class="item" href="<?php echo site_url();?>/recent_activity/unprocessed_orders">
                UNPROCESSED ORDERS
            </a>
              
            <a class=" item" href="<?php echo site_url();?>/recent_activity/unshipped_orders">
                UNSHIPPED ORDERS
            </a>    
              
            <a class="item" href="<?php echo site_url();?>/recent_activity/shipped_orders">
                SHIPPED ORDERS
            </a>

            <a class="item" href="<?php echo site_url();?>/recent_activity/unresolved_inquiries">
                UNRESOLVED INQUIRIES
            </a>

            <a class="active item" href="#">
                MESSAGES
            </a>  
        </div>  
    </div>
</div>

<div class="ui fluid container" style="overflow-y: scroll;height: 50%;max-height: 50%;padding-bottom: 10%">


        <table class="ui small compact celled table" id="table">
            <thead>
                <tr>
                    <th class="one wide">DATE</th>
                    <th class="three wide">EMAIL</th>
                    <th class="three wide">NAME</th>
                    <th class="two wide">CATEGORY</th>
                    <th class="six wide">MESSAGE TITLE</th>
                    <th class="one wide">READ</th>

                </tr>
            </thead>

            <tbody>


               <?php
                foreach ($row as $r) {
                    echo " <tr id='$r->tag'>
                            <td> $r->sent</td>
                            <td> $r->email</td>
                            <td> $r->name</td>
                            <td> $r->category</td>
                            <td> $r->title</td>
                            <td>"; 
                    
                    if ($r->replied == 0)            
                                echo "<button class='read ui button' id='$r->tag'>
                                    seen
                                </button>";
                    else 
                        echo "<button class='ui disabled button'>
                                    seen
                                </button>";

                    echo "</td>
                        </tr>";
                }
               ?>



            </tbody>
        </table>

        <p id="hello">

    </div>


<div class="ui fluid container" id="none">
    <div class="title">MESSAGE</div>
</div>

<?php
    foreach ($row as $r) {
        echo "<div class='ui fluid container' id='container$r->tag' style='display:none'>
                <div class='title'>MESSAGE</div>
                <div class='ui grid'>
                    <div class='row' style='word-wrap:break-word'>
                        <div class='ui seven wide column'>
                            <div class='text'> 
                                $r->sent <br>
                                $r->email <br>
                                $r->name <br>
                                <br>
                                MESSAGE CATEGORY: $r->category<br><br>
                            </div>
                            <div class='subheader'> 
                                $r->title </div>
                            <div class='text'> 
                                $r->body
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>";
    }
?>

<br><br><br><br>

</div>
</div>



<script type="text/javascript">
    var active = "#none";
    $("#table tr").click(function(event) {
        $(active).toggle();
        active = "#container" + this.id;
        $(active).toggle();
    });

    $(".read").click(function(event) {
            var a = "true";
            var tag = this.id;
            document.getElementById(tag).classList.add('disabled');
            jQuery.ajax({
                type: "POST",
                url: '<?php echo site_url();?>/recent_activity/messages',
                dataType: 'json',
                data: {read:a, tag: tag},
                complete: function() {
                        //alert(a + " " + tag + " " + reply+'Resolved.');
                        //window.location.href = window.location.href;
                }
            });
        });


    $(".cancel").click(function(event) {
        window.location.href = window.location.href;
    });

    // function getData(row) {
    //  var order_ID = document.getElementById("table").rows[row.rowIndex].cells[0].innerHTML;
    //  document.getElementById("hello").innerHTML = order_ID;
    // }
</script>