<?php
/*  HOME: Website's main page 
 *  SECTIONS  <-->  IMAGE SOURCE
 *  slideshow banner  banner        (banners, collections, horizontal images)
 *  featured      featured      (individual items)
 *  on sale       sale          (left square header + right side collage)
 *  new arrival     new         (right square header + left side collage)
*/
?>

<body>

  <script type="text/javascript">
  $(document).on('ready', function () {
    $(".regular").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 1000,
      dots: true,
      arrows:true,
      infinite: true
      });
    });

  $('.slider').glide();

</script>

<style type="text/css">
  .slick-prev {
    left: 3%; !important;
    z-index: 1;
  }
  .slick-next {
    right: 3%;
  }
   .overlay {
    transition: .5s ease;
    opacity: 0;
    position: absolute;
    transform: translate(60%, -300%);
    text-align: center;
  }

  .overlaysale {
    transition: .5s ease;
    opacity: 0;
    position: absolute;
    transform: translate(20%, -230%);
    text-align: center;
  }

  .card:hover .item {
    opacity: 0.4;
  }

  .card:hover .overlay {
    opacity: 1;
  }

   .card:hover .overlaysale {
    opacity: 1;
  }


  .content {
    text-align: center;
    line-height: 1.2;
  }
</style>

    <div id="body_content" style="margin-left:-7%;margin-right: -7%;margin-top:-1%">

      <?php echo $message;?>
      <script type="text/javascript">
        $('.message .close').on('click', function() { $(this).parent().hide(); });
      </script>

        <section class="regular slider">
          <?php
            for ($x = 1; $x <=4 ; $x++) {
              echo "<div> <img class='ui fluid image' src='" . base_url() . "assets/images/banner/" . $x . ".jpg'></div>";
            }
          ?>
        </section>
        <br><br>

        <div class="ui fluid container">
          <div class="ui stackable grid">
            <div class="ui row">
              <div class="ui column">
                <div class="text homebody" style="font-family: Assistant-Bold;font-size: 25px;letter-spacing: 20px;">
                  <center>WHAT'S HOT</center>
                </div>
              </div>
            </div>
            <div class="ui row" style="background-color: pink">
              <div class="ui column" style="text-align: center;">
                <div class='stackable cards'>
<?php 
  $cnt = 1;
  foreach($row_hot as $item){
    echo "
      <div class='card' style='display: inline-block; padding-bottom:5%'> 
        <a href='" . site_url() . "/product_description?code=$item->code'> 
          <img class='hot' src='" . base_url() . "assets/images/PH/$cnt.jpg'> 
        </a> 
        <div class='overlay'> 
          <button id='$item->code' class='favorite ui black mini button' style='opacity:.7;margin-bottom:5%'><div style='opacity:1'>ADD TO WISHLIST</div></button> <br>
          <button id='$item->code' class='tocart ui black mini button' style='opacity:.7;''><div style='opacity:1'>ADD TO CART</div></button> 
        </div> 
        <div class='home content'>$item->name<br>

        <i class='small orange star  icon'></i>
        <i class='small orange star  icon'></i>
        <i class='small orange star  icon'></i>
        <i class='small star icon'></i>
        <i class='small star icon'></i>

        <br>$item->price</div> 
      </div> ";
      if ($cnt == 4) break;
      $cnt = $cnt+1;
  } 
?>
</div>
              </div>
            </div>

            <div class="row" style="background-color: pink; padding-bottom: 30px;">
              <div class="ui column" style="text-align: center;"> 
                <a href="<?php echo site_url();?>/catalog?c=hot items" class="button  button-plain button-border button-rectangle button-small" style="font-family: Assistant-Bold;font-size: 15px;letter-spacing: 5px;"> BROWSE ALL ITEMS </a>
             </div>
            </div>
          </div>
        </div>

         <br><br><br>
                
                
                <div class="ui container">
                  <div class="ui stackable grid">
                  
                    <div class="ui row">
                      <div class="ui bottom aligned fluid column" style="text-align: right;">
                        <div class="text homebody" style="font-family: Assistant-ExtraBold;font-size: 90px;letter-spacing: 20px;">
                          FAVORITES
                        </div>
                      </div>
                    </div>
                  
                    <div class="ui five column row">
                        <?php 
                          $cnt = 1;
                          foreach($row_faves as $item){
                            echo "<div class='ui three wide centered column'>";
                           echo "
      <div class='card' style='display: inline-block; padding-bottom:5%'> 
        <a href='" . site_url() . "/product_description?code=$item->code'> 
          <img class='sale' src='" . base_url() . "assets/images/PH/$cnt.jpg'> 
        </a> 
        <div class='overlaysale'> 
          <button id='$item->code' class='favorite ui black mini button' style='opacity:.7;margin-bottom:5%'><div style='opacity:1'>ADD TO WISHLIST</div></button> <br>
          <button id='$item->code' class='tocart ui black mini button' style='opacity:.7;''><div style='opacity:1'>ADD TO CART</div></button> 
        </div> 
        <div class='home content'>$item->name<br>

        <i class='small orange star  icon'></i>
        <i class='small orange star  icon'></i>
        <i class='small orange star  icon'></i>
        <i class='small star icon'></i>
        <i class='small star icon'></i>

        <br>$item->price</div> 
      </div> ";
                            echo "</div>";
                            if ($cnt == 5) break;
                            $cnt = $cnt+1;
                          }
                        ?>
                    </div>

                    <div class="ui five column row">
                        <?php 
                           $cnt = 1;
                          foreach($row_faves as $item){
                            echo "<div class='ui three wide centered column'>";
                            echo "
      <div class='card' style='display: inline-block; padding-bottom:5%'> 
        <a href='" . site_url() . "/product_description?code=$item->code'> 
          <img class='sale' src='" . base_url() . "assets/images/PH/$cnt.jpg'> 
        </a> 
        <div class='overlaysale'> 
          <button id='$item->code' class='favorite ui black mini button' style='opacity:.7;margin-bottom:5%'><div style='opacity:1'>ADD TO WISHLIST</div></button> <br>
          <button id='$item->code' class='tocart ui black mini button' style='opacity:.7;''><div style='opacity:1'>ADD TO CART</div></button> 
        </div> 
        <div class='home content'>$item->name<br>

        <i class='small orange star  icon'></i>
        <i class='small orange star  icon'></i>
        <i class='small orange star  icon'></i>
        <i class='small star icon'></i>
        <i class='small star icon'></i>

        <br>$item->price</div> 
      </div> "; 
                            echo "</div>";
                            if ($cnt == 4) break;
                            $cnt = $cnt+1;
                          }
                        ?>
                        <div class='ui three middle aligned wide column' style="text-align: center;">
                          <a href="<?php echo site_url();?>/catalog?c=favorites">
                          <button class="button button-rectangle button-border button-small"> 
                             <div class='text homebody' style='font-family: Assistant-ExtraBold;font-size: 15px;letter-spacing: 5px;'>VIEW ALL</div>
                          </button></a>
                        </div>
                    </div>

                  </div>
                </div>

                <br><br><br>

                <div class="ui fluid container">
          <div class="ui stackable grid">
            <div class="ui row">
              <div class="ui column">
                <div class="text homebody" style="font-family: Assistant-Bold;font-size: 25px;letter-spacing: 20px;">
                  <center>NEW ARRIVAL</center>
                </div>
              </div>
            </div>
            <div class="ui row" style="background-color: lightblue">
              <div class="ui column" style="text-align: center;">
                <div class='stackable cards'>
<?php 
  $cnt = 1;
   foreach($row_new as $item){
    echo "
      <div class='card' style='display: inline-block; padding-bottom:5%'> 
        <a href='" . site_url() . "/product_description?code=$item->code'> 
          <img class='hot' src='" . base_url() . "assets/images/PH/$cnt.jpg'> 
        </a> 
        <div class='overlay'> 
          <button id='$item->code' class='favorite ui black mini button' style='opacity:.7;margin-bottom:5%'><div style='opacity:1'>ADD TO WISHLIST</div></button> <br>
          <button id='$item->code' class='tocart ui black mini button' style='opacity:.7;''><div style='opacity:1'>ADD TO CART</div></button> 
        </div> 
        <div class='home content'>$item->name<br>

        <i class='small orange star  icon'></i>
        <i class='small orange star  icon'></i>
        <i class='small orange star  icon'></i>
        <i class='small star icon'></i>
        <i class='small star icon'></i>

        <br>$item->price</div> 
      </div> ";
      if ($cnt == 4) break;
        $cnt = $cnt+1;
  } 
?>
</div>
              </div>
            </div>

            <div class="row" style="background-color: lightblue; padding-bottom: 80px;">
              <div class="ui column" style="text-align: center;">  
                <a href="<?php echo site_url();?>/catalog?c=new arrival" class="button  button-plain button-border button-rectangle button-small" style="font-family: Assistant-Bold;font-size: 15px;letter-spacing: 5px;">SEE ALL NEW ITEMS</a> 
              </div>
            </div>
          </div>
        </div>
        </div>
</body>

<script type="text/javascript">
  
  $(".favorite").click(function(event){
    var fave = "true";
    var c = this.id;
    //alert(c);
    
    jQuery.ajax({
        type: "POST",
        url: '<?php echo site_url();?>/profile/save',
        dataType: 'json',
        data: {fave: fave, code:c},
        complete: function() {
            alert('Added to wishlist.');
        }
      });
  });

  $(".tocart").click(function(event){
    var tocart = "true";
    var c = this.id;
    //alert(c);
    //var q = ("#quant").val();
    //alert(c);
    jQuery.ajax({
        type: "POST",
        url: '<?php echo site_url();?>/profile/save',
        dataType: 'json',
        data: {tocart: tocart, code:c},
        complete: function() {
          alert('Added to cart.');
        }
      });
  });
</script>