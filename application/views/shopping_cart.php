

<br><br>

<div class="ui container">
	<div class="ui grid">

		<div class="row">
			<div class="ui thirteen wide column">
				<div class='text' style="font-family: Assistant-ExtraBold;font-size: 70px;letter-spacing: 5px;">SHOPPING CART</div>		
			</div>
			<div class="ui bottom aligned three wide column" style="text-align: right">
				<div class='text' style="font-family: Assistant-Light;font-size: 20px;letter-spacing: 2px;">
					<form method="POST">
						<input type="submit" name="checkout" value="CHECK OUT" style="border: none;background-color: Transparent;cursor:pointer;">
					</form>
				</div>
			</div>
		</div>

		<div class="row" style="margin-top: -3.5%"> 
			<div class="ui sixteen wide column">
				<i class="trash alternate outline icon"></i> 
				<input type="submit" style="border: none;background-color: Transparent;cursor:pointer;" id="clearcart" name="clear" value="CLEAR CART">
			</div>
		</div>

		<br><br>

		<!-- <p id="okay" value="<?php //echo $okay;?>"> -->
			<!-- <input value="<?php //echo $okay;?>" id="okay"> -->

		<?php
			foreach ($row as $i) {		// for each item in my shoppping cart
				echo "<div class='row'>
						<div class='ui two wide column'>
							<img class='ui fluid image' src='". base_url() ."assets/images/ph.jpg'>
						</div>
						<div class='fourteen wide column'>
							<div class='row'>
								<div style='font-family: Assistant-Bold; font-size: 15;letter-spacing: 2px;padding-bottom: 1%'>
									$i->name
								</div>
							</div>
							<div class='row'>
								<div class='ui seven wide column'>
									<div style='font-family: Assistant-Light; font-size: 15;letter-spacing: 2px;'>VARIANT: <br>
									<div class='ui mini transparent input'>
										  <div class='ui basic button'  onclick='dec(this)' > 
										    <center> <i class='minus icon'></i></center>
										  </div>
										  <input type='text' value='$i->q' size='5%' style='text-align: center;'>
										  <div class='ui basic button' onclick='inc(this)'>
										    <center> <i class='plus icon'></i></center>
										  </div>
									</div> PIECES</div>
								</div>
								<div class='ui seven wide column' style='text-align: right'>
									<div style='font-family: Assistant-Bold; font-size: 15;letter-spacing: 2px;'>$i->price<br>
									REMOVE ITEM</div>
								</div>
							</div>
						</div>
					</div>";
			}
		?>
		

		<div class="row" style="padding-top: 7%">
			<div class="ui bottom aligned three wide column">
			</div>
			<div class="ui thirteen wide column" style="text-align: right;">
				<div style="font-family: Assistant-ExtraBold; font-size: 35;letter-spacing: 2px;">
					<!-- TOTAL: <br> --> 
					<form method="POST">
						<input type="submit" name="checkout" value="CHECK OUT" style="border: none;background-color: Transparent;cursor:pointer;">
					</form>
				</div>
			</div>
		</div>	




	</div>
</div>

<br><br>
<br><br>
<br><br>

<script type="text/javascript">
	function dec(e) {
		//alert(e.getAttribute("name"));
		var q = e.getAttribute("name");

		var limit = 1000; //avaialable stocks
		var current = document.getElementById(q).value;
		if (current > limit) {document.getElementById(q).value = parseInt(limit);}
		else if (current > 1) {document.getElementById(q).value = parseInt(current) - 1;}
		else {}
	}

	function inc(e) {
		var q = e.getAttribute("name");
		var limit = 1000; //avaialable stocks
		var current = document.getElementById(q).value;
		if (current < limit) document.getElementById(q).value = parseInt(current) + 1;
	}

		$("#clearcart").click(function(event) {
			var clearcart = "true";
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/my_cart/',
				dataType: 'json',
				data: {clear: clearcart},
				complete: function() {
						$('#clearcart').val("CLEARED");
				}
			});
		});
</script>