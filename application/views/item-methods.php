<script type="text/javascript">
	$("#favorite").click(function(event) {
			var code = event.attr('name');
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/profile/save',
				dataType: 'json',
				data: {favorite: code},
				complete: function() {
						event.val("ADDED TO WISHLIST");
				}
			});
		});	

	$("#toCart").click(function(event) {
			var code = event.attr('name');
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/profile/save',
				dataType: 'json',
				data: {toCart: code},
				complete: function() {
						event.val("ADDED TO CART");
				}
			});
		});	
</script>