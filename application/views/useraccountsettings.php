<link rel="stylesheet" type="text/css" href="/semantic/dist/semantic.min.css">
    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous"></script>
    <script src="/semantic/dist/semantic.min.js"></script>

    <style type="text/css">
    	body, html {
				margin-left:3%;
				margin-right:3%;
				margin-bottom:1%;
			}
    </style>

<body>
	<div onscroll="pagescroll()">


			<div class="ui container" style="padding-top: 5%">
				<div class="ui items">
					<div class="item">
					    <button class="ui basic button" style="margin-right: 20px; width:200px;height: 200px;font-size: 17px;color:black;letter-spacing: 2px;font-family: Assistant-Light;">
							UPLOAD DISPLAY PICTURE
						</button>
					</div>
				</div>
			</div>

			
			<!-- <div class="ui container">
				<h1 style="padding-top:5%;font-size: 200%;letter-spacing: 3px;font-family: Assistant-Bold;"> 
					CONTACT INFORMATION
				</h1>
				<form action='#' method='post' accept-charset='UTF-8'>
					<div class="ui form contact">
						<div class="inline field">
							<label style="margin-bottom: 10px; font-size: 15px;color:black;letter-spacing: 2px;font-family: Assistant-Light;">NAME:</label>
							<input name="name" type="text" size="40" value="<?php//echo $name;?>">
						</div>
						<div class="inline field">
							<label style="margin-bottom: 10px; font-size: 15px;color:black;letter-spacing: 2px;font-family: Assistant-Light;">CONTACT NUMBER:</label>
							<input name="mobile" type="text" size="40" value="<?php //echo $mobile;?>">
						</div>
						<div class="inline field">
							<label style="margin-bottom: 10px; font-size: 15px;color:black;letter-spacing: 2px;font-family: Assistant-Light;">EMAIL:</label>
							<input name="email" type="text" size="40" value="<?php //echo $email;?>">
						</div>
						<div style="text-align: right;">
			                  <div class="ui buttons">
			                        <button class="ui button">CANCEL</button>
			                        <div class="or"></div>
			                        <div class="ui positive submit button">SAVE CHANGES</div>
			                      </div>
			            </div>
			<a name="CHANGE_PASSWORD"></a>
			            <div class="ui error message"></div>
					</div>
				</form>
			</div> -->


			
			<div class="ui container">
					<?php echo $notice;?>
			</div>

			<div class="ui container segment">
				<h1 style="font-size: 200%;letter-spacing: 3px;font-family: Assistant-Bold;">CHANGE PASSWORD</h1>

				<form action="#CHANGE_PASSWORD" class="ui form password" action='#' method='post' name="change" accept-charset='UTF-8' onsubmit="log()">
		                <div class="required field">
		                    <label>CURRENT PASSWORD:</label>
		                    <input type="password" name="current" id='pwd'>
		                </div>
		                 
		                <input type="hidden" name="validate" />

		                <div class="required field">
		                    <label>NEW PASSWORD:</label>
		                    <input type="password" id="new" name="new">
		                </div>

		                <input type="hidden" name="take" />

		                <div class="required field">
		                    <label>REPEAT NEW PASSWORD:</label>
		                    <input type="password" name="repeatnew">
		                </div>
		                 
		                <div style="text-align: right;">
		                  <div class="ui buttons">
		                        <button class="ui button">CANCEL</button>
		                        <div class="or"></div>
		                        <<input type="submit" value="CHANGE PASSWORD" class="ui positive submit button" name="change_pw" />
		                      </div>
		                </div>
		                 
		                <div class="ui error message"></div>
		        </form>
			</div>

		</div>
	
	<br><br><br><br><br><br><br>
</body>


<script type="text/javascript">
	function log() {
		document.change.validate.value = md5(document.getElementById('pwd').value);
		document.change.take.value = md5(document.getElementById('new').value);
	}

	$('.submit.button').api({
    	beforeSend: function(settings) {
        	return false;
    	}
	});

	$('.ui.form.password')
          .form({
            fields: {
              password: {
                identifier: 'current',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'PLEASE ENTER YOUR CURRENT PASSWORD'
                  }
                ]
              },
              password2: {
                identifier: 'new',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'PLEASE ENTER YOUR NEW PASSWORD'
                  },
                  {
                    type   : 'match[repeatnew]',
                  }
                ]
              },
              password3: {
                identifier: 'repeatnew',
                rules: [
                  {
                    type   : 'match[new]',
                    prompt : "NEW PASSWORD DID NOT MATCH"
                  }
                ]
              }
            }
          })
        ;

        $('.ui.form.contact')
          .form({
            fields: {
              name: {
                identifier: 'name',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'NAME CANNOT BE BLANK'
                  }
                ]
              },
              mobile: {
                identifier: 'mobile',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'CONTACT NUMBER CANNOT BE BLANK'
                  }
                ]
              },
              email: {
                identifier: 'email',
                rules: [
                  {
                    type   : 'empty',
                    prompt : "EMAIL CANNOT BE BLANK"
                  }
                ]
              }
            }
          })
        ;
</script>


