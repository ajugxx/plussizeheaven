<html>
  <head>
    <title>PLUS SIZE HEAVEN</title>

    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/semantic/dist/semantic.min.css">
    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous">
    </script>
    <script src="<?php echo base_url();?>assets/semantic/dist/semantic.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/md5.min.js"></script>
    

    <link href="//cdn.muicss.com/mui-0.9.41/css/mui.min.css" rel="stylesheet" type="text/css" />
    <script src="//cdn.muicss.com/mui-0.9.41/js/mui.min.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/slick/slick-theme.css"/>

    <!-- Buttons core css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/buttons.css">
    <!-- Only needed if you want font icons -->
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" rel="stylesheet" />

    <style type="text/css">
      body, html {
        margin-left:3%;
        margin-right:3%;
        margin-bottom:1%;
      }

      .navbar {
        margin-left:-7%;
        margin-right:-7%;
        margin-bottom: 1%;
      }

      .navitemsLeft {
        margin-left:7%;
      }

      .navitemsRight {
        margin-right:5%;
      }

      @font-face {
        font-family: "Assistant-Bold";
        src: url("<?php echo base_url();?>assets/fonts/Assistant-Bold.ttf");

      }

      @font-face {
        font-family: "Assistant-ExtraBold";
        src: url("<?php echo base_url();?>assets/fonts/Assistant-ExtraBold.ttf");

      }

      @font-face {
        font-family: "Assistant-ExtraLight";
        src: url("<?php echo base_url();?>assets/fonts/Assistant-ExtraLight.ttf");

      }

      @font-face {
        font-family: "Assistant-Light";
        src: url("<?php echo base_url();?>assets/fonts/Assistant-Light.ttf");
      }

      @font-face {
        font-family: "Assistant-Regular";
        src: url("<?php echo base_url();?>assets/fonts/Assistant-Regular.ttf");
      }

      @font-face {
        font-family: "Assistant-SemiBold";
        src: url("<?php echo base_url();?>assets/fonts/Assistant-SemiBold.ttf");
      }

      .row.navbar {
        line-height: 1.5;
      }

      .footer {
          float: bottom;
          position: relative;
          left: 0;
          bottom: 0;
          width: 120%;
          height: 45%;
          margin-left:-7%;
          background-color: black;
          color: white;
      }

      .slick-slide {
      height: 500;
    }

    .text.homebody {
      line-height: 0.8;
    }

.cards.home {
    display: inline-block;
    padding-bottom: 7px;
  }

   a:link {color: black;     text-decoration: none;
}      /* unvisited link */
       a:visited {color: black;     text-decoration: none;
}   /* visited link */
       a:hover {color: black;    text-decoration: none;
}     /* mouse over link */
       a:active {color: black;    text-decoration: none;
}    /* selected link */

    img.hot {
      width:300px;
      height:400px;
      padding-top: 20px;
      padding-bottom: 5px;
      padding-right: 20px;
    }

    img.sale {
      width:200px;
      height:280px;
      padding-right: 20px;
      padding-bottom: 5px;x`
    }

    img.new {
      width:250px;
      height:350px;
      padding: 10px;
    }

    .stretch {
    width: 100%;
    display: inline-block;
    font-size: 0;
    line-height: 0;
}

  .home.content {
    text-align: center;
    line-height: 1.2;
    font-family: Assistant-Light;
    font-size: 15px;
    letter-spacing: 3px;">
  }
    </style>


<script type="text/javascript">
  $(document).on('ready', function () {
    $(".regular").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 1000,
      dots: true,
      infinite: true
      });
    });

   $('.ui.sticky')
  .sticky({
    context: '#bodycontent'
  })
;

  $('.slider').glide();



</script>

  </head>

  <div class="ui double stackable grid">
  <div class="ui five wide column" style="margin-right: -80px;">
    <a href="<?php echo base_url();?>">
      <img class="ui medium image" src="<?php echo base_url();?>assets/images/logo.jpg">
    </a>
  </div>

  <div class="one wide bottom aligned column" style = "margin-bottom: 15px;">
    <i class="large blue facebook icon"></i>
    <i class="large brown instagram icon"></i>
  </div>

  <div class="eleven wide column">
    <div class="row navbar" style="height: 25px">
    </div>
    <div class="row navbar" style="text-align: right;font-family: Assistant-Light; letter-spacing: 3px;">
      REGISTER  <i class="large pen square icon"></i> <br>
      SIGN-IN <i class="large angle right icon"></i> <br> <br>
      WISHLIST <i class="large heart outline icon"></i> <br>
      MY CART <i class="large shopping cart icon"></i> <br>
    </div>
  </div>
</div>

  
<div class="ui sticky">
  <div class="navbar">
    <div class="ui large borderless inverted menu" id="menubar">
      <div class="navitemsLeft"></div>
      <a class="item" href="<?php echo site_url();?>/catalog?c=cover-ups" style="padding-right: 40px; font-family: Assistant-Light; letter-spacing: 3px;">
        COVER-UPS
      </a>
      <a class="item" href="<?php echo site_url();?>/catalog?c=dresses" style="padding-right: 40px; font-family: Assistant-Light; letter-spacing: 3px;">
        DRESSES
      </a>
      <a class="item" href="<?php echo site_url();?>/catalog?c=rompers" style="padding-right: 40px; font-family: Assistant-Light;letter-spacing: 3px;">
        ROMPERS
      </a>
      <a class="item" href="<?php echo site_url();?>/catalog?c=summer dresses" style="padding-right: 40px; font-family: Assistant-Light;letter-spacing: 3px;">
        SUMMER DRESSES
      </a>
      <a class="item" href="<?php echo site_url();?>/catalog?c=swimwear" style="padding-right: 40px; font-family: Assistant-Light; letter-spacing: 3px;">
        SWIMWEAR
      </a>
      <div class="right menu">
        <div class="item">
            <div class="ui inverted transparent icon input">
              <input placeholder="Search..." type="text" size="35" style="font-family: Assistant-SemiBold; text-transform:uppercase"><i class="search link icon"></i>
          </div>
        </div>
      </div>
      <div class="navitemsRight"></div> 
    </div>
  </div>
</div>

<?php
/*  HOME: Website's main page 
 *  SECTIONS  <-->  IMAGE SOURCE
 *  slideshow banner  banner        (banners, collections, horizontal images)
 *  featured      featured      (individual items)
 *  on sale       sale          (left square header + right side collage)
 *  new arrival     new         (right square header + left side collage)
*/
?>

<body>
    <div id="bodycontent" style="margin-left:-7%;margin-right: -7%;margin-top:-1%">
        <section class="regular slider">
          <?php
            for ($x = 1; $x <=4 ; $x++) {
              echo "<div> <img class='ui fluid image' src='" . base_url() . "assets/images/banner/" . $x . ".jpg'></div>";
            }
          ?>
        </section>
        <br><br>

        <div class="ui fluid container">
          <div class="ui stackable grid">
            <div class="ui row">
              <div class="ui column">
                <div class="text homebody" style="font-family: Assistant-Bold;font-size: 25px;letter-spacing: 20px;">
                  <center>WHAT'S HOT</center>
                </div>
              </div>
            </div>
            <div class="ui row" style="background-color: pink">
              <div class="ui column" style="text-align: center;">
                <?php
                    for ($x = 1; $x <=4; $x++) {
                      echo "<div class='stackable cards home' style='padding-top:10px;'> <a href='" . site_url() . "/product_description?code=DRS-FLWR-BLK'> <img class='hot' src='" . base_url() . "assets/images/featured_/". $x .".jpg'> <div class='home content'>PRODUCT NAME<br>STAR RATING<br>PHP 500.00</div></a> </div>";
                    }              
                ?>
              </div>
            </div>

            <div class="row" style="background-color: pink; padding-bottom: 30px;">
              <div class="ui column" style="text-align: center;"> 
                <a href="<?php echo site_url();?>/catalog?c=hot items" class="button  button-plain button-border button-rectangle button-small" style="font-family: Assistant-Bold;font-size: 15px;letter-spacing: 5px;"> BROWSE ALL ITEMS </a>
             </div>
            </div>
          </div>
        </div>

         <br><br><br>
                
                
                <div class="ui container">
                  <div class="ui stackable grid">
                  
                    <div class="ui row">
                      <div class="ui bottom aligned fluid column" style="text-align: right;">
                        <div class="text homebody" style="font-family: Assistant-ExtraBold;font-size: 90px;letter-spacing: 20px;">
                          ON SALE 
                        </div>
                      </div>
                    </div>
                  
                    <div class="ui five column row">
                        <?php 
                          for($x=1 ; $x<=5 ; $x++) {
                            echo "<div class='ui three wide centered column'>";
                            echo "<div class='stackable cards home'> <a href='" . site_url() . "/product_description?code=DRS-FLWR-BLK'> <img class='sale' src='" . base_url() . "assets/images/ok.jpg'> <div class='home content'>PRODUCT NAME<br>STAR RATING<br>PHP 500.00</div></a> </div>"; 
                            echo "</div>";
                          }
                        ?>
                    </div>

                    <div class="ui five column row">
                        <?php 
                          for($x=1 ; $x<=4 ; $x++) {
                            echo "<div class='ui three wide centered column'>";
                            echo "<div class='stackable cards home'> <a href='" . site_url() . "/product_description?code=DRS-FLWR-BLK'> <img class='sale' src='" . base_url() . "assets/images/ok.jpg'> <div class='home content'>PRODUCT NAME<br>STAR RATING<br>PHP 500.00</div></a> </div>"; 
                            echo "</div>";
                          }
                        ?>
                        <div class='ui three middle aligned wide column' style="text-align: center;">
                          <a href="<?php echo site_url();?>/catalog?c=on sale items">
                          <button class="button button-rectangle button-border button-small"> 
                             <div class='text homebody' style='font-family: Assistant-ExtraBold;font-size: 15px;letter-spacing: 5px;'>VIEW ALL</div>
                          </button></a>
                        </div>
                    </div>

                  </div>
                </div>

                <br><br><br>

                <div class="ui fluid container">
          <div class="ui stackable grid">
            <div class="ui row">
              <div class="ui column">
                <div class="text homebody" style="font-family: Assistant-Bold;font-size: 25px;letter-spacing: 20px;">
                  <center>NEW ARRIVAL</center>
                </div>
              </div>
            </div>
            <div class="ui row" style="background-color: lightblue">
              <div class="ui column" style="text-align: center;">
                <?php
                    for ($x = 1; $x <=4; $x++) {
                      echo "<div class='stackable cards home' style='padding-top:10px;'> <a href='" . site_url() . "/product_description?code=DRS-FLWR-BLK'> <img class='hot' src='" . base_url() . "assets/images/featured_/". $x .".jpg'> <div class='home content'>PRODUCT NAME<br>STAR RATING<br>PHP 500.00</div></a> </div>";
                    }              
                ?>
              </div>
            </div>

            <div class="row" style="background-color: lightblue; padding-bottom: 80px;">
              <div class="ui column" style="text-align: center;">  
                <a href="<?php echo site_url();?>/catalog?c=new arrival" class="button  button-plain button-border button-rectangle button-small" style="font-family: Assistant-Bold;font-size: 15px;letter-spacing: 5px;">SEE ALL NEW ITEMS</a> 
              </div>
            </div>
          </div>
        </div>
        </div>
</body>

  <div class="footer">
    <div class="ui grid">
        <div class="two wide column"> 
        </div>
        <div class="four wide left aligned column"  style="font-size: 10px;font-family: Assistant-Light; letter-spacing: 3px;">
            <br><br>
          PLUS SIZE HEAVEN <BR><BR>
          SOCIAL MEDIA ACCOUNTS<BR>
        FACEBOOK.COM/PLUSSIZEHEAVEN<BR>
        INSTAGRAM.COM/PLUSSIZEHEAVEN<BR>
        </div>
        <div class="three wide left aligned column"  style="font-size: 10px;font-family: Assistant-Light; letter-spacing: 3px;">
          <br><br>
          COVERUPS<BR>
        DRESSES<BR>
        ROMPERS<BR>
        SUMMER DRESSES<BR>
        SWIMWEAR<BR>
        </div>
        <div class="three wide left aligned column"  style="font-size: 10px;font-family: Assistant-Light; letter-spacing: 3px;">
          <br><br>
          CREATE AN<br>
        LOG-IN TO
        EXISTING<br>
        </div>
        <div class="two wide left aligned column"  style="font-size: 10px;font-family: Assistant-Light; letter-spacing: 3px;">
            <br><br>
          ABOUT US<br>
        CONTACT US<br>
        RETURN PPOLICIES<br>
        PAYMENT METHODS<br>
        SHIPPING OPTIONS<br>
        </div>
    </div>
  </div>
</html>