<!DOCTYPE html>
<html>
<head>
	<title>Plus Size Heaven</title>
</head>

<body>
	<div onscroll="pagescroll()">
		<div id="body_content" style="margin-left:-7%;margin-right: -7%;margin-top:-1%;margin-bottom: 50px;">
			<h1 style="float:right; padding-top:50px;font-size: 50px;font-family: Assistant-Bold; letter-spacing: 5px;margin-right:58px;margin-bottom: 7%">SPECIAL ITEMS</h1>
			<div style="clear: both;margin-left: 60%;padding-bottom: 7%">
				<a style="float:left;margin-right: 50px; font-size: 25px;font-family: Assistant-Bold; letter-spacing: 2px;color:black">FEATURED ITEMS</a>
				<a style="float:left;font-size: 25px;font-family: Assistant-Bold; letter-spacing: 2px;color:pink">ITEMS ON SALE</a>
			</div>

			<h1 style="padding-top:10px;font-size: 35px;font-family: Assistant-Bold; letter-spacing: 2px;margin-left:7%;margin-bottom: 10px">10 ITEMS ARE CURRENTLY ON SALE</h1>

			<div style="clear: both;margin-left: 7%;padding-bottom: 7%">
				<a style="float:left;margin-right: 50%; font-size: 20px;font-family: Assistant-Bold; letter-spacing: 2px;color:black">ADD ITEMS</a>
				<div class="ui input" style="float:left; padding-right: 20px">
					<input placeholder="PRODUCT CODE" type="text" size="35" style="font-family: Assistant-SemiBold;">
					</div>
				<a style="float:left;font-size: 20px;font-family: Assistant-Bold; letter-spacing: 2px;color:black">REMOVE ITEM</a>
			</div>

			<div class="ui items" style="margin-left: 7%;margin-right:  7%;margin-top: 10px;margin-bottom: 5%">
				<div class="item">
				    <div class="image" style="width: 95px;height: 95px">
				    	<img src="<?php echo base_url();?>assets/images/1.jpg" class="visible content">
				    </div>
				    <div class="content">
				    	<div class="header" style="font-size: 9px;font-family: Assistant-Bold;">PRODUCT NAME</div>
				    	<div class="meta" style="font-size: 9px;font-family: Assistant-Light;">
							<span>PRODUCT CODE</span><br>
							<span>START</span><br>
							<span>END</span><br>
							<span>DISCOUNT</span><br>
							<span>ORIGINAL PRICE</span><br>
							<span>SALE PRICE</span><br>
						</div>
						<div class="bottom aligned content">
							<a class="item" style="color: black;font-size: 9px; padding-right: 40px; font-family: Assistant-Bold;">
					    		EDIT ITEM
							</a><br>
							<a class="item" style="color: black;font-size: 9px; padding-right: 40px; font-family: Assistant-Bold;">
					    		REMOVE ITEM
							</a>
						</div>			        
					</div>
				</div>
				<div class="item">
				    <div class="image" style="width: 95px;height: 95px">
				    	<img src="<?php echo base_url();?>assets/images/1.jpg" class="visible content">
				    </div>
				    <div class="content">
				    	<div class="header" style="font-size: 9px;font-family: Assistant-Bold;">PRODUCT NAME</div>
				    	<div class="meta" style="font-size: 9px;font-family: Assistant-Light;">
							<span>PRODUCT CODE</span><br>
							<span>START</span><br>
							<span>END</span><br>
							<span>DISCOUNT</span><br>
							<span>ORIGINAL PRICE</span><br>
							<span>SALE PRICE</span><br>
						</div>
						<div class="bottom aligned content">
							<a class="item" style="color: black;font-size: 9px; padding-right: 40px; font-family: Assistant-Bold;">
					    		EDIT ITEM
							</a><br>
							<a class="item" style="color: black;font-size: 9px; padding-right: 40px; font-family: Assistant-Bold;">
					    		REMOVE ITEM
							</a>
						</div>			        
					</div>
				</div>
				<div class="item">
				    <div class="image" style="width: 95px;height: 95px">
				    	<img src="<?php echo base_url();?>assets/images/1.jpg" class="visible content">
				    </div>
				    <div class="content">
				    	<div class="header" style="font-size: 9px;font-family: Assistant-Bold;">PRODUCT NAME</div>
				    	<div class="meta" style="font-size: 9px;font-family: Assistant-Light;">
							<span>PRODUCT CODE</span><br>
							<span>START</span><br>
							<span>END</span><br>
							<span>DISCOUNT</span><br>
							<span>ORIGINAL PRICE</span><br>
							<span>SALE PRICE</span><br>
						</div>
						<div class="bottom aligned content">
							<a class="item" style="color: black;font-size: 9px; padding-right: 40px; font-family: Assistant-Bold;">
					    		EDIT ITEM
							</a><br>
							<a class="item" style="color: black;font-size: 9px; padding-right: 40px; font-family: Assistant-Bold;">
					    		REMOVE ITEM
							</a>
						</div>			        
					</div>
				</div>
			</div>

		</div>
	</div>
	
</body>
</html>

<!-- <script type="text/javascript">
	$('.ui.sticky')
	  .sticky({
	    context: '#body_content'
	  })
	;
</script> -->