<body>
    	<div class="ui container">
			<h1 style="padding-top:50px;font-size: 300%;font-family: Assistant-ExtraBold;;letter-spacing: 5px;">
				ORDER HISTORY
			</h1>
			<a href="<?php echo site_url();?>/profile" style="color: black;font-size: 12px; padding-top: 7%; font-family: Assistant-Light; letter-spacing: 3px;">
				<i class="meidum caret left icon"></i>
				BACK TO MY PROFILE
			</a>
		</div>

		<div class="ui container" style="margin-top: 5%">
			<div class="mini four ui buttons">
				<button class="ui black button" id="topay"  style="height: 4%">TO PAY</button>
				<button class="ui black basic button" id="pending" style="height: 4%">PENDING</button>
				<button class="ui black basic button" id="completed" style="height: 4%">COMPLETED</button> 			
				<button class="ui black basic button" id="cancelled" style="height: 4%">CANCELLED</button>
			</div>
		</div>

		<div class="ui container" id="topaycontent" style="margin-top: 2%;">
			
			<div class="ui grid">



				<div class="row" style="font-family: Assistant-Regular;font-size: 90%; letter-spacing: 1px">
					<div class="ui four wide column">
						<div style="font-family: Assistant-Bold"> #ORDERIDGOESHERE</div>
						PLACED ON: <br>
						TOTAL AMOUNT: <br><br>
						MAKE A PAYMENT <br>
						<div style="font-size: 80%;padding-top:5%"> CANCEL ORDER</div>
					</div>
					<div class="ui twelve wide column">
						<div class="row" style="font-family: Assistant-ExtraBold;font-size: 90%; letter-spacing: 1px">
							ITEMS
						</div>
						<ul id="items" class="ui list">
							<li>	(1) 
								<a href="<?php echo site_url()?>/category/productcode"> 
									PRODUCT NAME
								</a>
								[SIZE:VARIATION] 
							</li>
							<li>	(1) 
								<a href="<?php echo site_url()?>/category/productcode"> 
									PRODUCT NAME
								</a>
								[SIZE:VARIATION] 
							</li>

						</ul>
					</div>
				</div>

				<div class="ui divider"></div>

				<div class="row" style="font-family: Assistant-Regular;font-size: 90%; letter-spacing: 1px">
					<div class="ui four wide column">
						<div style="font-family: Assistant-Bold"> #ORDERIDGOESHERE</div>
						PLACED ON: <br>
						TOTAL AMOUNT: <br><br>
						MAKE A PAYMENT <br>
						<div style="font-size: 80%;padding-top:5%"> CANCEL ORDER</div>
					</div>
					<div class="ui twelve wide column">
						<div class="row" style="font-family: Assistant-ExtraBold;font-size: 90%; letter-spacing: 1px">
							ITEMS
						</div>
						<ul id="items" class="ui list">
							<li>	(1) 
								<a href="<?php echo site_url()?>/category/productcode"> 
									PRODUCT NAME
								</a>
								[SIZE:VARIATION] 
							</li>
							<li>	(1) 
								<a href="<?php echo site_url()?>/category/productcode"> 
									PRODUCT NAME
								</a>
								[SIZE:VARIATION] 
							</li>

						</ul>
					</div>
				</div>





			</div>

			


		</div>

		<div class="ui container" id="pendingcontent" style="margin-top: 2%;display: none">
			PENDING
		</div>
		
		<div class="ui container" id="completedcontent" style="margin-top: 2%;display: none">
			COMPLETED
		</div>
		
		<div class="ui container" id="cancelledcontent" style="margin-top: 2%;display: none">
			CANCELLED
		</div>


	<br><br><br><br><br><br><br><br>
</body>

<style type="text/css">
	ul#items li {
		display: inline-block;
		margin-right: 5%;
	}
</style>

<script type="text/javascript">
	var active = "#topaycontent";
	var actButton = "#topay";
	$(".button").click(function(event) {
		$(actButton).addClass("basic");
		$(active).toggle();
		var display = "#" + event.target.id + "content";
		$("#" + event.target.id).removeClass("basic");
		$(display).toggle();
		active = display;
		actButton= "#" + event.target.id;
	});
</script>
