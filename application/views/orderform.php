<style type="text/css">
	.main {
		font-family: Assisstant-Bold;
		font-size: 15px;
	}
	.text {
		font-family: Assisstant-Light;
		font-size: 12px;
	}
</style>

<body>
	<br>
	<div class="ui double stackable grid">
		
		<div class="ui ten wide column"> 
			<div class="mini ui form">
				<h3 class="main ui dividing header">CONTACT INFORMATION</h3>

					<div class="field">
					    <label>NAME</label>
					    <div class="two fields">
					      <div class="field">
					        <input type="text" name="shipping[first-name]" placeholder="First Name">
					      </div>
					      <div class="field">
					        <input type="text" name="shipping[last-name]" placeholder="Last Name">
					      </div>
					    </div>
					</div>

					<div class="two fields">
						<div class="field">
						    <label>EMAIL</label>
						      <div class="field">
						        <input type="text" name="shipping[first-name]" placeholder="email">
						      </div>
						</div>

						<div class="field">
						    <label>MOBILE NO.</label>
						      <div class="field">
						        <div class="ui labeled input">
								  <div class="ui label">
								    +63
								  </div>
								  <input type="text" placeholder="912345678">
								</div>
						      </div>
						</div>
					</div>

				<h3 class="main ui dividing header">SHIPPING INFORMATION</h3>

				<div class="field">
					    <label>ADDRESS</label>
					    <select class="ui fluid dropdown">
                                            <option value="" selected="selected" disabled>SAVED ADDRESSES</option>
                                            <option value="coverups">COVERUPS</option>
                                            <option value="dresses">DRESSES</option>
                                            <option value="rompers">ROMPERS</option>
                                            <option value="summerdresses">SUMMER DRESSES</option>
                                            <option value="swimwear">SWIMWEAR</option>
                        </select>

					    </div>
					<div class="field">
					    	<input type="text" name="shipping[first-name]" placeholder="complete address">
					</div>

				<div class="two fields">
					<div class="field">
					    <label>RECIPIENT'S NAME/S</label>
					      <div class="field">
					        <input type="text" name="shipping[first-name]" placeholder="COMPLETE NAME">
					      </div>
					</div>

					<div class="field">
					    <div class="ui checkbox">
							  <input type="checkbox" name="example">
							  <label>RUSH ORDER</label>
							</div>
					</div>
				</div>

				<h3 class="main ui dividing header">PAYMENT</h3>
				<div class="field">
					    <label>METHOD</label>
					      <select class="ui fluid dropdown">
                                            <option value="" selected="selected" disabled>SAVED ADDRESSES</option>
                                            <option value="coverups">COVERUPS</option>
                                            <option value="dresses">DRESSES</option>
                                            <option value="rompers">ROMPERS</option>
                                            <option value="summerdresses">SUMMER DRESSES</option>
                                            <option value="swimwear">SWIMWEAR</option>
                        </select>
					</div>

				<h3 class="main ui dividing header">SPECIAL INSTRUCTIONS/NOTES FOR THE SELLER</h3>
				<div class="field">
					<textarea rows="2"></textarea>
				</div>

				<h3 class="main ui dividing header">ORDER SUMMARY</h3>
				SUBTOTAL: PHP <br>
				SHIPPING FEE: PHP <br><br>
				<div style="font-weight: bolder;font-size: 120%"> TOTAL: PHP <br></div>


				<div class="field" style="text-align: right;padding-top: 2%">
                                        <div class="mini ui buttons">
                                            <button class="ui button">CANCEL</button>
                                            <div class="or"></div>
                                            <button class="ui positive button">PLACE ORDER</button>
                                        </div>
                                    </div>


			</div>
		</div>
		
		<div class="ui six wide column raised segment">
			
				<div style="font-family: Assistant-ExtraBold;font-size: 90%; letter-spacing: 1px">
							ITEMS ON THIS CART
						</div>
						<ul id="items" class="ui list" style="font-family: Assistant-Regular;font-size: 90%; letter-spacing: 1px">
							<li>	(1) 
								<a href="<?php echo site_url()?>/category/productcode"> 
									PRODUCT NAME
								</a>
								[SIZE:VARIATION] 
							</li>
							<li>	(1) 
								<a href="<?php echo site_url()?>/category/productcode"> 
									PRODUCT NAME
								</a>
								[SIZE:VARIATION] 
							</li>

						</ul>
			



		</div>
	
	</div>
	<br><br><br><br><br><br><br>
</body>