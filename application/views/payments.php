<div class="ui thirteen wide column" style="padding-right: 3%">
	<div class="ui fluid container" style="margin-top: -3%;padding-bottom: 2%">
		<div style="font-family: Assistant-ExtraBold; font-size: 300%; letter-spacing: 5px; text-align: right;padding-top: 2.5%">
        	PAYMENTS
    	</div>
	</div>

	<div class="ui fluid container" style="overflow-y: scroll;height: 50%;max-height: 50%;padding-bottom: 10%">
        <table class="ui small compact celled table" id="table">
            <thead>
                <tr>
                    <th>USERNAME</th>
                    <th>ORDER ID</th>
                    <th>PAYMENT METHOD</th>
                    <th>PAYMENT REFERENCE</th>
                    <th>SENDER'S NAME</th>
                    <th>LATEST PAYMENT DATE</th>
                    <th style="text-align: right">AMOUNT SENT</th>
                    <th style="text-align: right">TOTAL</th>
                    <th>RECEIVED</th> 
                </tr>
            </thead>

            <tbody>
                

                <?php
                    foreach ($row as $r) {
                        echo "<tr>
                            <td> $r->username</td>
                            <td> $r->order_ID</td>
                            <td> $r->method</td>
                            <td> $r->reference</td>
                            <td> $r->sender_name</td>
                            <td> $r->date_sent</td>
                            <td style='text-align: right'> $r->total</td>
                            <td style='text-align: right'> $r->amount_sent</td>
                            <td> <button id='$r->received' class='fluid ui button'> received </button></td>
                        </tr>";
                    }
                ?>

            </tbody>
        </table>

        <p id="hello">

    </div>
</div>
</div>

<style type="text/css">
    tr:hover {
          background-color: #FFEF00;
        }

    thead th {
        position: sticky;
        position: -webkit-sticky;
        top: 0;
        background: white;
        z-index: 10;
    }
</style>