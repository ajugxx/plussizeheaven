<div class="ui text container" style="margin-top: 5%;margin-bottom: 10%">
	<form action="#" method="POST">
		<div class="mini ui form">
			<div style="font-size: 300%; font-family: Assistant-Bold;margin-bottom: 3%;letter-spacing: 3px">CONTACT US</div>
			<div class="required field">
				<label>COMPLETE NAME</label>
				<input type="text" name="name" placeholder="name">
			</div>
			<div class="required field">
				<label>EMAIL</label>
				<input type="text" name="email" placeholder="email@email.com">
			</div>
			<div class="required field">
				<label>CATEGORY</label>
				 <select class="ui fluid dropdown" name='cat '>
	   				<option value="" selected="selected" disabled>CATEGORY</option>
	   				<option value="business">BUSINESS</option>
	   				<option value="job inquiry">JOB INQUIRY</option>
	   				<option value="general">GENERAL</option>
	   			</select>
	   
			</div>
			<div class="required field">
				<label>TITLE</label>
				<input type="text" name="title" placeholder="title">
			</div>
			<div class="required field">
				<label>BODY</label>
				<textarea rows="7" name="body"></textarea>
			</div>
			<div class="field" style="text-align: right;">
				<input class='ui basic positive button' type="submit" name="contact" value="SEND MESSAGE">
			</div>
		</div>
	</form>
</div>

<!-- <script type="text/javascript">
	$(".button").click(function(event){
		alert("Message sent. We'll get back to you soon.");
	});
</script>
 -->
<!-- <script type="text/javascript">
	$("#contact").click(function(event){
		var contact = "true";
		var n = $(".name").val();
		var e = $(".email").val();
		var c = $(".cat").val();
		var t = $(".title").val();
		var b = $(".body").val();
		alert('Message sent.');

		//var q = ("#quant").val();
		//alert(c);
		jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/contactus',
				dataType: 'json',
				data: {contact:contact,n:n,e:e:,c:c,:t:t,b:b},
				complete: function() {
					alert('Message sent.');
				}
			});
	});
</script> -->