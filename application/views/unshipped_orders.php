<style type="text/css">
    tr:hover {
          background-color: #FFEF00;
        }

    thead th {
        position: sticky;
        position: -webkit-sticky;
        top: 0;
        background: white;
        z-index: 10;
    }
</style>

<div class="ui thirteen wide column" style="padding-right: 3%">
<div class="ui fluid container" style="margin-top: -3%">
    <div style="font-family: Assistant-ExtraBold; font-size: 300%; letter-spacing: 4%; text-align: right;padding-top: 2.5%">
        RECENT ACTIVITY
    </div>

    <div class="ui secondary pointing menu">
        <div class="right menu">
            <a class="item" href="<?php echo site_url();?>/recent_activity/unprocessed_orders">
                UNPROCESSED ORDERS
            </a>
              
            <a class="active item" href="#">
                UNSHIPPED ORDERS
            </a>    
                 
            <a class="item" href="<?php echo site_url();?>/recent_activity/shipped_orders">
                SHIPPED ORDERS
            </a>

            <a class="item" href="<?php echo site_url();?>/recent_activity/unresolved_inquiries">
                UNRESOLVED INQUIRIES
            </a>

            <a class="item" href="<?php echo site_url();?>/recent_activity/messages">
                MESSAGES
            </a>  
        </div>  
    </div>
</div>

<div class="ui fluid container" style="overflow-y: scroll;height: 50%;max-height: 50%;padding-bottom: 10%">


        <table class="ui small compact celled table" id="table">
            <thead>
                <tr>
                    <th>ORDER ID</th>
                    <th>USERNAME</th>
                    <th>PAYMENT DATE</th>
                    <th>PAYMENT REF</th>
                    <th>TRACKING NO.</th>
                    <th>ETA</th>
                    <th style="text-align: right">TOTAL</th>
                </tr>
            </thead>

            <tbody>
                
                <?php 
                        foreach ($row as $r) {
                            echo "<tr id='$r->order_ID'>
                                <td> $r->order_ID</td>
                                <td> $r->username</td>
                                <td> $r->date_sent</td>
                                <td> $r->reference</td>
                                <td> $r->trn</td>
                                <td> $r->eta</td>
                                <td style='text-align: right'> $r->total</td>
                            </tr>";
                        }
                    ?>

            </tbody>
        </table>

        <p id="hello">

    </div>

<div class='ui fluid container' id='none'>
                <div class='title'>ORDER DETAILS</div>
                <div class='ui grid'>
                    <div class='row' style='word-wrap:break-word'>
                        <div class='ui seven wide column'>
                            <div class='text'> 
                                ORDER ID:  <br>
                                USERNAME:  <br>
                                CHECKOUT DATE:  <br>
                                PAYMENT DATE:  <br>
                                PAYMENT METHOD:  <br>
                                PAYER'S NAME:  <br>
                                PAYMENT REFERENCE:  <br>
                                TOTAL: <div id='total' style='display:  inline-block;'></div>
                            </div>

                            <br>

                            <div class='subheader'>SHIPPING DETAILS</div>
                            <div class='text'>
                                RECEIVER NAME:  <br>
                                COMPLETE ADDRESS:  <br>
                                LANDMARK(S):  <br>
                                COURIER:  <br>
                            </div>
                        </div>

                        <div class='ui nine wide column'>
                            <div class='subheader'>PRODUCT LIST </div>
                            <div class='ui segment'>
                                <p><p>
                            </div>
                        </div>
                    </div>
                    <div class='row' >
                        <div class='ui right aligned column'>
                            <div class='ui buttons'>
                              <button class='ui button'>CANCEL</button>
                              <div class='or'></div>
                              <button class='ui positive button'>ORDER PROCESSED</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


 <?php 
    


        foreach ($row as $r) {
            echo "<div class='ui fluid container' id='container$r->order_ID' style='display:none'>
                <div class='title'>ORDER DETAILS</div>
                <div class='ui grid'>
                    <div class='row' style='word-wrap:break-word'>
                        <div class='ui seven wide column'>
                            <div class='text'> 
                                ORDER ID: $r->order_ID <br>
                                USERNAME: $r->username <br>
                                CHECKOUT DATE: $r->cdate <br>
                                PAYMENT DATE: $r->date_sent <br>
                                PAYMENT METHOD: $r->method <br>
                                PAYER'S NAME: $r->sender_name <br>
                                PAYMENT REFERENCE: $r->reference <br>
                                TOTAL: $r->amount_sent <br>
                                TOTAL: $r->total 
                            </div>

                            <br>

                            <div class='subheader'>SHIPPING DETAILS</div>
                            <div class='text'>
                                RECEIVER NAME: $r->receiver  <br>
                                COMPLETE ADDRESS: $r->address  <br>
                                LANDMARK(S): $r->landmark  <br><br>
                                COURIER: <div class='ui fluid input'>
                                  <input type='text' id='courier$r->order_ID' value='$r->courier'>
                                </div><br>
                                TRACKING NUMBER:  
                                <div class='ui fluid input'>
                                  <input type='text' id='trn$r->order_ID' value='$r->trn'>
                                </div><br>
                                ETA:  
                                <div class='ui fluid input'>
                                  <input type='text' id='eta$r->order_ID' value='$r->eta'>
                                </div>
                            </div>
                        </div>

                        <div class='ui nine wide column'>
                            <div class='subheader'>PRODUCT LIST </div>
                            <div class='ui segment' style='font-family:Assistant-Light;font-size:12px'>
                                <ul id='items' class='ui list'>";

                                foreach ($items[$r->username.$r->order_ID] as $i) {
                                    echo "<li>    ($i->q) 
                                            <a href='<?php echo site_url()?>/category/$i->c'> 
                                                $i->n
                                            </a>        
                                            [$i->s:$i->v] 
                                        </li>";
                                
                                    
                                }

                         echo       "</ul>
                            </div>
                        </div>
                    </div>
                    <div class='row' >
                        <div class='ui right aligned column'>
                            <div class='ui buttons'>
                              <button class='cancel ui button'>CANCEL</button>
                              <div class='or'></div>
                              <button id='$r->order_ID' class='save ui positive button'>ORDER PROCESSED</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>";
        }
    ?>    



</div></div>


<script type="text/javascript">
    var active = "#none";
    $("#table tr").click(function(event) {
        $(active).toggle();
        active = "#container" + this.id;
        $(active).toggle();
    });

    $(".save").click(function(event) {
        var save = "true";
        var c = $("#courier"+this.id).val();
        var trn = $("#trn"+this.id).val();
        var eta = $("#eta"+this.id).val();
        jQuery.ajax({
                type: "POST",
                url: '<?php echo site_url();?>/recent_activity/',
                dataType: 'json',
                data: {save: save, c:c, trn:trn, eta:eta},
                complete: function() {
                        //window.location.href = window.location.href;
                        alert('Changes saved.\nRefresh to reflect changes.');
                }
            });
    });

    $(".cancel").click(function(event) {
        window.location.href = window.location.href;
    });

    // function getData(row) {
    //  var order_ID = document.getElementById("table").rows[row.rowIndex].cells[0].innerHTML;
    //  document.getElementById("hello").innerHTML = order_ID;
    // }
</script>