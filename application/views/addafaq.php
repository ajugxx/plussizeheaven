<div class="ui thirteen wide column" style="padding-right: 3%">
	<div class="ui fluid container" style="margin-top: -3%;padding-bottom: 10%">
			<h1 style="float:right; padding-top:50px;font-size: 50px;font-family: Assistant-ExtraBold; letter-spacing: 5px;margin-bottom: 7%">ADD A FAQ</h1>
	</div>
	

	<div class="ui fluid container">
		<div class="ui form">
				<div class="field">
					<label style="margin-bottom: 10px; font-size: 15px;color:black;letter-spacing: 2px;font-family: Assistant-Light;">QUESTION</label>
					<textarea rows="2" id="question"></textarea>
				</div>

				<div class="field">
					<label style="margin-bottom: 10px; font-size: 15px;color:black;letter-spacing: 2px;font-family: Assistant-Light;">ANSWER</label>
					<textarea rows="10" id="answer"></textarea>
				</div>

				<input type="submit" style="border: none;background-color: Transparent;cursor:pointer;font-size: 20px; margin-bottom:5%; font-family: Assistant-Bold; letter-spacing: 3px;" id="add" name="add" value="ADD THIS FAQ">
			</div>
	</div>


	<div class="ui fluid container">
		<h2 style="padding-top:5%;font-size: 25px;font-family: Assistant-Bold; letter-spacing: 2px;padding-bottom: 2%">
			FREQUENTLY ASKED QUESTIONS
		</h2>
	</div>

	<div class="ui fluid container">
		<div class="ui items">

				<?php
					foreach ($row as $faq) {
						echo "<div class='ui fluid container' id='container$faq->tag' style='margin-bottom: 4%'>
						    	<div class='mini fluid ui input header' style='font-size: 15px;letter-spacing: 2px;font-family: Assistant-Bold;'>
						    		<input type='text' id='q$faq->tag' value='$faq->question'> 
						    	</div>
						    	<div class='meta' style='font-size: 15px;font-family: Assistant-Light;letter-spacing: 2px;'>
									<span><div class='ui form'>
										<textarea rows='2' id='a$faq->tag'>$faq->answer</textarea>
									</div></span><br>
								</div>
								<div class='bottom aligned content' style='margin-top:-1%'>
									<a class='edit item' id='$faq->tag' style='color: black;font-size: 15px; padding-right: 40px; font-family: Assistant-Bold;'>
							    		UPDATE
									</a>
									<a class='remove item' id='$faq->tag' style='color: black;font-size: 15px; padding-right: 40px; font-family: Assistant-Bold;'>
							    		REMOVE
									</a>
								</div>			        
						</div>
						";
					}
				?>

				
			</div>
	</div>
</div>
</div>

<script type="text/javascript">
	$("#add").click(function(event) {
			var a = "true";
			var q = $("#question").val();
			var ans = $("#answer").val();
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/addAFAQ/',
				dataType: 'json',
				data: {add: a, question:q, answer: ans},
				complete: function() {
						window.location.href = window.location.href;
						//alert(q + "\n" + ans);
				}
			});
		});

	$(".edit").click(function(event) {
			var id = event.target.id; 
			var a = "true";
			var q = $("#q"+id).val();
			var ans = $("#a"+id).val();
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/addAFAQ/',
				dataType: 'json',
				data: {update: a, tag: id, newQ:q, newAns: ans},
				complete: function() {
					alert('update successful!');
				}
			});
		});

	$(".remove").click(function(event) {
			var a = "true";
			var tag = event.target.id;
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/addAFAQ/',
				dataType: 'json',
				data: {remove: a, tag:tag},
				complete: function() {
						window.location.href = window.location.href;
				}
			});
		});
</script>