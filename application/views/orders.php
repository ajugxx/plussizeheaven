<!DOCTYPE html>
<html>
<head>
	<title>Plus Size Heaven</title>
</head>

<body>
	<div onscroll="pagescroll()">
		<div id="body_content" style="margin-left:-7%;margin-right: -7%;margin-top:-1%;margin-bottom: 50px;">
			<h1 style="float:right; padding-top:50px;font-size: 50px;font-family: Assistant-ExtraBold; letter-spacing: 5px;margin-right:5%;margin-bottom: 7%">ORDERS</h1>
			<table class="ui celled padded table" style="margin-left:5%;margin-bottom: 5%;margin-right: 5%; width:1244px;height: 650px;text-align: top">
				<thead>
					<tr>
						<th>USERNAME</th>
						<th>ORDER_ID</th>
						<th>TRACKING NO.</th>
						<th>CHECKOUT DATE</th>
						<th>PAYMENT REF.</th>
						<th>TOTAL AMOUNT</th>
						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td data-label="USERNAME">12345678</td>
						<td data-label="ORDER_ID">arianne</td>
						<td data-label="TRACKING NO">235343</td>
						<td data-label="CHECKOUT DATE">11/11/2011</td>
						<td data-label="PAYMENT REF">5000.00</td>
						<td data-label="TOTAL AMOUNT"></td>
					</tr>
					
				</tbody>
			</table>

			<h2 style="font-size: 25px;font-family: Assistant-Bold; letter-spacing: 2px;margin-left:5%;margin-bottom: 2%">ORDER DETAILS</h2>

			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">ORDER ID:</h3>
			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">USERNAME:</h3>
			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">CHECKOUT DATE:</h3>
			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">SHIPPING FEE:</h3>
			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">SENDER'S NAME:</h3>
			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">PAYMENT DATE:</h3>
			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">PAYMENT METHOD:</h3>
			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">TOTAL:</h3>

			<h2 style="padding-top:15px;font-size: 25px;font-family: Assistant-Bold; letter-spacing: 2px;margin-left:58px;margin-bottom: 10px">SHIPPING DETAILS</h2>

			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">RECEIVER NAME:</h3>
			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">COMPLETE ADDRESS:</h3>
			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">COURIER:</h3>
			<h3 style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-left:5%;">TRACKING NO.:</h3>

			<h2 style="padding-top:15px;font-size: 30px;font-family: Assistant-Bold; letter-spacing: 2px;margin-left:58px;margin-bottom: 10px">PRODUCT LIST</h2>
		</div>
	</div>
	
</body>
</html>

<!-- <script type="text/javascript">
	$('.ui.sticky')
	  .sticky({
	    context: '#body_content'
	  })
	;
</script> -->