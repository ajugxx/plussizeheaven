<style type="text/css">
    tr:hover {
          background-color: #FFEF00;
        }

    thead th {
        position: sticky;
        position: -webkit-sticky;
        top: 0;
        background: white;
        z-index: 10;
    }

    img {
        height: 100px;
        width: 100px;
    }
</style>

<div class="ui thirteen wide column" style="padding-right: 3%">
<div class="ui fluid container" style="margin-top: -3%">

    <div style="font-family: Assistant-ExtraBold; font-size: 300%; letter-spacing: 4%; text-align: right;padding-top: 2.5%">
        RECENT ACTIVITY
    </div>

    <div class="ui secondary pointing menu">
        <div class="right menu">
            <a class="item" href="<?php echo site_url();?>/recent_activity/unprocessed_orders">
                UNPROCESSED ORDERS
            </a>
              
            <a class=" item" href="<?php echo site_url();?>/recent_activity/unshipped_orders">
                UNSHIPPED ORDERS
            </a>    
              
            <a class="item" href="<?php echo site_url();?>/recent_activity/shipped_orders">
                SHIPPED ORDERS
            </a>

            <a class="active item" href="#">
                UNRESOLVED INQUIRIES
            </a>

            <a class="item" href="<?php echo site_url();?>/recent_activity/messages">
                MESSAGES
            </a>  
        </div>  
    </div>
</div>

<div class="ui fluid container" style="overflow-y: scroll;height: 50%;max-height: 50%;padding-bottom: 10%">


        <table class="ui small compact celled table" id="table">
            <thead>
                <tr>
                    <th class="one wide">DATE</th>
                    <th class="three wide">USERNAME</th>
                    <th class="five wide">PRODUCT NAME</th>
                    <th class="seven wide">INQUIRY</th>
                </tr>
            </thead>

            <tbody>
                
                <?php 
                        foreach ($row as $r) {
                            echo "<tr id='$r->tag'>
                                <td> $r->sent</td>
                                <td> $r->username</td>
                                <td> $r->prodname</td>
                                <td> $r->body</td>
                            </tr>";
                        }
                    ?>

            </tbody>
        </table>

        <p id="hello">

    </div>


<div class="ui fluid container" id="none">
    <div class="title">INQUIRY</div>
</div>

<?php
    foreach ($row as $i) {
        echo "<div class='ui fluid container' id='container$i->tag' style='display:none'>
            <div class='title'>INQUIRY</div>
            <div class='ui grid'>
                <div class='row' style='word-wrap:break-word'>
                    <div class='ui seven wide column'>
                        <div class='text'> 
                            $i->sent <br>
                            USERNAME: $i->username <br>
                       
                            <br>
                            <a href='<?php echo site_url()?>/category/$i->product_code'>
                                $i->prodname
                            </a>
                        </div>
                        <br>
                        <div class='text'>
                            $i->body
                        </div>
                    </div>

                    <div class='ui nine wide column'>
                        <div class='subheader'>REPLY </div>
                        <div class='ui form'>
                          <div class='field'>
                            <textarea id='reply$i->tag'></textarea>
                          </div>
                        </div>
                    </div>
                </div>
                <div class='row' >
                    <div class='ui right aligned column'>
                        <div class='ui buttons'>
                          <button class='cancel ui button'>CANCEL</button>
                          <div class='or'></div>
                          <button class='save ui positive button' id='$i->tag'>RESOLVE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>";
    }
?>


</div></div>



<script type="text/javascript">
    var active = "#none";
    $("#table tr").click(function(event) {
        $(active).toggle();
        active = "#container" + this.id;
        $(active).toggle();
    });

    $(".save").click(function(event) {
            var a = "true";
            var tag = this.id;
            var reply = $("#reply"+this.id).val();
            jQuery.ajax({
                type: "POST",
                url: '<?php echo site_url();?>/recent_activity/unresolved_inquiries',
                dataType: 'json',
                data: {save:a, resolvetag: tag, rep:reply},
                complete: function() {
                        //alert(a + " " + tag + " " + reply+'Resolved.');
                        window.location.href = window.location.href;
                }
            });
        });


    $(".cancel").click(function(event) {
        window.location.href = window.location.href;
    });

    // function getData(row) {
    //  var order_ID = document.getElementById("table").rows[row.rowIndex].cells[0].innerHTML;
    //  document.getElementById("hello").innerHTML = order_ID;
    // }
</script>