<div class="ui thirteen wide column" style="padding-right: 3%">
	<div class="ui fluid container" style="margin-top: -3%">
		<h1 style="padding-top:50px;font-size: 50px;font-family: Assistant-Bold; letter-spacing: 5px; text-align: right;">
			SITE IDENTITY
		</h1>
	</div>

	<div class="ui center aligned container" style="padding-top: 3%">
		<button class="ui basic button" style="width: 270;height: 170px;font-size: 15px;color:black;letter-spacing: 2px;font-family: Assistant-Light;">
				UPLOAD SITE ICON
			</button>
			<button class="ui basic button" style="width: 270;height: 170px;font-size: 15px;color:black;letter-spacing: 2px;font-family: Assistant-Light;">
				UPLOAD HEADER LOGO
			</button>
	</div>

	<div class="ui container" style="padding-top: 5%">
		<div style="text-align: center;font-size: 20px;font-family: Assistant-Bold;">SOCIAL MEDIA ACCOUNTS</div>
	</div>

	<div class="ui container" style="padding-top: 1%; text-align: center;">
		<div class="ui form">
				<div class="inline field">
					<label style="font-size: 15px;color:black;font-family: Assistant-Light;">INSTAGRAM.COM/</label>
					<input type="text" id="ig" size="40" value="<?php echo $row[0]->instagram;?>">
				</div>
				<div class="inline field">
					<label style="font-size: 15px;color:black;font-family: Assistant-Light;">FACEBOOK.COM/</label>
					<input type="text" id="fb" size="40" value="<?php echo $row[0]->fb;?>">
				</div>
				<br><br>
				<div class="ui buttons" style="float: right">
                  <button class="cancel ui button">CANCEL</button>
                  <div class="or"></div>
                  <button class="save ui positive button">SAVE CHANGES</button>
                </div>
		</div>
	</div>

</div>
</div>


<script type="text/javascript">
	$(".save").click(function(event) {
			var a = "true";
			var ig = $("#ig").val();
			var fb = $("#fb").val();
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/site_identity/',
				dataType: 'json',
				data: {save: a, ig:ig, fb:fb},
				complete: function() {
						//window.location.href = window.location.href;
						alert('Changes saved.');
				}
			});
		});

	$('.cancel').click(function(e) {
		window.location.href = window.location.href;
		});
</script>