<body>
	<div onscroll="pagescroll()">
    	<div id="body_content" style="margin-left:-7%;margin-right: -7%;margin-top:-1%">
			<div class="ui container">
				<h1 style="padding-top:50px;font-size: 50px;font-family: Assistant-ExtraBold;;letter-spacing: 5px;">WISHLIST</h1>
				<a style="color: black;font-size: 15px; padding-top: 7%; font-family: Assistant-Light; letter-spacing: 3px;">
					<i class="trash alternate outline icon"></i>
					<input type="submit" style="border: none;background-color: Transparent;cursor:pointer;" id="clear" name="clear" value="CLEAR WISHLIST">
				</a>
			</div>

			<div class="ui container " style="margin-top: 5%">

				<?php
					foreach($row as $i) {
						echo "<div class='ui container' id='$i->product_code'>
								<div class='ui stackable grid'>
									<div class='row'>
										<div class='ui two wide column'>
									      <img class='ui fluid image' src='".base_url()."assets/images/users/arianne.jpg' class='visible content'>
										</div>
										<div class='ui fourteen wide column'>
											<div class='ui row'>
												<div class='header' style='font-size: 15px;font-family: Assistant-Bold;'>
													$i->name
												</div>
											      <div class='meta' style='font-size: 15px;font-family: Assistant-Light;'>
											        <span>$i->status</span>
											      </div>
											</div>
											<div class='row'>
												<div class='row' style='text-align: right;color: black;font-size: 15px; font-family: Assistant-Bold;margin-top: 5%'>
													PHP $i->price<br>
											    		<input type='submit' style='color: black;font-size: 15px; font-family: Assistant-Bold;border: none;background-color: Transparent;cursor:pointer; padding: 0' class='remover' id='$i->product_code' name='remove' value='REMOVE ITEM'>
										      </div>
											</div>
										</div>
									</div>
								</div>
							</div>";
					}
				?>
			</div>
			</div>
		</div>
	<br><br><br><br>
</body>

<script type="text/javascript">
	$("#clear").click(function(event) {
			var clear = "true";
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/wishlist/',
				dataType: 'json',
				data: {clear: clear},
				complete: function() {
						$('#clear').val("CLEARED");
				}
			});
		});

	$(".remover").click(function(event) {
			var remove = event.target.id;
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/wishlist/',
				dataType: 'json',
				data: {removecode: remove},
				complete: function() {
						$("#" + remove).remove();
				}
			});
		});
</script>
