<div class="ui grid">
	<div class="ui row">
		<div class="ui fluid three wide column" style="padding-top: 3%;background-color: black;">
			<div style="font-family: Assistant-Light;font-size: 10px;color: white;letter-spacing: 3px;">
				<center>
					<p id="hello"></p>
					<img class='ui tiny image' style="padding-bottom: 2%" src='<?php echo base_url()?>assets/images/users/arianne.jpg'>
					<?php echo $name ?> <br>
					<?php echo $handle ?></center><br><br>
			</div>

			<div class="ui fluid inverted secondary vertical menu">
				<div style="font-family: Assistant-Regular;font-size: 10px;color: white;letter-spacing: 3px;line-height: 370%;padding-left: 6%;">
					<a class="item" href="<?php echo site_url();?>/recent_activity/">
						<i class="bell icon"></i>RECENT ACTIVITY 
					</a> 
					<a class="item" href="<?php echo site_url();?>/payments/">
						<i class="money bill alternate outline icon"></i>PAYMENTS
					</a>
					<a class="item" href="<?php echo site_url();?>/admin_accounts/">
						<i class="user icon"></i>ADMIN ACCOUNTS
					</a>
					<a class="item" href="<?php echo site_url();?>/site_identity/">
						<i class="laptop icon"></i>SITE IDENTITY
					</a>
					<a class="item" href="<?php echo site_url();?>/special_items/">
						<i class="star icon"></i>SPECIAL ITEMS
					</a>
					<a class="item" href="<?php echo site_url();?>/products/">
						<i class="shopping cart icon"></i>PRODUCTS
					</a>
					<a class="item" href="<?php echo site_url();?>/registered_users/">
						<i class="users icon"></i>REGISTERED USERS
					</a>
					<a class="item" href="<?php echo site_url();?>/terms_and_policies/">
						<i class="pencil alternate icon"></i></i>TERMS AND POLICIES
					</a>
					<a class="item" href="<?php echo site_url();?>/addAFAQ/">
						<i class="question circle icon"></i>ADD A FAQ
					</a>
				</div>
			</div>
		</div>

