
<body>
	<div id="body_content" style="margin-left:-7%;margin-right: -7%;margin-top:-1%">
	<div class="ui fluid container" style="background-color: hotpink;text-align: center;">


		<form action="#" method="post" name="signin" class="mui-form--inline" style="padding: 20%" onsubmit="log()">
			

			<?php 
				echo $message;
			?>

			<legend> <div class="text accounttitle"> SIGN IN </div> </legend>
			<div  style="font-family: Assistant-Regular;color: white; opacity: .8;font-size: 12px;letter-spacing: 2px">  
			OR <a href="#NEW_ACCOUNT" > <div style="display: inline-block;font-family: Assistant-Regular;color: white; opacity: .8;font-size: 12px;letter-spacing: 2px">  CREATE A NEW ACCOUNT</div></a>  
			</div>
		  
			<div class="mui-textfield text accountbody" style="width: 30%;">
	    		<input name="acctholder" type="text" placeholder="USERNAME/EMAIL" style="color: white; text-align: center;font-family: Assistant-Regular; letter-spacing: 2px">
	  		</div>

			<input type="hidden" name="placeholder" />

			<br>

			<div class="mui-textfield text accountbody" style="width: 30%;">
	    		<input id="key" type="password" placeholder="PASSWORD" style="color: white; text-align: center;font-family: Assistant-Regular; letter-spacing: 2px">
	  		</div>

			<br>
			<br>

			<div style="font-family: Assistant-ExtraBold; letter-spacing: 3px">    
				 <input type="submit" value="SIGN IN" name="SIGNIN" class="button button-plain button-border button-rectangle button-small" />
			</div> <br>
				
			<a  href="#"> 
			<div class="acctrd" style="color: white;font-family: Assistant-Regular; opacity: .7;margin-top: -2%;font-size: 10px;letter-spacing: 2px;"> 
				  FORGOT PASSWORD </div></a> 
		</form>		
<a name="NEW_ACCOUNT"></a>
	</div>

	<br><br><br><br><br>

		<div class="ui grid">
			<div class="ui eight wide column" > 
			<div style="font-family: Assistant-Bold; font-size: 50px;color:#DAA520;text-align: right">CREATE AN ACCOUNT</div>
			<div class="ui container" style="padding-left: 20% ">

				<form name="register" action="#SIGNIN" method="post" class="mui-form--inline" onsubmit="subencrypt()" style="background-color: #DAA520; padding-left: 5%; padding-bottom: 5%">
			  		<div class="mui-textfield text accountbody" style="width: 45%;">
			    		<input type="text" placeholder="FIRST NAME" name="firstname" style="color: white;font-family: Assistant-Regular; letter-spacing: 1px; text-indent: 5%" required>
			  		</div>

			  		<div class="mui-textfield text accountbody" style="width: 45%;">
			    		<input type="text" placeholder="LAST NAME" name="lastname" style="color: white;font-family: Assistant-Regular; letter-spacing: 1px;text-indent: 5%" required>
			  		</div>

			 		<br>

			 		<div class="mui-textfield text accountbody" style="width: 90%;">
			    		<input type="email" placeholder="EMAIL" name="email" style="color: white;font-family: Assistant-Regular; letter-spacing: 1px;text-indent: 2.5%" required>
			  		</div>	

			  		<input type="hidden" name="testfield" />

			  		<br>

			  		<div class="mui-textfield text accountbody" style="width: 90%;">
			    		<input type="text" placeholder="USERNAME" name="username" minlength="6" style="color: white; font-family: Assistant-Regular; letter-spacing: 1px;text-indent: 2.5%" required>
			  		</div> 
			 	 	<br>

			  		<div class="mui-textfield text accountbody" style="width: 90%;">
			    		<input type="text" pattern="09[0-9]{9}" name="mobile" placeholder="PRIMARY MOBILE NUMBER: 09123456789" style="color: white;font-family: Assistant-Regular; letter-spacing: 1px;text-indent: 2.5%" required>
			  		</div>


					<br><br>

					<div class="mui-textfield text accountbody" style="width: 90%;">
			    		<input id="pw" type="password" placeholder="PASSWORD" minlength="6" style="color: white;font-family: Assistant-Regular; letter-spacing: 1px;text-indent: 2.5%" required>
			  		</div>


					<br>

					<div class="mui-textfield text accountbody" style="width: 90%;">
			    		<input type="password" placeholder="REPEAT PASSWORD" minlength="6" oninput="check(this)" style="color: white;font-family: Assistant-Regular; letter-spacing: 1px;text-indent: 2.5%" required>
			  		</div>
			  
			 		<br><br>

			 		<div style="text-align: right; padding-right: 10%;font-family: Assistant-ExtraBold; letter-spacing: 3px">
			  			<input type="submit" value="SIGN UP" name="SIGNUP" class="button  button-plain button-border button-rectangle button-small" /
						> 
					</div>
				</form>


</div>
			</div>

			<div class="ui middle aligned eight wide column">
				<img class="ui fluid image" src="<?php echo base_url();?>assets/images/account/new_account_footer.jpg">
			</div>
		</div>
</div>
</body>

<br>
<br>
<br>
<br>
<br>
<br><br>
<br>
<br>
<br>

<script type="text/javascript">
	function log() {
		document.signin.placeholder.value = md5(document.getElementById('key').value);
		return true;
	}

	function subencrypt() {
		document.register.testfield.value = md5(document.getElementById('pw').value);
		return true;
	}

	function check(input) {
		if (input.value != document.getElementById('pw').value) {
            input.setCustomValidity("PASSWORDS DON'T MATCH");
        } else {
            // input is valid -- reset the error message
            input.setCustomValidity('');
        }
	}

	$('.ui.sticky')
	  .sticky({
	    context: '#body_content'
	  })
	;

	
</script>