<div class="ui thirteen wide column" style="padding-right: 3%">
	<div class="ui fluid container" style="margin-top: -3%;padding-bottom: 10%">
			<h1 style="float:right; padding-top:50px;font-size: 50px;font-family: Assistant-ExtraBold; letter-spacing: 5px;">
				TERMS & POLICIES
			</h1>
	</div>

	<div class="ui fluid container">


		<div class="ui form">
				<div class="field">
					<label style="margin-bottom: 10px; font-size: 15px;color:black;letter-spacing: 2px;font-family: Assistant-Bold;">ABOUT US</label>
					<textarea id="au" rows="10"><?php echo $row[0]->about_us;?></textarea>
				</div>

				<div class="field">
					<label style="margin-bottom: 10px; font-size: 15px;color:black;letter-spacing: 2px;font-family: Assistant-Bold;">RETURN POLICIES</label>
					<textarea id="rp" rows="10"><?php echo $row[0]->return_pol;?></textarea>
				</div>

				<div class="field">
					<label style="margin-bottom: 10px; font-size: 15px;color:black;letter-spacing: 2px;font-family: Assistant-Bold;">PAYMENT METHODS</label>
					<textarea id="pm" rows="10"><?php echo $row[0]->payment_methods;?></textarea>
				</div>

				<div class="field">
					<label style="margin-bottom: 10px; font-size: 15px;color:black;letter-spacing: 2px;font-family: Assistant-Bold;">SHIPPING OPTIONS</label>
					<textarea id="so" rows="10"><?php echo $row[0]->shipping_options;?></textarea>
				</div>
				<br>
				<div class="ui buttons" style="float: right">
                  <button class="cancel ui button">CANCEL</button>
                  <div class="or"></div>
                  <button class="save ui positive button">SAVE CHANGES</button>
                </div>
			</div>
	</div>
</div>
</div>

<script type="text/javascript">
	$(".save").click(function(event) {
			var a = "true";
			var au = $("#au").val();
			var rp = $("#rp").val();
			var pm = $("#pm").val();
			var so = $("#so").val();
			jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/terms_and_policies/',
				dataType: 'json',
				data: {save: a, au:au, rp:rp, pm:pm, so:so},
				complete: function() {
						//window.location.href = window.location.href;
						alert('Changes saved.');
				}
			});
		});

	$('.cancel').click(function(e) {
		window.location.href = window.location.href;
		});
</script>