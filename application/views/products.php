<div class="ui thirteen wide column" style="padding-right: 3%">
	<div class="ui fluid container" style="margin-top: -3%;padding-bottom: 2%">
		<div style="font-family: Assistant-ExtraBold; font-size: 300%; letter-spacing: 5px; text-align: right;padding-top: 2.5%">
        	PRODUCTS
    	</div>
	</div>

    <div class="ui fluid container">
        <div class="ui input"  style="width: 50%">
          <input id='pcode' type="text" placeholder="product code or prouct name here">
        </div>
        <!-- <form action="#" method="POST" style="display: inline-block;"> 
            <input type="submit" class="edit ui basic button" name="edit" value="edit this product"> 
        </form> -->
        <button class="remove ui basic button">remove this product</button>
    </div>

	<div class="ui fluid container" style="padding-top: 2%">
            
        <div class="ui accordion">

            <div id="addproduct" class="active title" onclick="toggleADD()">  
                    <i class="dropdown icon"></i> <div class="accordiontitle">ADD/EDIT A PRODUCT </div> 
            </div>
            <div id="addprodform" class="active content" style="margin-left: 2%;font-family: Assistant-Light;font-size: 100%;letter-spacing: 2px;"> 

                    <div class="ui grid">
                        <div class="row">
                            <div class="ui ten wide column">
                                <div class="ui form">
                                    <div class="field">
                                        <label>PRODUCT NAME</label>
                                        <input type="text" id="name" placeholder="product name">
                                    </div>
                                    <div class="field">
                                        <label>PRODUCT CODE</label>
                                        <div class="ui small input">
                                          <input type="text" id="code" placeholder="product code">
                                          <button class="generate ui button">GENERATE</button>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <label>CATEGORY</label>
                                        <select id="cat" class="ui fluid dropdown">
                                            <option value="" disabled selected="true">CATEGORY</option>
                                            <option value="coverups">COVERUPS</option>
                                            <option value="dresses">DRESSES</option>
                                            <option value="rompers">ROMPERS</option>
                                            <option value="summerdresses">SUMMER DRESSES</option>
                                            <option value="swimwear">SWIMWEAR</option>
                                        </select>
                                    </div>
                                    <div class="field">
                                        <label>STATUS</label>
                                        <select id="stat" class="ui fluid dropdown">
                                            <option value="" disabled selected="true">STATUS</option>
                                            <option value="available">AVAILABLE</option>
                                            <option value="out-of-stock">OUT-OF-STOCK</option>
                                        </select>
                                    </div>
                                    <div class="field">
                                        <label>PRICE</label>
                                        <div class="ui right labeled input">
                                            <label for="amount" class="ui label">PHP</label>
                                            <input type="text" placeholder="AMOUNT" id="amount">
                                        </div>
                                    </div>
                                    <div class="field">
                                        <label>DESCRIPTION</label>
                                        <textarea id="desc" rows="5"></textarea>
                                    </div>
                                    <div class="field">
                                        <label>MANUFACTURER</label>
                                        <input type="text" id="manufacturer" placeholder="manufacturer">
                                    </div>
                                    <div class="field">
                                        <label>SIZE RANGE</label>
                                        <input type="text" id="sizerange" placeholder="size range">
                                    </div>
                                    <div class="field">
                                        <label>COLORS</label>
                                        <input type="text" id="colors" placeholder="colors">
                                    </div>
                                    <div class="field">
                                        <label>MATERIAL</label>
                                        <input type="text" id="material" placeholder="material">
                                    </div>
                                    <div class="field">
                                        <label>MADE IN</label>
                                        <input type="text" id="madein" placeholder="made in">
                                    </div>
                                    <div class="field" style="text-align: right;padding-top: 2%">
                                        <button class="save ui positive button">SAVE TO CATALOG</button>
                                    </div>
                                </div>
                            </div>
                            <div class="ui fluid six wide column">
                                <form action="/upload-target" class="dropzone"></form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">

    $(".save").click(function(event){
            var add = "true";
            var n = $("#name").val();
            var c = $("#code").val();
            var cat = $("#cat").val();
            var s = $("#stat").val();
            var a = $("#amount").val();
            var d = $("#desc").val();
            var m = $("#manufacturer").val();
            var sr = $("#sizerange").val();
            var col = $("#colors").val();
            var mat = $("#material").val();
            var made = $("#madein").val();

            jQuery.ajax({
                type: "POST",
                url: '<?php echo site_url();?>/products/',
                dataType: 'json',
                data: {add:add,n:n,c:c,cat:cat,s:s,a:a,d:d,m:m,sr:sr,col:col,mat:mat,made:made},
                complete: function() {
                    alert('Product '+ c +' added!');
                    $("#name").val("");
                    $("#code").val("");
                    $("#cat").val("");
                    $("#stat").val("");
                    $("#amount").val("");
                    $("#desc").val("");
                    $("#manufacturer").val("");
                    $("#sizerange").val("");
                    $("#colors").val("");
                    $("#material").val("");
                    $("#madein").val("");
                }
            });
    });

    $(".generate").click(function(event){
        var n = $("#name").val().toUpperCase();
        var c = n.replace(/ /g,"-");
        var c = c.replace(/A/g,"");
        var c = c.replace(/E/g,"");
        var c = c.replace(/I/g,"");
        var c = c.replace(/O/g,"");
        var c = c.replace(/U/g,"");
        $("#code").val(c);
    });

    //  $(".edit").click(function(event){
    //     var r = "true";
    //     var tag = $("#pcode").val();
        
    // });

    $(".remove").click(function(event){
        var r = "true";
        var tag = $("#pcode").val();
         jQuery.ajax({
                type: "POST",
                url: '<?php echo site_url();?>/products/',
                dataType: 'json',
                data: {remove:r, tag:tag},
                complete: function() {
                    alert('Product '+ tag +' removed!');
                }
            });
    });


    // function toggleADD() {
    //     var element = document.getElementById("addprodform");
    //     var wrap = document.getElementById("addproduct");
    //     if (document.getElementById("addprodform").classList.contains("active")) {
    //         element.classList.remove("active");
    //         wrap.classList.remove("active");
    //     }
    //     else {
    //         element.classList.add("active");
    //         wrap.classList.add("active");
    //     }
    // }
</script>

