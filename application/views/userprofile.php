<body>
	<div onscroll="pagescroll()">
			
			<div class="ui container" style="padding-top: 5%">
				<div class="ui items">
				<div class="item">
				    <div class="image" style="width: 150px;height: 150px">
				    	<img src="<?php echo base_url();?>assets/images/users/arianne.jpg" class="visible content">
				    </div>
				    <div class="content">
				    	<div class="header" style="font-size: 200%;letter-spacing: 5px;font-family: Assistant-Bold;">
				    		<?php echo $name; ?>
				    	</div>
				    	<div class="meta" style="font-size: 120%;font-family: Assistant-Light;margin-top: -1%">
							<span>@<?php echo $handle; ?></span><br>
							<span>MEMBER SINCE: <?php echo $memdate; ?></span>
						</div>
						<div class="bottom aligned content" style="margin-top: 2%; line-height: 150%">
							<a href="<?php echo site_url();?>/profile/settings" class="item" style="color: black;font-size: 12px; padding-right: 40px; font-family: Assistant-Bold; letter-spacing: 3px;">
					    		ACCOUNT SETTINGS
							</a><br>
							<a href="<?php echo site_url();?>/profile/order_history" class="item" style="color: black;font-size: 12px; padding-right: 40px; font-family: Assistant-Bold; letter-spacing: 3px;">
					    		ORDER HISTORY
							</a><br>
							<a href="<?php echo site_url();?>/profile/review" class="item" style="color: black;font-size: 12px; padding-right: 40px; font-family: Assistant-Bold; letter-spacing: 3px;">
					    		REVIEW PRODUCTS
							</a>
						</div>			        
					</div>
				</div>
			</div>
			</div>


			<div class="addForm ui container" style="margin-top: 3%;display: none;">
				<form action="#" method="POST">
					<div class="mini ui form">
						<div class="two fields">
							<div class="field">
								<label>UNIT NO</label>
								<input type="text" name="unit" placeholder="UNIT NO.">
							</div>
							<div class="field">
								<label>BUILDING NAME</label>
								<input type="text" name="bldng" placeholder="BLDNG NAME">
							</div>
						</div>
						<div class="two fields">
							<div class="field">
									<label>STREET ADDRESS</label>
									<input type="text" name="st" placeholder="BLDNG NO. STREET NAME">
							</div>
							<div class="field">
									<label>MUNICIPALITY</label>
									<input type="text" name="municipality" placeholder="MUNICIPALITY">
							</div>
						</div>
						<div class="field">
								<label>CITY</label>
								<input type="text" name="city" placeholder="CITY">
						</div>
						<div class="field">
								<label>PROVINCE</label>
								<input type="text" name="province" placeholder="PROVINCE">
						</div>
						<div class="field">
								<label>REGION</label>
								<input type="text" name="region" placeholder="REGION">
						</div>
						<div class="field">
								<label>LANDMARK/S</label>
								<textarea rows="3" name="landmark"></textarea>
						</div>
						<div class="field" style="text-align: right;">
								<input type="submit" name="add_address" class="mini ui basic positive button" value="ADD THIS ADDRESS">
						</div>
					</div>
				</form>
			</div>


			<div class="ui container segment" style="margin-top:2%;">
				<h2 style="float:left; font-size: 100%;font-family: Assistant-Bold; letter-spacing: 3px;color:black"> 
					MY SHIPPING ADDRESSES
				</h2>
				<input type="submit" class="newAdd" style="border: none;background-color: Transparent;cursor:pointer;float:right;font-size: 100%;font-family: Assistant-Bold; letter-spacing: 2px;color:black;" value="+ ADD A NEW ADDRESS">
					

				<div class="ui items">
				<?php
					foreach ($my_addresses as $a) {
						echo "
								<div class='item' id='cont$a->a_index' style='margin-bottom:-2%'>
								    <div class='content'>
								    	<div class='meta' style='font-size: 100%;color:black;letter-spacing: 2px;font-family: Assistant-Light;'>
											<span>$a->address</span><br><span> LANDMARK: $a->landmark</span><br>
										</div>
										<div class='extra'>
							      			<!--a name='$a->a_index' class='item' style='color: black;font-size: 100%; font-family: Assistant-Light; letter-spacing: 3px;'>
								    			EDIT
								  			</a--!>
							      			<button id='$a->a_index' class='remove item' style='border: none;background-color: Transparent;cursor:pointer;color: black;font-size: 100%; font-family: Assistant-Light; letter-spacing: 3px;'>
								    			REMOVE
								  			</button>
							      		</div>			        
									</div>
								</div>
							<br>";
					}
				?>

				</div>
				<br><br><br>
			</div>

			<div class="ui container" style="padding-top: 4%">
				<div class="ui accordion">
	
				<div id="reviewtitle" class="active title" onclick="togglerev()"> 
					<i class="dropdown icon"></i> <div class="accordiontitle">MY REVIEWS</div>
				</div>
				<div id="reviewcontent" class="active content" style="margin-left: 2%">
					<div class="ui container">
						<div class="ui grid">

							<?php
								foreach ($my_reviews as $r) {
									echo "<div class='ui row'>
											<div class='ui two wide column'> 
												<img class='ui fluid image' src='".base_url()."assets/images/users/arianne.jpg'>
												<div style='font-family: Assistant-Light;font-size: 100%;letter-spacing: 2px'> 
													BOUGHT ON:<br>
													$r->time_stamp
												</div>
											</div>
											<div class='ui fourteen wide column'>
												<div class='ui row' style='font-family: Assistant-ExtraBold;font-size: 100%;letter-spacing: 2px'> 
													$name
												</div>
												<div class='ui row' style='font-family: Assistant-ExtraBold;font-size: 100%;letter-spacing: 2px'>
													$r->title
												</div>
												<div class='ui row' style='font-family: Assistant-Light;font-size: 100%;letter-spacing: 2px;'>
													$r->body
												</div>
											</div>
										</div>";
								}
							?>
						</div>
					</div>
				</div>

				<div id="inqtitle" class="title" onclick="toggleinq()"> 
					<i class="dropdown icon"></i><div class="accordiontitle">MY INQUIRIES</div>
				</div>
				<div id = "inqcontent" class="content" style="margin-left:2%"> 
					
					<?php
						foreach ($my_inquiries['roots'] as $i) {
							echo "<div class='ui container'>
									<div class='row'> 
										<div  style='font-family: Assistant-ExtraBold;font-size: 100%;letter-spacing: 2px'>  
											$name
										</div>
										<div  style='font-family: Assistant-Light;font-size: 100%;letter-spacing: 2px;'>
											$i->sent
										</div>
										<div  style='font-family: Assistant-Light;font-size: 100%;letter-spacing: 2px; padding-top: .4%'>
											$i->body
										</div>
									</div>";

							//print_r($my_inquiries);

							// foreach ($my_inquiries[$i->tag] as $reply) {
							// 		echo "<div class='row' style='margin-left: 6%; padding-top: 1%'>
							// 				<div  style='font-family: Assistant-ExtraBold;font-size: 100%;letter-spacing: 2px'>  
							// 					$reply->username
							// 				</div>
							// 				<div style='font-family: Assistant-Light;font-size: 100%;letter-spacing: 2px;'>
							// 					$reply->sent
							// 				</div>
							// 				<div  style='font-family: Assistant-Light;font-size: 100%;letter-spacing: 2px; padding-top: .4%'>
							// 					$reply->body
							// 				</div>
							// 			</div>	";
							// 	}	
							echo "</div>";
						}
					?>
				</div>
			</div>
			</div>
	</div>
<br><br><br><br><br><br><br>
	
</body>

<script type="text/javascript">
	$(".newAdd").click(function(event) {
		$(".addForm").toggle();
	});

	$(".remove").click(function(event){
		var r = "true";
		var i = this.id;
		jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/profile/',
				dataType: 'json',
				data: {remove_address: r, index:i},
				complete: function() {
						//alert(i);
						$('#cont'+i).remove();
				}
			});
	});


	function togglerev() {
    	var element = document.getElementById("reviewcontent");
    	var wrap = document.getElementById("reviewtitle");
    	if (document.getElementById("reviewcontent").classList.contains("active")) {
    		element.classList.remove("active");
    		wrap.classList.remove("active");
    	}
    	else {
    		element.classList.add("active");
    		wrap.classList.add("active");
    	}
	}

	function toggleinq() {
    	var element = document.getElementById("inqcontent");
    	var wrap = document.getElementById("inqtitle");
    	if (document.getElementById("inqcontent").classList.contains("active")) {
    		element.classList.remove("active");
    		wrap.classList.remove("active");
    	}
    	else {
    		element.classList.add("active");
    		wrap.classList.add("active");
    	}
	}
</script>