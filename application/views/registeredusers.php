<div class="ui thirteen wide column" style="padding-right: 3%">
	<div class="ui fluid container" style="margin-top: -3%;padding-bottom: 10%">
			<h1 style="float:right; padding-top:50px;font-size: 50px;font-family: Assistant-ExtraBold; letter-spacing: 5px;">
				REGISTERED USERS
			</h1>
	</div>

	<div class="ui fluid container" style="text-align:right; padding-bottom: 2%;font-size: 20px;font-family: Assistant-Light; letter-spacing: 5px;">
		<?php echo $cnt[0]->cnt?> REGISTERED USERS
	</div>

	<div class="ui fluid container" style="overflow-y: scroll;height: 70%;max-height: 70%;padding-bottom: 10%">
			<table class="ui small compact celled table" id="table">
                <thead>
                    <tr>
                       <th>USERNAME</th>
						<th>EMAIL</th>
						<th>MEMBER SINCE</th>
						<th>DISPLAY NAME</th>
						<th style="text-align: right;">LAST LOGIN</th>
                    </tr>
                </thead>

                <tbody>

	                    <?php 

	                    foreach ($row as $r) 
		                    echo "<tr>
		                        <td data-label='USERNAME'>$r->username</td>
								<td data-label='EMAIL'>$r->email</td>
								<td data-label='MEMBER SINCE'>$r->membership_date</td>
								<td data-label='DISPLAY NAME'>$r->display_name</td>
								<td style='text-align: right;' data-label='AMT SPENT'>$r->lastlogin</td>
		                    </tr>"

	                    ?>
                    


                </tbody>
            </table>
	</div>

	<!-- <div class="ui fluid container">
	
			<h2 style="padding-top:15px;font-size: 25px;font-family: Assistant-Bold; letter-spacing: 2px;">USER PROFILE</h2>

			<div style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;">FIRSTNAME LASTNAME</div>
			<div style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;">@username</div>
			<div style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;">email@yahoo.com</div>
			<div style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;">MEMBER SINCE: MM-DD-YYYY</div>
			<div style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;">ORDER HISTORY COUNT:</div>
			<div style="font-size: 15px;font-family: Assistant-Regular; letter-spacing: 2px;margin-bottom: 2%">TOTAL AMOUNT SPENT:</div>
	</div> -->
			
</div>
</div>

<style type="text/css">
	tr:hover {
          background-color: #FFEF00;
        }

    thead th {
	    position: sticky;
	    position: -webkit-sticky;
	    top: 0;
	    background: white;
	    z-index: 10;
	}
</style>

<script type="text/javascript">


	function getData(row) {
		var order_ID = document.getElementById("table").rows[row.rowIndex].cells[0].innerHTML;
		document.getElementById("hello").innerHTML = order_ID;
	}
</script>


<!-- <script type="text/javascript">
	$('.ui.sticky')
	  .sticky({
	    context: '#body_content'
	  })
	;
</script> -->