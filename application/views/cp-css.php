<style type="text/css">
	.title {
		font-family: Assistant-ExtraBold;
		font-size: 200%;
		letter-spacing: 5%;
	}

	.subheader {
		font-family: Assistant-Bold;
		font-size: 100%;
		letter-spacing: 5%;
	}

	.text {
		font-family: Assistant-Light;
		font-size: 100%;
		letter-spacing: 5%;
		line-height: 120%;
	}

</style>