<style type="text/css">
	.text {
		line-height: 1;
	}

	.cards {
		display: inline-block;
		padding-bottom: 70px;
	}

	 a:link {color: black;     text-decoration: none;
}      /* unvisited link */
       a:visited {color: black;     text-decoration: none;
}   /* visited link */
       a:hover {color: black;    text-decoration: none;
}     /* mouse over link */
       a:active {color: black;    text-decoration: none;
}    /* selected link */

	img.item {
		width:250px;
		  height:400px;
		  padding-bottom: 10px;
		  display: block;
		  backface-visibility: hidden;
		  transition: .5s ease;
	}

	.overlay {
	  transition: .5s ease;
	  opacity: 0;
	  position: absolute;
	  transform: translate(100%, -300%);
	  text-align: center;
	}

	.card:hover .item {
		opacity: 0.4;
	}

	.card:hover .overlay {
	  opacity: 1;
	}

	.content {
		text-align: center;
		line-height: 1.2;
	}

	.stretch {
    width: 100%;
    display: inline-block;
    font-size: 0;
    line-height: 0;
}

</style>


<?php
/*
*	IMAGES' PATH: assets/images/CATEGORY/PRODUCT_CODE/n.jpg
*	MAIN PRODUCT IMAGE: index - 1 
*	FORMAT: .jpg (forced during upload)
*
*	? button -> post 
*/

?>

<br>

<div class='text' style="font-family: Assistant-ExtraBold;font-size: 80px;letter-spacing: 5px; text-align: right;">
<?php echo strtoupper($page);?> </div>

<div class='text' style="font-family: Assistant-Light;font-size: 15px;letter-spacing: 5px; text-align: right;">
<?php echo $count;?> ITEMS FOUND</div> <br><br>

<div class="ui fluid justified container" style="font-family: Assistant-Light;font-size: 15px;letter-spacing: 3px;">


<div class="ui grid">
	<div class='ui row' style="margin-left: 40%;">
              <div class='ui column' style='text-align: center;'>

<div class='stackable cards'>
<?php 
	//for($x=1 ; $x<=$count ; $x++){
	$cnt = 1;
	foreach ($row as $item) {
		echo "
			<div class='card' style='display: inline-block; padding-bottom:5%;'> 
				<a href='" . site_url() . "/product_description?code=$item->code'> 
					<img class='item' src='" . base_url() . "assets/images/PH/$cnt.jpg'> 
				</a> 
				<div class='overlay'> 
					<button id='$item->code' class='favorite ui black mini button' style='opacity:.7;margin-bottom:5%'>
						<div style='opacity:1'>ADD TO WISHLIST</div>
					</button> <br>
					<button id='$item->code' class='tocart ui black mini button' style='opacity:.7;''>
						<div style='opacity:1'>ADD TO CART</div>
					</button> 
				</div> 
				<div class='content' style='text-align:center'>$item->name<br>";

		$x = 0;
		for( ; $x<4; $x++) {
			echo "<i class='small yellow star  icon'></i>";
		}
		for( ; $x<5 ; $x++) {
			echo "<i class='small star  icon'></i>";
		}

		echo "<br>PHP $item->price</div> 
			</div> ";

		$cnt = $cnt+1;
		if ($cnt == 6) $cnt = 1;
	} 
?>

</div></div>

<span class="stretch"> </span>
</div>
</div>

<script type="text/javascript">
	$(".favorite").click(function(event){
		var fave = "true";
		var c = this.id;
		
		jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/profile/save',
				dataType: 'json',
				data: {fave: fave, code:c},
				complete: function() {
						alert('Added to wishlist.');
				}
			});
	});

	$(".tocart").click(function(event){
		var tocart = "true";
		var c = this.id;
		//var q = ("#quant").val();
		//alert(c);
		jQuery.ajax({
				type: "POST",
				url: '<?php echo site_url();?>/profile/save',
				dataType: 'json',
				data: {tocart: tocart, code:c},
				complete: function() {
					alert('Added to cart.');
				}
			});
	});
</script>


