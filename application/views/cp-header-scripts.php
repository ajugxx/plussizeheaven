<html>
	<head>
		<title>CONTROL PANEL</title>

		<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/semantic/dist/semantic.min.css">
		<script
			src="https://code.jquery.com/jquery-3.1.1.min.js"
			integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			crossorigin="anonymous">
		</script>
		<script src="<?php echo base_url();?>assets/semantic/dist/semantic.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/md5.min.js"></script>

		<script src="<?php echo base_url();?>assets/ui_accordion/accordion.min.js"></script>
		
		<script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
		<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">

		<link href="<?php echo base_url();?>assets/css/mui.css" rel="stylesheet" type="text/css" />
		<script src="//<?php echo base_url();?>assets/js/mui.min.js"></script>

		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/slick/slick.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/slick/slick-theme.css"/>

		<!-- Buttons core css -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/buttons.css">
		<!-- Only needed if you want font icons -->
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css" rel="stylesheet">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" rel="stylesheet" />
		<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" rel="stylesheet" />

		<style type="text/css">
			
			@font-face {
				font-family: "Assistant-Bold";
				src: url("<?php echo base_url();?>assets/fonts/Assistant-Bold.ttf");

			}

			@font-face {
				font-family: "Assistant-ExtraBold";
				src: url("<?php echo base_url();?>assets/fonts/Assistant-ExtraBold.ttf");

			}

			@font-face {
				font-family: "Assistant-ExtraLight";
				src: url("<?php echo base_url();?>assets/fonts/Assistant-ExtraLight.ttf");

			}

			@font-face {
				font-family: "Assistant-Light";
				src: url("<?php echo base_url();?>assets/fonts/Assistant-Light.ttf");
			}

			@font-face {
				font-family: "Assistant-Regular";
				src: url("<?php echo base_url();?>assets/fonts/Assistant-Regular.ttf");
			}

			@font-face {
				font-family: "Assistant-SemiBold";
				src: url("<?php echo base_url();?>assets/fonts/Assistant-SemiBold.ttf");
			}

			.row.navbar {
				line-height: 1.5;
			}

			.footer {
			    float: bottom;
			    position: relative;
			    left: 0;
			    bottom: 0;
			    width: 120%;
			    height: 45%;
			    margin-left:-7%;
			    background-color: black;
			    color: white;
			}

			.slick-slide {
      height: 500;
    }

    .text.homebody {
      line-height: 0.8;
    }

.cards.home {
    display: inline-block;
    padding-bottom: 7px;
  }


 .normal a:link {color: black;     text-decoration: none;
}      /* unvisited link */
       a:visited {color: black;     text-decoration: none;
}   /* visited link */
       a:hover {color: black;    text-decoration: none;
}     /* mouse over link */
       a:active {color: black;    text-decoration: none;
}    /* selected link */


    img.hot {
      width:300px;
      height:400px;
      padding-top: 20px;
      padding-bottom: 5px;
      padding-right: 20px;
    }

    img.sale {
      width:200px;
      height:280px;
      padding-right: 20px;
      padding-bottom: 5px;x`
    }

    img.new {
      width:250px;
      height:350px;
      padding: 10px;
    }

    .stretch {
    width: 100%;
    display: inline-block;
    font-size: 0;
    line-height: 0;
}

  .home.content {
    text-align: center;
    line-height: 1.2;
    font-family: Assistant-Light;
    font-size: 15px;
    letter-spacing: 3px;">
  }

  .text.accountbody {
  	font-family: Assistant-Regular;
  	font-size: 5px;
  	letter-spacing: 15px;
  }

  .text.accounttitle {
  	font-family: Assistant-ExtraBold;
  	font-size: 30px;
  	letter-spacing: 20px;
  	color: white;
  }

  .accordiontitle {
  	font-family: Assistant-ExtraBold;
  	font-size: 30px;
  	letter-spacing: 4px;
  	display: inline-block;
  }

   .accordioncontent {
  	font-family: Assistant-Light;
  	font-size: 13px;
  	letter-spacing: 2px;
  	display: inline-block;
  }

  button:focus { outline: none; }

</style>


	</head>