<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_accounts extends CI_Controller {

	public $message = "notice";

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->model('Admin_accounts_model');
		$this->load->library('session');
	}

	public function index()
	{
		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		} else {

			//print_r($this->session->userdata);
			
			$data['self'] = $_SESSION['display_name'];
			$data['handle'] = $_SESSION['admin_ID'];
			$data['row'] = $this->Admin_accounts_model->get_all_admins($_SESSION['admin_ID']);
			

			if ($this->input->post('changeKey')){ 
				$auth = $this->Admin_accounts_model->auth_key($_SESSION['admin_ID'], $this->input->post('key'));
				if ($auth == "true") 
					 $this->Admin_accounts_model->change_key($_SESSION['admin_ID'], $this->input->post('newkey'));
				return $auth;
			}

			if ($this->input->post('save')){ 
					$this->Admin_accounts_model->change_name($_SESSION['admin_ID'], $this->input->post('n'));
					$this->session->set_userdata('display_name', $this->input->post('n'));
			}

				//$data['message'] = "clicked";

				//$this->output->set_output(json_encode($show));
			
			if ($this->input->post('add'))
				$this->Admin_accounts_model->add_an_admin($this->input->post('h'), $this->input->post('n'),$this->input->post('p'));

			if ($this->input->post('remove'))
				$this->Admin_accounts_model->remove_an_admin($this->input->post('id'));


			$data['message'] = $this->message;
			$this->load->view('cp-header-scripts');
			$this->load->view('cp-css');
			$this->load->view('control-panel-header');
			$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
			$this->load->view('adminaccount', $data);
		}
	}
}
