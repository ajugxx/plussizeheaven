<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_Description extends CI_Controller {

	public function __construct()
	{
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
	$this->load->library('session');
	$this->load->model('Product_model');
	$this->load->model('Post_model');
	}

	public function index()
	{
		//$data['rev_count'] = 24;
		//$data['variations_count'] = 7;
		//$data['price'] = 1234.50;
		// return price $data['price']
		//return reviews $data['reviews']
		//return inquiries $data['inquiries']


		if ($this->session->userdata('logged_in') and $this->input->post('logout')) { // destroy session data
			$this->session->set_userdata('logged_in', FALSE);
			session_destroy();
		}

		$code = $this->input->get('code');

		if ($this->input->post('inquire')) {
			$this->Post_model->inquire($_SESSION['username'],$this->input->post('body'), $code);
		}

		$data['desc'] = $this->Product_model->get_data($code);
		$data['rev'] = $this->Product_model->get_reviews($code);
		$data['inq'] = $this->Product_model->get_inquiries($code);   
		$data['rev_count'] = $this->Product_model->get_review_cnt($code);

		$this->load->view('header-scripts');
		$this->load->view('item-methods');

		if ($this->session->userdata('logged_in')) 
			$this->load->view('site-head-navbar-member');
		elseif (!$this->session->userdata('logged_in')) 
			$this->load->view('site-head-navbar');

		$this->load->view('product_description' , $data);
		if ($this->session->userdata('logged_in')) 
		$this->load->view('inquire-form');


		$this->load->view('footer');
		
	}
}
