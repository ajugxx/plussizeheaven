<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog extends CI_Controller {

	public function __construct()
	{
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
	$this->load->library('session');
	$this->load->model('Catalog_model');
	//$this->load->model('User_accounts_model');
	}

	public function index()
	{

		if ($this->session->userdata('logged_in') and $this->input->post('logout')) { // destroy session data
			$this->session->set_userdata('logged_in', FALSE);
			session_destroy();
		}

		//echo $category;
		$this->load->view('header-scripts');
		$this->load->view('item-methods');

		if ($this->session->userdata('logged_in')) 
			$this->load->view('site-head-navbar-member');
		elseif (!$this->session->userdata('logged_in')) 
			$this->load->view('site-head-navbar');

		if ($this->input->post('addToWishlist')) {
			//echo "add";
		}
		if ($this->input->post('addToCart')) {
			//echo "add";
		}
		
		$data['page'] = $this->input->get('c');

		if ($data['page']=="hot items") {
			$data['result'] = $this->Catalog_model->get_hot($data['page']);	// table of all items in the category
		}
		else if ($data['page']=="favorites") {
			$data['result'] = $this->Catalog_model->get_favorites($data['page']);	// table of all items in the category
		}
		else if ($data['page']=="new arrival") {
			$data['result'] = $this->Catalog_model->get_new($data['page']);	// table of all items in the category
		}
		else {
			$data['result'] = $this->Catalog_model->get_items($data['page']);	// table of all items in the category
		}  

		$data['count'] = $data['result']['count']; 
		$data['row'] = $data['result']['rows'];
		// $data['count'] = $this->Catalog_model->count($data['page']);	// no of items in the category
		//$this->Product_card->primary($data['page']);
		$this->load->view('catalog', $data);
		$this->load->view('footer');
		//$something = $this->input->
	}
}
