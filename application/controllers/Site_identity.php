<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_identity extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->model('Site_identity_model');
		$this->load->library('session');
	}

	public function index()
	{
		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		}

		$data['row'] = $this->Site_identity_model->get_all();

		if ($this->input->post('save')) {
			$new['fb'] = $this->input->post('fb');
			$new['ig'] = $this->input->post('ig');
			$this->Site_identity_model->update($new);
		}

		$this->load->view('cp-header-scripts');
		$this->load->view('cp-css');
		$this->load->view('control-panel-header');
		$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
		$this->load->view('contactinformation', $data);
		// $this->load->view('footer');
	}
}
 