<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class recent_activity extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('Core_model');
	}

	public function index()
	{
		redirect('/recent_activity/unprocessed_orders');	
	}

	public function unprocessed_orders()
	{
		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		}

		else {

			$data['row'] = $this->Core_model->get_unprocessed(); 
			foreach ($data['row'] as $r) {	// get corresponding items for each
				$orderid = $r->order_ID;
				$u = $r->username;
				$data['items'][$u.$orderid]  =  $this->Core_model->get_items($u, $orderid); 
			}

			$data['m'] = "message";
			$this->load->view('cp-header-scripts');
			$this->load->view('cp-css');
			$this->load->view('control-panel-header');
			$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
			$this->load->view('unprocessed_orders', $data);
		}
	}

	public function unshipped_orders()
	{
		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		}

		else {
			$data['row'] = $this->Core_model->get_unshipped(); 
			foreach ($data['row'] as $r) {	// get corresponding items for each
				$orderid = $r->order_ID;
				$u = $r->username;
				$data['items'][$u.$orderid]  =  $this->Core_model->get_items($u, $orderid); 
			}

			$this->load->view('cp-header-scripts');
			$this->load->view('control-panel-header');
			$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
			$this->load->view('cp-css');
			$this->load->view('unshipped_orders', $data);
		}
	}

	public function shipped_orders()
	{
		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		}

		else {
			$data['row'] = $this->Core_model->get_shipped(); 
			foreach ($data['row'] as $r) {	// get corresponding items for each
				$orderid = $r->order_ID;
				$u = $r->username;
				$data['items'][$u.$orderid]  =  $this->Core_model->get_items($u, $orderid); 
			}
			$this->load->view('cp-header-scripts');
			$this->load->view('control-panel-header');
			$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
			$this->load->view('cp-css');
			$this->load->view('shipped_orders', $data);
		}
	}

	public function unresolved_inquiries()
	{
		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		}

		else {
			$data['row'] = $this->Core_model->get_inquiries(); 

			if ($this->input->post('save')) {
				//echo $_SESSION['admin_ID']. " " .$this->input->post('resolvetag'). " " .$this->input->post('rep');
				$this->Core_model->resolve($_SESSION['admin_ID'],$this->input->post('resolvetag'), $this->input->post('rep'));
			} 
			$this->load->view('cp-header-scripts');
			$this->load->view('control-panel-header');
			$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
			$this->load->view('cp-css');
			$this->load->view('unresolved_inquiries', $data);
		}
	}

	public function messages()
	{
		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		}

		else {
			$data['row'] = $this->Core_model->messages(); 

			if ($this->input->post('read')) {
				$this->Core_model->markAsRead($this->input->post('tag'));
			} 

			$this->load->view('cp-header-scripts');
			$this->load->view('control-panel-header');
			$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
			$this->load->view('cp-css');
			$this->load->view('messages', $data);
		}
	}
}
