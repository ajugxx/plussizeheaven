<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddAFAQ extends CI_Controller {
	public function __construct()
	{
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
	$this->load->model('FAQ_model');
	$this->load->library('session');
	}
 
	public function index()
	{
		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		}
		else {

			$data['row'] = $this->FAQ_model->get_all();

			if ($this->input->post('add')) { 	// add a faq
				 $this->FAQ_model->add($this->input->post('question'),$this->input->post('answer'));
			}

			if ($this->input->post('remove')) 	// remove a faq
				 $this->FAQ_model->remove($this->input->post('tag'));

			if ($this->input->post('update')) 	// edit a faq
				 $this->FAQ_model->update($this->input->post('tag'), $this->input->post('newQ'),$this->input->post('newAns'));
			

			$this->load->view('cp-header-scripts');
			$this->load->view('cp-css');
			$this->load->view('control-panel-header');
			$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
			$this->load->view('addafaq', $data);
		}
	}
}
