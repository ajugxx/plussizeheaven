<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactUs extends CI_Controller {

	public function __construct()
	{
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
	$this->load->library('session');
	$this->load->model('Message_model');
	//$this->load->model('User_accounts_model');
	}

	public function index()
	{

		if ($this->session->userdata('logged_in') and $this->input->post('logout')) { // destroy session data
			$this->session->set_userdata('logged_in', FALSE);
			session_destroy();
		}

		//echo $category;
		$this->load->view('header-scripts');
		//$this->load->view('item-methods');

		if ($this->session->userdata('logged_in')) 
			$this->load->view('site-head-navbar-member');
		elseif (!$this->session->userdata('logged_in')) 
			$this->load->view('site-head-navbar');

		if ($this->input->post('contact')) {
			$this->Message_model->send($this->input->post('name'), $this->input->post('email'), $this->input->post('cat'), $this->input->post('title'), $this->input->post('body'));
		}
		// $data['count'] = $this->Catalog_model->count($data['page']);	// no of items in the category
		//$this->Product_card->primary($data['page']);
		$this->load->view('contact_us');
		$this->load->view('footer');
		//$something = $this->input->
	}
}
