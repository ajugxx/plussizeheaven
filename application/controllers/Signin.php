<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {
	public function index()
	{
		$this->load->helper('url');
		$this->load->view('header-scripts');
		//$this->load->view('site-head-navbar');
		$this->load->view('signin');
	}
}
