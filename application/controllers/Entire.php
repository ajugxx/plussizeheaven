<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entire extends CI_Controller {
	public function index()
	{
		$this->load->helper('url');
		$this->load->view('entire');
	}
}
