<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist extends CI_Controller {

	public function __construct()
	{
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
	$this->load->library('session');
	$this->load->model('Wishlist_model');
	}
 
	public function index()
	{
		if (!$this->session->userdata('logged_in'))
		{
			redirect('/account');	
		}

		elseif ($this->session->userdata('logged_in')) 
		{
			$data['row'] = $this->Wishlist_model->get_items($_SESSION['username']);

			if ($this->input->post('removecode'))
				$this->Wishlist_model->remove_item($_SESSION['username'], $this->input->post('removecode')); 
			if ($this->input->post('clear'))
				$this->Wishlist_model->clear_wishlist($_SESSION['username']); 

			$this->load->view('header-scripts');
			$this->load->view('site-head-navbar-member');
			$this->load->view('wishlist', $data);
			$this->load->view('footer');
		}
		
	}
}
