<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct()
	{
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
	$this->load->model('User_accounts_model');
	$this->load->library('session');
	}
 

	public function index()
	{
		if ($this->session->userdata('logged_in')) // an account is logged in 
		{
			redirect('/profile');
		}

		if ($this->input->post('SIGNUP'))
		{
			$f=$this->input->post('firstname');
			$l=$this->input->post('lastname');
			$e=$this->input->post('email');
			$u=$this->input->post('username');
			$m=$this->input->post('mobile');
			$t=date('Y-m-d H:i:s');
			$a=$this->input->post('testfield');  //encrypt
			$data['registered'] = $this->User_accounts_model->add_a_new_user($u,$e,$f,$l,$m,$t,$a);	
			if ($data['registered'] == "true") {
				$data['message'] = "<div class='ui compact success message'>
							<div class='header'>
								YOUR USER REGISTRATION WAS SUCCESSFUL.
							</div>
							<p>YOU MAY NOW LOG-IN WITH YOUR USERNAME OR EMAIL.</p>
						</div>";
			}
			else {
				$data['message'] = "<div class='ui compact error message'>
							  <div class='header'>
							    USERNAME/EMAIL ALREADY EXISTS.
							  </div>
							</div>";
			}
			//redirect("/account/", $data);	
		}

		elseif ($this->input->post('SIGNIN'))
		{
			$a=$this->input->post('acctholder');
			$p=$this->input->post('placeholder');
			$t=date('Y-m-d H:i:s');
			$data['loggedin'] = $this->User_accounts_model->log_in($a,$p,$t);
			
			if ($data['loggedin'] != "false"){		// successful log in, user session
				// $user_credentials = $this->session->set_userdata($data['loggedin']);
				$user_credentials = json_decode(json_encode($data['loggedin'][0]), True);
				$this->session->set_userdata($user_credentials);
				$this->session->set_userdata('logged_in', TRUE);
				//$this->session->set_userdata('authorized', FALSE);
				//$this->session->set_flashdata('user_credentials', $data['loggedin']);
				//print_r($this->session->all_userdata());
				
				if ($_SESSION['authorized'])
					redirect('/recent_activity');

				redirect("/profile");


			}else {
				$data['message'] = "<div class='ui compact error message'>
							  <div class='header'>
							    LOG-IN FAILED.
							    <p>wrong username/email or password</p>
							  </div>
							</div>";
			}
		}

		else {
			//$data['registered'] = "none";
			//$data['loggedin'] = "none";
			$data['message'] = "";
 		}

		$this->load->view('header-scripts');
		$this->load->view('site-head-navbar');
		$this->load->view('account', $data);
		$this->load->view('footer');
		$data['message'] = "";
	}
}
