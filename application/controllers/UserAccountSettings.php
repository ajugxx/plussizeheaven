<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserAccountSettings extends CI_Controller {
	public function index()
	{
		$this->load->helper('url');
		$this->load->view('header-scripts');
		$this->load->view('site-head-navbar');
		$this->load->view('useraccountsettings');
		$this->load->view('footer');
	}
}
