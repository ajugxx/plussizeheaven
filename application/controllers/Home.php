<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/* HOME CONTROLLER */

class Home extends CI_Controller {

	public function __construct()
	{
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
	$this->load->library('session');
	$this->load->model('Catalog_model');
	}

	public function index()
	{

		$data['message'] = '';
		$data['row_hot'] = $this->Catalog_model->get_hot()['rows'] ;
		$data['row_faves'] = $this->Catalog_model->get_favorites()['rows'];
		$data['row_new'] = $this->Catalog_model->get_new()['rows'];

		if ($this->session->userdata('logged_in') and $this->input->post('logout')) { // destroy session data
			$this->session->set_userdata('logged_in', FALSE);
			session_destroy();
		}

		$this->load->view('header-scripts');
		$this->load->view('item-methods');

		if ($this->session->userdata('logged_in')) {
			$this->load->view('site-head-navbar-member');
		}
		elseif (!$this->session->userdata('logged_in')) 
			$this->load->view('site-head-navbar');
			

		$this->load->view('home', $data);	
		$this->load->view('footer');
	}
}
