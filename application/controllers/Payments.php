<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {

	public function __construct()
	{
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
	$this->load->library('session');
	$this->load->model('Core_model');
	}

	public function index()
	{

		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		}

		else {
			if ($this->session->userdata('logged_in') and $this->input->post('logout')) { // destroy session data
				$this->session->set_userdata('logged_in', FALSE);
				session_destroy();
			}

			//echo $category;
			else 
			{
				$data['row'] = $this->Core_model->get_payments();
				$this->load->view('cp-header-scripts');
				$this->load->view('cp-css');
				$this->load->view('control-panel-header');
				$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
				$this->load->view('payments', $data);

			}
		}
		//$something = $this->input->
	}
}

