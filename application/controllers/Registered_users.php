<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registered_users extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->model('Users_model');
		$this->load->library('session');
	}

	public function index()
	{
		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		}
		else {

			$data['row'] = $this->Users_model->get_all();
			$data['cnt'] = $this->Users_model->count();

			$this->load->view('cp-header-scripts');
			$this->load->view('cp-css');
			$this->load->view('control-panel-header');
			$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
			$this->load->view('registeredusers', $data);
		}
		// $this->load->view('footer');
	}
}
