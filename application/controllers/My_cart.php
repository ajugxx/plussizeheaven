<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_cart extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('Shopping_cart_model');
		$this->load->view('header-scripts');
		if ($this->session->userdata('logged_in')) 
			$this->load->view('site-head-navbar-member');
		elseif (!$this->session->userdata('logged_in')) 
			$this->load->view('site-head-navbar');
		if ($this->session->userdata('logged_in') and $this->input->post('logout')) { // destroy session data
			$this->session->set_userdata('logged_in', FALSE);
			redirect('/my_cart');
			session_destroy();
		}
	}

	public function index()
	{
		// get my shopping list
		$data['row'] = $this->Shopping_cart_model->get_items($_SESSION['username']);

		if ($this->input->post('clear')) {
			$this->Shopping_cart_model->clear_cart($_SESSION['username']);
		}
		if ($this->input->post('checkout')) {
			redirect('/my_cart/checkout');
			// display list of items
			// checkout form
			// insert to checkout
			// message: order placed, shop for more, clear cart
		}
		
		$this->load->view('shopping_cart', $data);
		$this->load->view('footer');
	}

	public function checkout(){ 
		/*if ( $_SERVER['REQUEST_METHOD']=='GET') 
		{
	        echo "oops sorry you cant do that :)";
    	}

	    else 
	    {*/
			$this->load->view('orderform');		// present order form
		//}	

		if ($this->input->post('confirm order')) {
			// process and insert to table checkout
			// message: order placed, shop for more, clear cart
		}


		$this->load->view('footer');	
		}
	}
?>
