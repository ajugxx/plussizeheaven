<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms_and_policies extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->model('TAP_model');
		$this->load->library('session');
	}

	public function index()
	{
		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		}
		else {

			$data['row'] = $this->TAP_model->get_all();

			if ($this->input->post('save')) {
				$new['au'] = $this->input->post('au');
				$new['rp'] = $this->input->post('rp');
				$new['pm'] = $this->input->post('pm');
				$new['so'] = $this->input->post('so');
				$this->TAP_model->update($new);
			}

			$this->load->view('cp-header-scripts');
			$this->load->view('cp-css');
			$this->load->view('control-panel-header');
			$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
			$this->load->view('termspolicies', $data);
		// $this->load->view('footer');
		}
		
	}
}
