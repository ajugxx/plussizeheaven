<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
	$this->load->library('session');
	$this->load->model('User_accounts_model');
	$this->load->model('My_profile_model');
	$this->load->model('Wishlist_model');
	$this->load->model('Shopping_cart_model');
	}
 
	public function index()
	{
		if (!$this->session->userdata('logged_in'))
		{
			$data['message'] = "<div class='ui compact error message'>
							  <div class='header'>
							    ACCESS DENIED. PLEASE LOG-IN FIRST.
							  </div>
							</div>";

			redirect('/account/', $data);	
		}

		elseif ($this->session->userdata('logged_in')) 
		{
			if ($this->session->userdata('logged_in') and $this->input->post('logout')) { // destroy session data
				$this->session->set_userdata('logged_in', FALSE);
				session_destroy();
				redirect('/');
			}

			//print_r($this->session->userdata); 
			//Array ( [__ci_last_regenerate] => 1543314049 [username] => augallardo [email] => hello@world.com [display_name] => hello world [mobile] => 09171169207 [logged_in] => 1 )
			
			// get my basic info
			$data['handle'] = $_SESSION['username'];
			$data['name'] = strtoupper($_SESSION['display_name']);
			$data['memdate'] = $_SESSION['membership_date'];

			// get my list of reviews
			$data['my_reviews'] = $this->My_profile_model->get_my_reviews($data['handle']);
			//echo $data['my_reviews'];

			// my list of inquiries and the replies to it
			$data['my_inquiries'] = $this->My_profile_model->get_my_inquiries($data['handle']);
			//$data['my_inquiries'] contains: 'roots' and 'n' where n is every tag in 'roots'

			// return my shipping addresses
			$data['my_addresses'] = $this->My_profile_model->get_my_addresses($data['handle']);
			
			if ($this->input->post('add_address')) 
			{
				// get address info
				$address = "(Unit " . $this->input->post('unit');
				$address = $address . " " . $this->input->post('bldng') . ") ";
				$address = $address . " " . $this->input->post('st');
				$address = $address . " " . $this->input->post('municipality');
				$address = $address . " " . $this->input->post('city');
				$address = $address . " " . $this->input->post('province');
				$address = $address . " " . $this->input->post('region');
				//$address = $address . " " . $this->input->post('country');
				//$address = $address . " " . $this->input->post('zipcode');

				$this->My_profile_model->add_address($data['handle'], $address, $this->input->post('landmark'));
			}

			if ($this->input->post('edit_address'))
			{
				$address = $this->input->post('address');
				/*$address['complete'] = "(Unit " . $this->input->post('unit');
				$address['complete'] = $address['complete'] . " " . $this->input->post('bldng name') . ") ";
				$address['complete'] = $address['complete'] . " " . $this->input->post('street_add');
				$address['complete'] = $address['complete'] . ", " . $this->input->post('brgy');
				$address['complete'] = $address['complete'] . ", " . $this->input->post('municipality');
				$address['complete'] = $address['complete'] . ", " . $this->input->post('city');
				$address['complete'] = $address['complete'] . ", " . $this->input->post('province');
				$address['complete'] = $address['complete'] . ", " . $this->input->post('region');
				//$address['complete'] = $address['complete'] . " " . $this->input->post('country');
				$address['complete'] = $address['complete'] . " " . $this->input->post('zipcode');
				$address['landmark'] = $this->input->post('landmark');*/

				$this->My_profile_model->update_address($data['handle'], $this->input->post('index'), $address);
			}

			if ($this->input->post('remove_address'))
			{
				$this->My_profile_model->remove_address($data['handle'], $this->input->post('index'));
			}

			$this->load->view('header-scripts');
			$this->load->view('site-head-navbar-member');
			$this->load->view('userprofile', $data);
			$this->load->view('footer');
		}
	}

	public function settings() 
	{
		if (!$this->session->userdata('logged_in'))
		{
			$data['message'] = "<div class='ui compact error message'>
							  <div class='header'>
							    ACCESS DENIED. PLEASE LOG-IN FIRST.
							  </div>
							</div>";

			redirect('/account/', $data);	
		}

		elseif ($this->session->userdata('logged_in')) 
		{
			if ($this->session->userdata('logged_in') and $this->input->post('logout')) { // destroy session data
				$this->session->set_userdata('logged_in', FALSE);
				session_destroy();
				redirect('/');
			}

			//$data['handle'] = $_SESSION['username'];
			$data['name'] = strtoupper($_SESSION['display_name']);
			$data['email'] = $_SESSION['email'];
			$data['mobile'] = $_SESSION['mobile'];
			//$data['notice'] = "";
			$this->load->view('header-scripts');
			$this->load->view('site-head-navbar-member');


			if ($this->input->post('change_pw')) {
				$current = $this->input->post('validate');
				$new = $this->input->post('take');

				$data['notice'] = $this->My_profile_model->change_key($_SESSION['username'], $current, $new);		
			} else {
				$data['notice'] = "";
			}
				
			$this->load->view('useraccountsettings', $data);
			$this->load->view('footer');	
			
		}
	}

	public function order_history(){
		if (!$this->session->userdata('logged_in'))
		{
			$data['message'] = "<div class='ui compact error message'>
							  <div class='header'>
							    ACCESS DENIED. PLEASE LOG-IN FIRST.
							  </div>
							</div>";

			redirect('/account/', $data);	
		}

		elseif ($this->session->userdata('logged_in')) 
		{
			$data['rowtopay'] = $this->My_profile_model->getToPay($_SESSION['username']); 
			$data['rowpending'] = $this->My_profile_model->getPending($_SESSION['username']); 
			$data['rowcompleted'] = $this->My_profile_model->getCompleted($_SESSION['username']); 
			$data['rowcancelled'] = $this->My_profile_model->getCancelled($_SESSION['username']); 

			$this->load->view('header-scripts');
			$this->load->view('site-head-navbar-member');
			$this->load->view('orderhistory', $data);
			$this->load->view('footer');
		}
	}

	public function review() {
		if (!$this->session->userdata('logged_in'))
		{
			$data['message'] = "<div class='ui compact error message'>
							  <div class='header'>
							    ACCESS DENIED. PLEASE LOG-IN FIRST.
							  </div>
							</div>";

			redirect('/account/', $data);	
		}

		elseif ($this->session->userdata('logged_in')) 
		{
			$data['row'] = $this->My_profile_model->get_availed($_SESSION['username']); 

			$this->load->view('header-scripts');
			$this->load->view('site-head-navbar-member');
			$this->load->view('review-products', $data);
			$this->load->view('footer');
		}
	}

	public function save(){
		if ( $_SERVER['REQUEST_METHOD']=='GET') 
		{
	        redirect('/');
    	} else {
    		if ($this->input->post('fave')) 
    		{
    			$this->Wishlist_model->add_item($_SESSION['username'], $this->input->post('code'));
    		}

    		if ($this->input->post('tocart'))
    		{
    			$this->Shopping_cart_model->add_item($_SESSION['username'], "","",$this->input->post('code'), 1);	
    		}
    	}
	}
}