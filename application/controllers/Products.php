<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

	public function __construct()
	{
	parent::__construct();
	$this->load->database();
	$this->load->helper('url');
	$this->load->library('session');
	$this->load->model('Product_list_model');
	}

	public function index()
	{

		if (!$_SESSION['authorized']) { //not admin
			redirect('/');
		}
		else {



			if($this->input->post('edit'))  
			{   
				$this->Product_list_model->get_details($this->input->post('tag'));
			}

			if($this->input->post('add'))  
			{   
				$this->Product_list_model->add_new($this->input->post('c'), $this->input->post('n'), $this->input->post('cat'), $this->input->post('s'), $this->input->post('d'), $this->input->post('a'), $this->input->post('m'), $this->input->post('sr'), $this->input->post('col'), $this->input->post('mat'), $this->input->post('made'));
			}

			if($this->input->post('remove'))  
			{   
				$this->Product_list_model->remove_product($this->input->post('tag'));
			}

			

			//echo $category;
			$this->load->view('cp-header-scripts');
			$this->load->view('cp-css');
			$this->load->view('control-panel-header');
			$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
			$this->load->view('products');
		}

		//$something = $this->input->
	}
}

