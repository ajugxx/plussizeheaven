<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class All_products extends CI_Controller {
	public function index()
	{
		$this->load->helper('url');
		$this->load->view('cp-header-scripts');
		$this->load->view('cp-css');
		$this->load->view('control-panel-header');
		$data['name']=$_SESSION['display_name'];
$data['handle']=$_SESSION['admin_ID'];

$this->load->view('control-panel-navbar',$data);
		$this->load->view('products');
		// $this->load->view('footer');
	}
}
