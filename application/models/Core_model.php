<?php

	/* MODEL USED BY ADMIN CONTROL PANEL: RECENT ACTIVITY, PAYMENTS */

	class Core_model extends CI_Model 
	{
		function get_payments() {
			$e = "SELECT * from payments";
			return $this->db->query($e)->result();
		}

		function get_unprocessed() {
			$e = "SELECT *,checkout_date cdate, shipping_address address, receiver, landmark 
					FROM payments p,checkout c 
					WHERE p.username=c.username AND p.order_id=c.order_id AND status='processing' AND status!='cancelled'";
			return $this->db->query($e)->result();
		}

		function get_items($u, $orderid) 
		{
			$e = "SELECT variation v, size s, code c, quantity q, name n 
				FROM availed, products 
				WHERE username='$u' AND order_id='$orderid' AND product_code=code";
		 	return $this->db->query($e)->result();
		}

		function get_unshipped()
		{
			$e = "SELECT *, checkout_date cdate, shipping_address address, receiver, courier, landmark, tracking_number trn, eta
					FROM payments p,checkout c 
					WHERE p.username=c.username AND p.order_id=c.order_id AND status='processed' AND status!='cancelled'";
			return $this->db->query($e)->result();
		}

		function get_shipped()
		{
			// return all columns in table checkout + all * for each row in table availed
				// WHERE STATUS == SHIPPED
			$e = "SELECT *, checkout_date cdate, shipping_address address, receiver, courier, landmark, tracking_number trn, eta
					FROM payments p,checkout c 
					WHERE p.username=c.username AND p.order_id=c.order_id AND status='shipped' AND status!='cancelled'";
			return $this->db->query($e)->result();
		}

		// function get_paid()
		// {
		// 	// return all rows with payment reference not null
		// }

		function get_inquiries()
		{
			$e = "SELECT *,name prodname 
				FROM inquire i, products p 
				WHERE admin=0 AND replied=0 AND p.product_code = i.product_code";
			return $this->db->query($e)->result();
		}

		function resolve($admin, $tag, $body) 
		{
			//echo "here";
			$t=date('Y-m-d H:i:s');
			$e = "INSERT INTO inquire 
					VALUES ('','$admin','','$t','$body','$tag',1,0)";
			$this->db->query($e);

			$e = "UPDATE inquire SET replied=1 WHERE tag='$tag'";
			 $this->db->query($e);
		}

		function messages()
		{
			$e = "SELECT * FROM messages ORDER BY tag DESC";
			return $this->db->query($e)->result();
		}

		function markAsRead($tag) 
		{
			$e = "UPDATE messages SET replied=1 WHERE tag='$tag'";
			 $this->db->query($e);
		}
	}
?>
