<?php

	/* MODEL USED BY ADMIN ACCOUNTS PAGE */

	class Admin_accounts_model extends CI_Model 
	{
		function get_self($username)
		{
			//return name
			$e = "SELECT display_name name FROM administrators WHERE admin_ID='$username'";
			$query=$this->db->query($e);
			return $query->result();
		}

		function auth_key($username, $key)
		{
			// if $key matches adkey in table administratos w/ username == admin_ID
					// then return true
			// else return false
			$e = "SELECT * FROM administrators WHERE admin_ID='$username' AND adkey='$key'";
			$query=$this->db->query($e);
			
			if ($query->num_rows() == 1) {
				return "true";
			}
			else {
				return "false";
			}
		}

		function change_key($username, $newkey)
		{
			// update adkey in table administrators where username matches admin_ID
			$e = "UPDATE administrators SET adkey = '$newkey' WHERE admin_ID='$username'";
			$this->db->query($e);
		}

		function change_name($username, $newname) 
		{
			// update display_name in table administrator where username matches
			$e = "UPDATE administrators SET display_name = '$newname' WHERE admin_ID='$username'";
			$query=$this->db->query($e);
		} 

		function change_handle($username, $new) 
		{
			// update display_name in table administrator where username matches
			echo $username;
			$e = "UPDATE administrators SET admin_ID = '$new' WHERE admin_ID='$username'";
			$query=$this->db->query($e);
		} 


		function get_all_admins($username) 
		{
			// return name and username of all admins except for $username
			$e = "SELECT admin_ID id ,display_name name FROM administrators WHERE admin_ID!='$username'";
			$query=$this->db->query($e);
			return $query->result();
		}

		function add_an_admin($username, $name, $adkey)
		{
			// insert	
			//check if such account already exists
			// $e="SELECT * FROM administrators WHERE admin_ID='$username'";
			// $query=$this->db->query($e);
			// if ($query->num_rows() == 1) {
			// 	return "existing";
			// }
			// else {		
				$query = "INSERT INTO administrators VALUES('$username', '$name', '$adkey', 1, '')";
				$this->db->query($query);

				$query = "INSERT INTO registered_users VALUES('$username', '$username','','','','$adkey','','')";
				$this->db->query($query);


				// return "added";
			// }
		}

		function remove_an_admin($username) 
		{
			// remove from table where username matches
			$e="DELETE FROM administrators WHERE admin_ID='$username'";
			$this->db->query($e);
		}

	}
?>