<?php
	class My_profile_model extends CI_Model 
	{
		function get_my_info($username)
		{
			//return name and contact info

			//check if such account already exists
			$e="SELECT * FROM registered_users WHERE username='".$username."' OR email='".$email."'";
			$query=$this->db->query($e);
			if ($query->num_rows() == 1) {
				return "false";
			}
			else {	//register
				$display_name = $firstname. " ".$lastname;	
				$query = "INSERT INTO registered_users VALUES('$username', '$email', '$display_name', '$mobile', '$timestamp', '$acctkey', '$timestamp')";
				$this->db->query($query);
				return "true";
			}
		}

		function get_availed($username) 
		{
			$e = "SELECT DISTINCT order_ID, code, name, category 
							FROM availed a, products p 
							WHERE a.username='$username'
									AND a.code='p.product_code'";
			$query=$this->db->query($e);
			return $query->result();
		}

		function getToPay($username) 
		{
			$e = "SELECT order_ID id, checkout_date cdate, recipient r, shipping_address sa, shipping_fee sfee, products_price subtotal, status, courier c, tracking_number tn, cancelled_on cancel, ETA e
					FROM checkout 
					WHERE username='$username' AND status='processing'";
			$query=$this->db->query($e);
			return $query->result();
		}

		function getPending($username) 
		{
			$e = "SELECT order_ID id, checkout_date cdate, recipient r, shipping_address sa, shipping_fee sfee, products_price subtotal, status, courier c, tracking_number tn, cancelled_on cancel, ETA e 
					FROM checkout 
					WHERE username='$username' AND status='processed' OR status='shipped'";
			$query=$this->db->query($e);
			return $query->result();
		}

		function getCompleted($username) 
		{
			$e = "SELECT order_ID id, checkout_date cdate, recipient r, shipping_address sa, shipping_fee sfee, products_price subtotal, status, courier c, tracking_number tn, cancelled_on cancel, ETA e 
					FROM checkout 
					WHERE username='$username' AND status='completed'";
			$query=$this->db->query($e);
			return $query->result();
		}

		function getCancelled($username) 
		{
			$e = "SELECT order_ID id, checkout_date cdate, recipient r, shipping_address sa, shipping_fee sfee, products_price subtotal, status, courier c, tracking_number tn, cancelled_on cancel, ETA e 
					FROM checkout 
					WHERE username='$username' AND status='cancelled'";
			$query=$this->db->query($e);
			return $query->result();
		}

		function get_my_reviews($username)
		{
			$e = "SELECT product_code,time_stamp,title,body,stars FROM rate WHERE username='".$username."'";
			$query=$this->db->query($e);
			//if ($query->num_rows() == 0) {
			//	return "no reviews";
			//}
			//else {
				return $query->result();
			//}
		}

		function get_my_inquiries($username)
		{
			// select all roots of threads i created
			$e = "SELECT tag, product_code, sent, body FROM inquire WHERE username='".$username."' AND parent=".strval(0);
			$query=$this->db->query($e);
			//if ($query->num_rows() == 0) 
			//{
			//	return "no inquiries";
			//}
			//else 
			//{
				 $result['roots'] = $query->result();		// contains table of all root threads i created
				 if ($query->num_rows() != 0) {
					 foreach ($result['roots'] as $parent) {		//get all replies to each of my roots 
					 	$replies = "SELECT username, sent, body FROM inquire WHERE parent='".strval($parent->tag)."'";
					 	$result[strval($parent->tag)] = $this->db->query($e)->result();
					 	// the index of each reply is the parent 
					 	// so to get all replies for my root w/ tag 4 --> $result['4']
					 }
				}
				 return $result;
			//}
		}

		function get_my_addresses($username)
		{
			$e = "SELECT a_index, address, landmark FROM user_addresses WHERE username='$username'";
			$query=$this->db->query($e);
			return $query->result();
		}


		function update_singular($new_name,$new_contact_no,$new_email)
		{
			// update name and contact info

			//check if admin 
			$e = "SELECT admin_ID, display_name,admin FROM administrators WHERE admin_ID='" .$tag."' AND adkey='" .$pw."'";
			$query=$this->db->query($e);
			if($query->num_rows() == 1) { // it's an admin account
				return $query->result();
			}


			$e="SELECT username, email,display_name,mobile FROM registered_users WHERE (username='".$tag."' OR email='". $tag."') AND acctkey= '". $pw."'";
			$query=$this->db->query($e);
			if($query->num_rows() == 1) { //successful log in
				return $query->result();
			}
			else return "false";
		}

		function update_address($username, $index, $new_address) 
		{
			$e = "UPDATE user_addresses SET address='". $new_address['complete'] ."' , landmark='".$new_address['landmark']."' WHERE a_index=".strval($index);
			$query=$this->db->query($e);
		}


		function add_address($username, $complete, $landmark) 
		{
			$c= "SELECT * FROM user_addresses WHERE username='$username' AND  address='$complete'";
			$c = $this->db->query($c)->num_rows();

			if ($c == 0 ) {

				$index = "SELECT MAX(a_index)+1 AS i FROM user_addresses WHERE username='$username'";
				$index=$this->db->query($index)->result();
				$index=$index[0]->i;
				$e = "INSERT INTO user_addresses VALUES ('$username', '$index','$complete', '$landmark')";
				$this->db->query($e);
			}
		}

		function remove_address($username, $index) 
		{
			$e = "DELETE FROM user_addresses WHERE username='$username' AND a_index='$index'";
			$query=$this->db->query($e);
		}

		function change_key($username, $current, $new) 
		{
			$e = "SELECT * FROM registered_users WHERE username='$username' AND acctkey='$current'";
			$query=$this->db->query($e);

			if($query->num_rows() == 1) { 
				$e = "UPDATE registered_users SET acctkey='$new' WHERE username='$username'";
				$query=$this->db->query($e);
				return "<div class='ui fluid success message'><div class='header'>PASSWORD SUCCESSFULY CHANGED. </div></div>";
			}
			else {
				return "<div class='ui fluid error message'> <div class='header'>WRONG PASSWORD</div></div>";
			}

		}

		//function displayrecords()
		//{
		//$query=$this->db->query("select * from users");
		//return $query->result();
		//}
	}
?>